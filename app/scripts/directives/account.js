define([
  'angularAMD',
  'environment'
], function (__interface__, config) {

  accountDirective = function ($http, $sce, Account) {

    var postLink = function ($scope, element) {
      $scope.__interface__ = {};
      $scope.model = {};

      Account.get(function (data, getResponseHeaders) {
        $scope.account = data;
      });

      /*
       *{
       *  "network"           : "livenet",
       *  "depth"             : 0,
       *  "fingerPrint"       : 40714239,
       *  "parentFingerPrint" : 0,
       *  "childIndex"        : 0,
       *  "chainCode"         : "cba62fbdd072f862aa0f5da7de25b95d921b60a08246862a5a8f634d9890ed8f",
       *  "privateKey"        : "b3287a7cbed896cb5d058b64361d24a4035cc0bffc92248b416348f89be79319",
       *  "checksum"          : 1166884313,
       *  "xprivkey"          : "xprv9s21ZrQH143K45xJiPpFTwz51dKoxFk2CtnR4fEWjKtPMVw8TfvUaxYNKvoPbvXMsLnsCimYNTABTijUvdE4gniBcy3dRnxSk7tVRiS3CyW"
       *}
       */

      /*
       *{
       *  "network"           : "livenet",
       *  "depth"             : 0,
       *  "fingerPrint"       : 906916294,
       *  "parentFingerPrint" : 0,
       *  "childIndex"        : 0,
       *  "chainCode"         : "056a922847c4a23afb52b74f09ed0a561148136ab35234020e0cddd4c4e51515",
       *  "privateKey"        : "de8dff6e0985724dde19870a09b066324fb6a666d276abb193fdd5bdac35507c",
       *  "checksum"          : -869826662,
       *  "xprivkey"          : "xprv9s21ZrQH143K27V5CdQHZHhDafC5UTgdH1bbQdVZ69VpcKQoaG1TKxm8ZSyadhFeqmh3QHef3BzP3V5iFeV3kmVoNLAE6DT9v832akQyzWu"
       *}
       */

      var newUser = new Account({
        mnemonic: 'swallow edit wear diary mushroom adapt logic talent beef antique mobile knee'
      });
      newUser.$save();
    };

    return {
      restrict    : 'A',
      templateUrl : 'scripts/partials/account.html',
      link        : postLink
    };
  };

  __interface__.directive('account', [
    '$http',
    '$sce',
    'Account',
    accountDirective
  ]);

});
