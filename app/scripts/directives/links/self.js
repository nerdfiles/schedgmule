define([
  'angularAMD',
  'environment'
], function (__interface__, environment) {

  var selfLinkRelation = function ($http, $q, $rootScope) {

    /**
     * @ngdoc
     * @ngdirective
     */
    $rootScope.relations = {};
    $rootScope.relations.accountSelf = ['account', 'self'];
    function postLink ($scope, element, $attrs) {
      $scope.self = '';

      var _parent = '^' + $attrs.relationships[0][0]+ 'Directive';
      var _templateUrl = './scripts/partials/links/' + $attrs.relationships[0][1];
    }

    return {
      restrict    : 'A',
      scope       : {},
      require     : _parent,
      templateUrl : _templateUrl,
      link        : postLink
    };
  };

  __interface__.directive('relationships', [
    '$http',
    '$q',
    '$rootScope',
    selfLinkRelation
  ]);
});
