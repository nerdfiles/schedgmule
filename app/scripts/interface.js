/***

***/
define([
  "angularAMD",
  "routes",
  "materialize",
  "angular-ui-router",
  "angular-animate",
  "angular-cookies",
  "angular-resource",
  "angular-sanitize",
  "angular-touch",
  "angular-loading-bar"
], function (angularAMD, routes) {

  var
  appName = 'schedgmule',
  appDependencies = [
    "ui.router",
    "ngAnimate",
    "ngCookies",
    "ngResource",
    "ngSanitize",
    "ngTouch",
    "angular-loading-bar"
  ],
  app = angular.module(
    appName,
    appDependencies
  );

  function appInit ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }

  function httpIntercept ($http) {
    $http.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
  }

  app.factory(['$http', httpIntercept]);

  app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    //$httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = 'X-Requested-With';
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
  }]);

  app.run([
    '$rootScope',
    '$state',
    '$stateParams',
    appInit
  ]);

  app.config([
    '$httpProvider',
    '$locationProvider',
    '$stateProvider',
    '$urlRouterProvider',
    routes
  ]);

  app.config(function ($provide, $httpProvider) {

    $provide.factory('ApiControls', function ($q) {
      return {
        request: function (config) {
          /**
           * @param {Object} config A config object.
           */
          return config || $q.when(config);
        },

        requestError: function (rejection) {
          /**
           * @param {Object} rejection A failure object.
           */
          return $q.reject(rejection);
        },

        response: function (response) {
          /**
           * @param {Object} response A response object.
           */
          console.log(response);
          return response || $q.when(response);
        },

        responseError: function (rejection) {
          /**
           * @param {Object} rejection A failure object.
           */
          return $q.reject(rejection);
        }
      };
    });

    $httpProvider.interceptors.push('ApiControls');

  });

  app.config(function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
  });

  function AccountFactory ($resource) {
    return $resource('http://localhost:3000/v1/account/', null, {
      'update' : { method : 'PUT' }
    });
  }

  app.factory('Account', [
    '$resource',
    AccountFactory
  ]);

  var $body = angular.element(document);
  console.dir($body);
  return angularAMD.bootstrap(app, true, $body);
  //return angularAMD.bootstrap(app);
});
