/***
 # fileOverview

     _
    | |                 _
    | |__   ___   ___ _| |_
    |  _ \ / _ \ / _ (_   _)
    | |_) ) |_| | |_| || |_
    |____/ \___/ \___/  \__)

***/

require.config({

  'baseUrl': "../scripts/",

  'paths': {

    "materialize"          : "ext/materialize.min",
    "jquery"               : "ext/jquery.min",
    "hammerjs"             : "ext/hammer.min",

    "interface"            : "interface",
    "routes"               : "routes",

    "angular"              : "ext/angular",
    "angular-ui-router"    : "ext/angular-ui-router.min",
    "angular-loading-bar"  : "ext/loading-bar",
    "angularAMD"           : "ext/angularAMD.min",
    "ngload"               : "ext/ngload.min",
    "angular-animate"      : "ext/angular-animate",
    "angular-cookies"      : "ext/angular-cookies",
    "angular-resource"     : "ext/angular-resource",
    "angular-sanitize"     : "ext/angular-sanitize",
    "angular-touch"        : "ext/angular-touch",
    "angular-tree-control" : "ext/angular-tree-control/angular-tree-control",

    "bitcore"              : "ext/bitcore",
    "bitcore-ecies"        : "ext/bitcore-ecies",
    //"bitcore-lib"          : "ext/bitcore-lib",
    "bitcore-mnemonic"     : "ext/bitcore-mnemonic",

    "openchain-services"   : "ext/services",
    "openchain-models"     : "ext/models",

    "Long"       : "ext/long",
    "ByteBuffer" : "ext/ByteBufferAB",
    "ProtoBuf"   : "ext/ProtoBuf",
    "bitcoreW"   : "utils/bitcoreW",

    "IndexWpHeaderController"        : "modules/home/controllers/header",
    "SigninController"               : "modules/home/controllers/base",
    "MainController"                 : "modules/index/controllers/base",
    "SettingsController"             : "modules/account/controllers/settingsView",
    "RegisterAssetGroupController"         : "modules/assetGroup/controllers/detailView",
    "RegisterBaseModelController" : "modules/coring-sample/controllers/detailView",

    "lodash"     : "ext/lodash",
    "underscore" : "ext/underscore",
    "environment" : "environment/base",

    "account.directive" : "directives/account"

  },

  'shim': {
    "materialize": [
      'jquery'
    ],
    "angular-loading-bar": [
      "angular"
    ],
    "angular-ui-router": [
      "angular"
    ],
    "angularAMD": [
      "angular"
    ],
    "ngload": [
      "angularAMD"
    ],
    "angular-animate": [
      "angular"
    ],
    "angular-cookies": [
      "angular"
    ],
    "angular-resource": [
      "angular"
    ],
    "angular-sanitize": [
      "angular"
    ],
    "angular-touch": [
      "angular"
    ],
    "openchain-services": [
      "bitcore-mnemonic"
    ],
    "openchain-models": [
      "bitcore-mnemonic"
    ],
    "bitcore": [],
    "bitcoreW": ['bitcore'],
    "bitcore-ecies": [
      'bitcore'
    ],
    "bitcore-lib": [],
    "bitcore-mnemonic": [
      "bitcore"
    ],
    "lodash": {
      "exports": "_"
    },
    "interface": {
      "deps": [
        "ProtoBuf",
        "ByteBuffer",
        "Long"
      ]
    },
    "materialize": ['jquery']

  },

  'deps': ['interface']
});

