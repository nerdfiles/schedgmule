define([
  'angularAMD'
], function (__interface__) {
  var routes = function ($httpProvider, $locationProvider, $stateProvider, $urlRouterProvider) {

    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/logged-out');

    $stateProvider
      .state("signin-one", {
        url: "/",
        views: {
          'headerView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/header.html',
            controllerUrl : 'IndexWpHeaderController'
          }),
          'contentView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/home.html',
            controllerUrl : 'SigninController'
          })
        }
      });

    $stateProvider
      .state("main", {
        url: "/main",
        views: {
          'headerView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/header.html',
            controllerUrl : 'IndexWpHeaderController'
          }),
          'contentView': __interface__.route({
            templateUrl   : 'scripts/modules/index/partials/page.html',
            controllerUrl : 'MainController'
          })
        }
      });

    $stateProvider
      .state("settings", {
        url: "/settings",
        views: {
          'headerView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/header.html',
            controllerUrl : 'IndexWpHeaderController'
          }),
          'contentView': __interface__.route({
            templateUrl   : 'scripts/modules/account/partials/settings.html',
            controllerUrl : 'SettingsController'
          })
        }
      });

    $stateProvider
      .state("register-assetGroup", {
        url: "/register/assetGroup",
        views: {
          'headerView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/header.html',
            controllerUrl : 'IndexWpHeaderController'
          }),
          'contentView': __interface__.route({
            templateUrl   : 'scripts/modules/assetGroup/partials/detail.html',
            controllerUrl : 'RegisterAssetGroupController'
          })
        }
      });

    $stateProvider
      .state("register-base-model", {
        url: "/register/base-model",
        views: {
          'headerView': __interface__.route({
            templateUrl   : 'scripts/modules/home/partials/header.html',
            controllerUrl : 'IndexWpHeaderController'
          }),
          'contentView': __interface__.route({
            templateUrl   : 'scripts/modules/base-model/partials/detail.html',
            controllerUrl : 'RegisterBaseModelController'
          })
        }
      });

  };
  return routes;
});
