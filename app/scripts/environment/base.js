define([], function () {
  var configInterface = {};
  configInterface.controls = {};
  var baseUrl = 'http://localhost:3000/';
  configInterface.controls.account = baseUrl + 'v1/account/settings';
  return configInterface;
});
