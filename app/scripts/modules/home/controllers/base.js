define([
  "openchain-services",
  "openchain-models",
  "account.directive"
], function () {

  function HomeController ($scope, $timeout, $interval) {

    $scope.message = 'Welcome to schedgmule';

    tday=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
    tmonth=new Array("January","February","March","April","May","June","July","August","September","October","November","December");

    function GetClock (){
        var d=new Date();
        var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate();
        var nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;

        if(nhour==0){ap=" AM";nhour=12;}
        else if(nhour<12){ap=" AM";}
        else if(nhour==12){ap=" PM";}
        else if(nhour>12){ap=" PM";nhour-=12;}

        if(nmin<=9) nmin="0"+nmin;
        if(nsec<=9) nsec="0"+nsec;

        var clockboxd = document.getElementById('clockboxd');
        if ( clockboxd )
          clockboxd.innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+"";

        var clockbox = document.getElementById('clockbox');
        if ( clockbox )
          clockbox.innerHTML=""+nhour+":"+nmin+":"+nsec+ap+"";
    }

    GetClock();
    $interval(GetClock, 1000);

    angular.element.ready(function () {
      $('.btn-submit-lock').on('click', function (e) {
          e.preventDefault();
          $(this).text('Recognizing/Loading...');

          $timeout(function () {
              $(location).attr('href', "#");
          },'3000')
      });
    });

  }

  return [
    '$scope',
    '$timeout',
    '$interval',
    HomeController
  ];
});
