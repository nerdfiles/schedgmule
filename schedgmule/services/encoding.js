
/**
@fileOverview ./schedgmule/services/encoding.js
@description
Encoding services.
 */
var EncodingService, bytebuffer;

bytebuffer = require('bytebuffer');

EncodingService = (function() {
  function EncodingService() {}


  /**
  @interface EncodingService
   */

  EncodingService.prototype.encodeString = function(value) {

    /**
    @function
    @name EncodingService#encodeString
    @param {String} value An unencoded string.
     */
    return bytebuffer.wrap(value, 'utf8', true);
  };

  EncodingService.prototype.encodeRecordKey = function(path, type, name) {

    /**
    @function
    @name EncodingService#encodeRecordKey
    @param {String} path A path string.
    @param {String} type A type designation.
    @param {String} name A name label.
     */
    return this.encodeString(path + ':' + type + ':' + name);
  };

  EncodingService.prototype.encodeAccount = function(account, asset) {

    /**
    @function
    @name EncodingService#encodeAccount
    @param {String} account A root account key string.
    @param {Object} asset An asset construct.
     */
    return this.encodeRecordKey(account, 'ACC', asset);
  };

  EncodingService.prototype.encodeData = function(path, name) {

    /**
    @function
    @name EncodingService#encodeData
    @param {String} path A path string.
    @param {String} name A name string.
     */
    return this.encodeRecordKey(path, 'DATA', name);
  };

  EncodingService.prototype.encodeInt64 = function(value, usage) {

    /**
    @function
    @name EncodingService#encodeInt64
    @param {Number} value A numerical value string.
    @param {String} usage A string specifying usage.
     */
    var result;
    result = new bytebuffer(null, true);
    result.BE();
    result.writeInt64(value);
    result.flip();
    return result;
  };

  EncodingService.prototype.decodeInt64 = function(buffer) {

    /**
    @function
    @name EncodingService#decodeInt64
    @param {Buffer} buffer A buffer object to be read.
     */
    var result;
    buffer.BE();
    result = buffer.readInt64();
    buffer.flip();
    return result;
  };

  EncodingService.prototype.decodeString = function(buffer) {

    /**
    @function
    @name EncodingService#decodeString
    @param {Buffer} buffer A buffer object to be read.
     */
    var result;
    result = buffer.readUTF8String(buffer.remaining());
    buffer.flip();
    return result;
  };

  return EncodingService;

})();

module.exports = EncodingService;
