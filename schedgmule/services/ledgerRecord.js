
/**
@fileOverview ./schedgmule/services/ledgerRecord.js
@description
Ledger Record.
 */
var EncodingService, LedgerRecord, bytebuffer;

bytebuffer = require('bytebuffer');

EncodingService = require('./encoding');

LedgerRecord = (function() {
  LedgerRecord.prototype.path = null;

  LedgerRecord.prototype.recordType = null;

  LedgerRecord.prototype.name = null;

  function LedgerRecord() {

    /**
    @class LedgerRecord
     */
  }

  LedgerRecord.prototype.record = function(path, recordType, name) {
    return {
      path: path,
      recordType: recordType,
      name: name
    };
  };

  LedgerRecord.prototype.toString = function() {

    /**
    @memberof! LedgerRecord#toString
     */
    return this.record.path.toString() + ':' + this.record.recordType + ':' + this.record.name;
  };

  LedgerRecord.prototype.toByteBuffer = function() {

    /**
    @memberof! LedgerRecord#toByteBuffer
     */
    return bytebuffer.wrap(this.toString(), 'utf8', true);
  };

  LedgerRecord.prototype.parse = function(value) {

    /**
    @function
    @name LedgerRecord#parse
    @instance
     */
    var e, encodingService, ledgerRecord, parts, text;
    encodingService = new EncodingService;
    text = value;
    if (typeof text !== 'string') {
      text = encodingService.decodeString(text);
    }
    parts = text.split(':');
    if (parts.length < 1) {
      throw 'Invalid record key';
    }
    try {
      ledgerRecord = this.record(parts[0], parts[1], parts.slice(2, parts.length).join(':'));
    } catch (_error) {
      e = _error;
      throw e;
    }
    return ledgerRecord;
  };

  return LedgerRecord;

})();

module.exports = LedgerRecord;
