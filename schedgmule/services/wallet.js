
/**
@fileOverview ./schedgmule/services/wallet.js
@description
Wallet Settings.
 */
var Api, WalletSettings, _;

Api = require('./api');

_ = require('lodash');

WalletSettings = (function() {

  /**
  @class WalletSettings
   */
  WalletSettings.prototype.defaultAccount = {
    rawAddress: 'XjsApTFjTL63YTototE8rX3NF5aqRqcari',
    rootAddress: '/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/',
    initialized: false
  };

  WalletSettings.prototype.permissions = function() {
    var apiService;
    return apiService = new Api;
  };

  function WalletSettings(initAccount) {
    if (initAccount == null) {
      initAccount = null;
    }
    if (initAccount) {
      _.extend(this.defaultAccount, initAccount);
    }
    this;
  }

  return WalletSettings;

})();

module.exports = WalletSettings;
