###*
@fileOverview ./schedgmule/services/api.js
@description
Openchain API Services.
###

try
  $q = require('q')
catch e
  $q = require('Q')

_ = require('lodash')
bitcore = require('bitcore')
bytebuffer = require('bytebuffer')
long = require('long')
request = require('request')
isArray = require('../utils/isArray')
prettyjson = require('prettyjson')
options =
  noColor: true
EncodingService = require('./encoding')
LedgerRecord = require('./ledgerRecord')

class Api

  ###*
  @interface Api
  ###

  ###*
  @typedef RecordVersion
  @type {Object}
  @property {String} key ...
  @property {String} value ...
  @property {String} version ...
  ###

  ###*
  @typedef RecordMutations
  @type {Array<Object>}
  @property {String} mutation_hash ...
  ###

  ###*
  @typedef RecordsByName
  @type {Array<Object>}
  @property {String} key ...
  @property {String} value ...
  @property {String} version ...
  ###

  ###*
  @typedef AccountRecord
  @type {Object}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {String} account Root key of account.
  @property {Object} asset Asset construction.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
  ###

  ###*
  @typedef DataRecord
  @type {Object}
  @property {String} key Root Account Key.
  @property {String} recordKey Record key of DATA.
  @property {Object} data DATA construction.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
  ###

  ###*
  @typedef TransactionHash
  @type {Object}
  @property {String} transaction_hash The transaction_hash field contains the 
  hex-encoded hash of the full transaction.
  ###

  ###*
  @typedef Transaction
  @type {Object}
  @property {String} transaction_hash The transaction_hash field contains the 
  hex-encoded hash of the full transaction.
  @property {String} mutation_hash ...
  @property {Object} mutation ...
  @property {String} mutation.namespace ...
  @property {Array<Object>} mutation.records ...
  @property {String} mutation.records.key ...
  @property {String} mutation.records.value ...
  @property {String} mutation.records.version ...
  @property {String} timestamp ...
  @property {String} transaction_metadata ...
  ###

  ###*
  @typedef Record
  @type {Object}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {Number} value The new value that the record should have after 
  update. If it is unspecified, the record version is checked, but no update 
  is made to the value.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
  ###

  ###*
  @typedef Info
  @type {Object}
  @property {String} namespace is the hex representation of the namespace 
  expected in transactions submitted to the Openchain instance.
  ###

  ###*
  @typedef AccountAssets
  @type {Array<Object>}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {String} account The path of the record.
  @property {String} value The asset ID of the record (the record name).
  @property {String} balance The balance for that asset ID at that path.
  @property {String} version The hex-encoded version of the record. 
  @description
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
  ###

  ###*
  @typedef Subaccounts
  @type {Array<Object>}
  @property {String} key Encoded record key.
  @property {String} recordKey A value that uniquely identifies the record to 
  modify.
  @property {String} value The new value that the record should have after 
  update. If it is unspecified, the record version is checked, but no update 
  is made to the value.
  @property {String} version The last version of the record being updated. 
  @description
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
  ###


  loadAccount: (endpoint, account, asset) ->

    ###*
    Load ACC record.
    @function
    @name Api#loadAccount
    @param {String} endpoint An endpoint URL.
    @param {String} account An account string (rootAccount as Derived Key).
    @param {String} asset An asset string.
    @returns {AccountRecord}
    ###

    def = $q.defer()
    encodingService = new EncodingService

    @getRecord(endpoint, encodingService.encodeAccount(account, asset)).then((result) ->
      encodingService = new EncodingService

      accountRecord =
        key     : result.key
        account : account
        asset   : asset
        version : result.version

      if result.value.remaining() == 0
        # Unset value
        accountRecord.balance = long.ZERO
      else
        accountRecord.balance = encodingService.decodeInt64(result.value)
      def.resolve accountRecord
    , (err) ->
      def.reject error: err
    )

    def.promise


  loadData: (endpoint, path, name) ->

    ###*
    Load DATA record.
    @function
    @name Api#loadData
    @param {String} endpoint An endpoint URL.
    @param {String} path A path in the account hierarchy indicating where the record is situated.
    @param {String} name The name of the record.
    @returns {DataRecord}
    ###

    def = $q.defer()
    encodingService = new EncodingService

    @getRecord(endpoint, encodingService.encodeData(path, name)).then((result) ->

      try
        ledgerRecord = new LedgerRecord
      catch e
        console.log e

      dataRecord =
        key       : result.key
        recordKey : ledgerRecord.parse(result.key)
        version   : result.version

      try
        if result.value.remaining() == 0
          dataRecord.data = null
        else
          dataRecord.data = encodingService.decodeString(result.value)
      catch e
        throw e

      def.resolve dataRecord
    , (err) ->
      def.reject error: err
    )

    def.promise


  postTransaction: (endpoint, encodedTransaction, key) ->

    ###*
    @function
    @name Api#postTransaction
    @param {String} endpoint An endpoint string.
    @param {Object} encodedTransaction An encoded transaction object prepared from front end controller.
    @param {Object} key A key construct prepared by a front end controller.
    @returns {TransactionHash}
    ###

    def = $q.defer()

    rootUrl = endpoint
    urlClause = 'submit'

    try
      transactionBuffer = new Uint8Array(encodedTransaction.toArrayBuffer())
    catch e
      console.log e

    try
      hash = bitcore.crypto.Hash.sha256(bitcore.crypto.Hash.sha256(transactionBuffer))
    catch e
      console.log e

    if key and !key.privateKey
      console.log 'No private key provided.'
      def.reject error: 'No private key provided.'
    else

      baseUrl =  rootUrl + urlClause

      _privateKey = new bitcore.PrivateKey(key.privateKey)
      _publicKey = new bitcore.PublicKey(key.publicKey)

      try

        signatureBuffer = bitcore.crypto.ECDSA().set(
          hashbuf : hash
          endian  : 'big'
          privkey : _privateKey
        ).sign().sig.toBuffer()

      catch e
        #throw e
        def.reject error: e

      formConstruct =
        mutation  : encodedTransaction.toHex()
        signatures : [{
          pub_key   : bytebuffer.wrap(_publicKey.toBuffer()).toHex()
          signature : bytebuffer.wrap(signatureBuffer).toHex()
        }]

      console.log '================================'
      console.log baseUrl
      request.post
        url    : baseUrl
        json   : formConstruct
      , (error, response, body) ->

        if error
          def.reject error: error
          return

        def.resolve body

    def.promise


  getRecord: (endpoint, key) ->

    ###*
    @function
    @name Api#getRecord
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL for the ledger.
    @param {String} key The hex-encoded key of the record being queried.
    @returns {Record}
    ###

    def = $q.defer()

    try
      rootUrl = if isArray(endpoint) then (if !_.isString(endpoint) then endpoint else _.last(endpoint)) else endpoint.rootUrl
    catch e
      console.log e
      rootUrl = endpoint

    if rootUrl == undefined
      rootUrl = endpoint

    urlClause = 'record'
    baseUrl =  rootUrl + urlClause

    console.log baseUrl
    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        key: key.toHex()

    request(requestConfig, (error, response, body) ->

      if error
        console.log '=!'
        console.log error
        def.reject error: error
      else

        result = JSON.parse(body)

        record =
          #key     : key
          key     : bytebuffer.fromHex result.key
          value   : bytebuffer.fromHex(result.value)
          version : bytebuffer.fromHex(result.version)

        def.resolve record
    )

    def.promise


  getInfo: (endpoint) ->

    ###*
    @function
    @name Api#getInfo
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL for the ledger.
    @returns {Info}
    ###

    def = $q.defer()

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    urlClause = 'info'
    baseUrl =  rootUrl + urlClause

    requestConfig =
      url    : baseUrl
      method : 'get'

    request(requestConfig, (error, response, body) ->
      if error
        console.log error
        def.reject error: error
      else
        result = JSON.parse body
        info = _.extend {}, result
        def.resolve info
    )

    def.promise


  getAccountAssets: (endpoint, account) ->

    ###*
    Get Account Assets
    @description
    Do a lookup on the given account to see which assets for which it has 
    received issues. Typically these will be ACC records in the ledger under 
    `/p2pkh/` by default.
    @function
    @name Api#getAccountAssets
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL to pass into Openchain.
    @param {String} account A path to query for.
    @returns {AccountAssets}
    ###

    def = $q.defer()
    urlClause = 'query/account'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause
    encodingService = new EncodingService

    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        account: account

    request(requestConfig, (error, response, body) ->

      if error
        console.log error
        def.reject error: error
      else
        result = JSON.parse(body)

        if result.length == 0
          def.resolve []
          return

        accountAssets = _.map result, (item) ->
          encodedAccountKey = encodingService.encodeAccount(item.account, item.asset)
          accountAsset =
            key     : encodedAccountKey
            account : item.account
            asset   : item.asset
            balance : new long.fromString(item.balance)
            version : bytebuffer.fromHex(item.version)

        def.resolve accountAssets
    )

    def.promise


  getTransaction: (endpoint, mutation_hash, format='json') ->

    ###*
    @param {Object} endpoint ...
    @param {String} endpoint.rootUrl ...
    @param {String} mutation_hash ...
    @param {String} format (raw|json)
    @returns {Transaction}
    ###

    def = $q.defer()
    urlClause = 'query/transaction'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause
    encodingService = new EncodingService

    requestConfig =
      url : baseUrl
      method : 'get'
      qs:
        mutation_hash : mutation_hash
        format        : format

    request(requestConfig, (error, response, body) ->

      if error
        console.log error
        def.reject error: error
      else
        result = JSON.parse(body)
        transaction =
          transaction_hash     : bytebuffer.fromHex result.transaction_hash
          mutation_hash        : bytebuffer.fromHex result.mutation_hash
          timestamp            : bytebuffer.fromHex result.timestamp
          transaction_metadata : bytebuffer.fromHex result.transaction_metadata
          mutation:
            namespace : bytebuffer.fromHex(result.mutation.namespace)
            records   : _.map result.mutation.records, (record) ->
              _record =
                key     : bytebuffer.fromHex(record.key)
                value   : bytebuffer.fromHex(record.value)
                version : bytebuffer.fromHex(record.version)
        def.resolve transaction
    )

    def.promise


  getVersion: (endpoint, key) ->

    ###*
    Retrieve a specific version of a record.
    @returns {RecordVersion}
    ###

    def = $q.defer()
    urlClause = 'query/recordversion'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause
    #encodingService = new EncodingService

    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        key: key.toHex()

    request(requestConfig, (error, response, body) ->
      if error
        #console.log error
        def.reject error: error
      else
        result = JSON.parse(body)
        version =
          key     : bytebuffer.fromHex(result.key)
          value   : bytebuffer.fromHex(result.value)
          version : bytebuffer.fromHex(result.version)

        def.resolve version
    )

    def.promise


  getMutations: (endpoint, key) ->

    ###*
    Retrieve all the mutations that have affected a given record.
    @returns {RecordMutations}
    ###

    def = $q.defer()
    urlClause = 'query/recordmutations'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause

    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        key: key.toHex()

    request(requestConfig, (error, response, body) ->
      if error
        #console.log error
        def.reject error: error
      else
        mutations = []
        result = JSON.parse(body)
        for item in result
          mutation =
            mutation_hash: item.mutation_hash
          mutations.push mutation
        def.resolve mutations
    )

    def.promise


  getRecordsByName: (endpoint, name, type) ->

    ###*
    Retrieve all the mutations that have affected a given record.
    @param {Object} endpoint ...
    @param {String} endpoint.rootUrl ...
    @param {String} name ...
    @param {String} type ACC|DATA
    @returns {RecordsByName}
    ###

    def = $q.defer()
    urlClause = 'query/recordsbyname'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause

    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        name: name
        type: type

    request(requestConfig, (error, response, body) ->
      if error
        console.log error
        def.reject error: error
      else
        records = []
        result = JSON.parse(body)
        for item in result
          record =
            key     : bytebuffer.fromHex(item.key)
            value   : bytebuffer.fromHex(item.value)
            version : bytebuffer.fromHex(item.version)
          records.push record

        def.resolve records
    )

    def.promise


  getSubaccounts: (endpoint, account) ->

    ###*
    @function
    @name Api#getSubaccounts
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL to pass into Openchain.
    @param {String} account A path to query for.
    @returns {Subaccounts}
    ###

    def = $q.defer()
    ledgerRecord = new LedgerRecord
    urlClause = 'query/subaccounts'

    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    baseUrl =  rootUrl + urlClause
    requestConfig =
      url    : baseUrl
      method : 'get'
      qs     :
        account: account

    request(requestConfig, (error, response, body) ->

      if error
        def.reject error: error
      else
        subaccountRecords = []
        result = JSON.parse(body)
        for item in result
          key = bytebuffer.fromHex(result[item].key)
          subaccountRecord =
            key       : key
            recordKey : ledgerRecord.parse(key)
            value     : bytebuffer.fromHex(result[item].value)
            version   : bytebuffer.fromHex(result[item].version)
          subaccountRecords.push subaccountRecord

        def.resolve subaccountRecords
    )

    def.promise

module.exports = Api
