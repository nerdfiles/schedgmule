###*
@fileOverview ./schedgmule/services/ledgerPath.js
@description
Ledger Path.
###

class LedgerPath

  constructor: (parts) ->

    ###*
    @class LedgerPath
    ###

    @parts = parts

  toString: () ->

    ###*
    @memberof! LedgerPath#toString
    @returns {String} outputString A string that has been constructed to 
    represent the ledger path.
    ###

    '/' + @parts.map((item) ->
      item + '/'
    ).join('')

  parse: (value) ->

    ###*
    @function
    @name LedgerPath#parse
    @param {String} value A string construct for ledger path to be parsed.
    @instance
    ###

    if !value
      return

    parts = value.split('/')
    if parts.length < 2 or parts[0] != '' or parts[parts.length - 1] != ''
      throw 'Invalid path'
    new LedgerPath(parts.slice(1, parts.length - 1))

module.exports = LedgerPath
