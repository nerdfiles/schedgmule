###*
@fileOverview ./schedgmule/services/endpointManager.js
@description
Endpoint Manager.
###

try
  $q = require('q')
catch e
  $q = require('Q')

_ = require('lodash')
Account = require '../models/Account'
#Endpoint = require '../models/Endpoint'

WalletSettings = require('./wallet')
#walletSettings = new WalletSettings

EndpointService = require('./endpoint')

class EndpointManager

  ###*
  @class EndpointManager
  ###

  ###*
  @typedef Endpoint
  @param {String} rootUrl A root url.
  ###

  endpoints: null
  initialEndpoints: null
  constructor: (@endpointsList=null) ->

    @endpoints = {}
    storedEndpoints = @endpointsList

    if storedEndpoints
      @initialEndpoints = storedEndpoints
    else
      @initialEndpoints = []

    if @initialEndpoints and @initialEndpoints.length == 0
      throw 'Could not load initial endpoints.'
      return

    i = 0
    while i < @initialEndpoints.length
      a = new EndpointService(@initialEndpoints[i])
      @endpoints[@initialEndpoints[i]] = a
      i++

  loadEndpoints: () ->

    ###*
    @function
    @name EndpointManager#loadEndpoints
    ###

    $q.all(@initialEndpoints.map((url) ->
      @endpoints[url].loadEndpointInfo()
    ))

  addEndpoint: (endpoint) ->

    ###*
    @function
    @name EndpointManager#addEndpoint
    @param {Endpoint}
    ###

    @endpoints[endpoint.rootUrl] = endpoint
    @saveEndpoints()
    return

  saveEndpoints: () ->

    ###*
    @function
    @name EndpointManager#saveEndpoints
    ###

    endpoints = []
    for key of @endpoints
      endpoints.push @endpoints[key].rootUrl

    Account.findOne {
      _id: @uuid
    }, (err, instance) ->

      defaultEndpoint =
        meta:
          version: 'v2'
        endpoints: ['http://107.170.46.60:8080/']

      #defaultEndpoint =
        #meta:
          #version   : 'v2'
        #endpoints : endpoints

      instance.endpoint = defaultEndpoint
      instance.markModified 'endpoint'

      instance.save(() ->
        console.log "Saved endpoint data to Account."
        return
      )

      return
    return

module.exports = EndpointManager
