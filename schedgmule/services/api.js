
/**
@fileOverview ./schedgmule/services/api.js
@description
Openchain API Services.
 */
var $q, Api, EncodingService, LedgerRecord, _, bitcore, bytebuffer, e, isArray, long, options, prettyjson, request;

try {
  $q = require('q');
} catch (_error) {
  e = _error;
  $q = require('Q');
}

_ = require('lodash');

bitcore = require('bitcore');

bytebuffer = require('bytebuffer');

long = require('long');

request = require('request');

isArray = require('../utils/isArray');

prettyjson = require('prettyjson');

options = {
  noColor: true
};

EncodingService = require('./encoding');

LedgerRecord = require('./ledgerRecord');

Api = (function() {
  function Api() {}


  /**
  @interface Api
   */


  /**
  @typedef RecordVersion
  @type {Object}
  @property {String} key ...
  @property {String} value ...
  @property {String} version ...
   */


  /**
  @typedef RecordMutations
  @type {Array<Object>}
  @property {String} mutation_hash ...
   */


  /**
  @typedef RecordsByName
  @type {Array<Object>}
  @property {String} key ...
  @property {String} value ...
  @property {String} version ...
   */


  /**
  @typedef AccountRecord
  @type {Object}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {String} account Root key of account.
  @property {Object} asset Asset construction.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
   */


  /**
  @typedef DataRecord
  @type {Object}
  @property {String} key Root Account Key.
  @property {String} recordKey Record key of DATA.
  @property {Object} data DATA construction.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
   */


  /**
  @typedef TransactionHash
  @type {Object}
  @property {String} transaction_hash The transaction_hash field contains the 
  hex-encoded hash of the full transaction.
   */


  /**
  @typedef Transaction
  @type {Object}
  @property {String} transaction_hash The transaction_hash field contains the 
  hex-encoded hash of the full transaction.
  @property {String} mutation_hash ...
  @property {Object} mutation ...
  @property {String} mutation.namespace ...
  @property {Array<Object>} mutation.records ...
  @property {String} mutation.records.key ...
  @property {String} mutation.records.value ...
  @property {String} mutation.records.version ...
  @property {String} timestamp ...
  @property {String} transaction_metadata ...
   */


  /**
  @typedef Record
  @type {Object}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {Number} value The new value that the record should have after 
  update. If it is unspecified, the record version is checked, but no update 
  is made to the value.
  @property {String} version The last version of the record being updated. 
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
   */


  /**
  @typedef Info
  @type {Object}
  @property {String} namespace is the hex representation of the namespace 
  expected in transactions submitted to the Openchain instance.
   */


  /**
  @typedef AccountAssets
  @type {Array<Object>}
  @property {String} key A value that uniquely identifies the record to modify.
  @property {String} account The path of the record.
  @property {String} value The asset ID of the record (the record name).
  @property {String} balance The balance for that asset ID at that path.
  @property {String} version The hex-encoded version of the record. 
  @description
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
   */


  /**
  @typedef Subaccounts
  @type {Array<Object>}
  @property {String} key Encoded record key.
  @property {String} recordKey A value that uniquely identifies the record to 
  modify.
  @property {String} value The new value that the record should have after 
  update. If it is unspecified, the record version is checked, but no update 
  is made to the value.
  @property {String} version The last version of the record being updated. 
  @description
  Every modification of the record will cause the version to change. If the 
  version specified doesn’t match the actual version in the data store, then 
  the update fails.
   */

  Api.prototype.loadAccount = function(endpoint, account, asset) {

    /**
    Load ACC record.
    @function
    @name Api#loadAccount
    @param {String} endpoint An endpoint URL.
    @param {String} account An account string (rootAccount as Derived Key).
    @param {String} asset An asset string.
    @returns {AccountRecord}
     */
    var def, encodingService;
    def = $q.defer();
    encodingService = new EncodingService;
    this.getRecord(endpoint, encodingService.encodeAccount(account, asset)).then(function(result) {
      var accountRecord;
      encodingService = new EncodingService;
      accountRecord = {
        key: result.key,
        account: account,
        asset: asset,
        version: result.version
      };
      if (result.value.remaining() === 0) {
        accountRecord.balance = long.ZERO;
      } else {
        accountRecord.balance = encodingService.decodeInt64(result.value);
      }
      return def.resolve(accountRecord);
    }, function(err) {
      return def.reject({
        error: err
      });
    });
    return def.promise;
  };

  Api.prototype.loadData = function(endpoint, path, name) {

    /**
    Load DATA record.
    @function
    @name Api#loadData
    @param {String} endpoint An endpoint URL.
    @param {String} path A path in the account hierarchy indicating where the record is situated.
    @param {String} name The name of the record.
    @returns {DataRecord}
     */
    var def, encodingService;
    def = $q.defer();
    encodingService = new EncodingService;
    this.getRecord(endpoint, encodingService.encodeData(path, name)).then(function(result) {
      var dataRecord, ledgerRecord;
      try {
        ledgerRecord = new LedgerRecord;
      } catch (_error) {
        e = _error;
        console.log(e);
      }
      dataRecord = {
        key: result.key,
        recordKey: ledgerRecord.parse(result.key),
        version: result.version
      };
      try {
        if (result.value.remaining() === 0) {
          dataRecord.data = null;
        } else {
          dataRecord.data = encodingService.decodeString(result.value);
        }
      } catch (_error) {
        e = _error;
        throw e;
      }
      return def.resolve(dataRecord);
    }, function(err) {
      return def.reject({
        error: err
      });
    });
    return def.promise;
  };

  Api.prototype.postTransaction = function(endpoint, encodedTransaction, key) {

    /**
    @function
    @name Api#postTransaction
    @param {String} endpoint An endpoint string.
    @param {Object} encodedTransaction An encoded transaction object prepared from front end controller.
    @param {Object} key A key construct prepared by a front end controller.
    @returns {TransactionHash}
     */
    var _privateKey, _publicKey, baseUrl, def, formConstruct, hash, rootUrl, signatureBuffer, transactionBuffer, urlClause;
    def = $q.defer();
    rootUrl = endpoint;
    urlClause = 'submit';
    try {
      transactionBuffer = new Uint8Array(encodedTransaction.toArrayBuffer());
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    try {
      hash = bitcore.crypto.Hash.sha256(bitcore.crypto.Hash.sha256(transactionBuffer));
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    if (key && !key.privateKey) {
      console.log('No private key provided.');
      def.reject({
        error: 'No private key provided.'
      });
    } else {
      baseUrl = rootUrl + urlClause;
      _privateKey = new bitcore.PrivateKey(key.privateKey);
      _publicKey = new bitcore.PublicKey(key.publicKey);
      try {
        signatureBuffer = bitcore.crypto.ECDSA().set({
          hashbuf: hash,
          endian: 'big',
          privkey: _privateKey
        }).sign().sig.toBuffer();
      } catch (_error) {
        e = _error;
        def.reject({
          error: e
        });
      }
      formConstruct = {
        mutation: encodedTransaction.toHex(),
        signatures: [
          {
            pub_key: bytebuffer.wrap(_publicKey.toBuffer()).toHex(),
            signature: bytebuffer.wrap(signatureBuffer).toHex()
          }
        ]
      };
      console.log('================================');
      console.log(baseUrl);
      request.post({
        url: baseUrl,
        json: formConstruct
      }, function(error, response, body) {
        if (error) {
          def.reject({
            error: error
          });
          return;
        }
        return def.resolve(body);
      });
    }
    return def.promise;
  };

  Api.prototype.getRecord = function(endpoint, key) {

    /**
    @function
    @name Api#getRecord
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL for the ledger.
    @param {String} key The hex-encoded key of the record being queried.
    @returns {Record}
     */
    var baseUrl, def, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    try {
      rootUrl = isArray(endpoint) ? (!_.isString(endpoint) ? endpoint : _.last(endpoint)) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
      rootUrl = endpoint;
    }
    if (rootUrl === void 0) {
      rootUrl = endpoint;
    }
    urlClause = 'record';
    baseUrl = rootUrl + urlClause;
    console.log(baseUrl);
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        key: key.toHex()
      }
    };
    request(requestConfig, function(error, response, body) {
      var record, result;
      if (error) {
        console.log('=!');
        console.log(error);
        return def.reject({
          error: error
        });
      } else {
        result = JSON.parse(body);
        record = {
          key: bytebuffer.fromHex(result.key),
          value: bytebuffer.fromHex(result.value),
          version: bytebuffer.fromHex(result.version)
        };
        return def.resolve(record);
      }
    });
    return def.promise;
  };

  Api.prototype.getInfo = function(endpoint) {

    /**
    @function
    @name Api#getInfo
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL for the ledger.
    @returns {Info}
     */
    var baseUrl, def, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    urlClause = 'info';
    baseUrl = rootUrl + urlClause;
    requestConfig = {
      url: baseUrl,
      method: 'get'
    };
    request(requestConfig, function(error, response, body) {
      var info, result;
      if (error) {
        console.log(error);
        return def.reject({
          error: error
        });
      } else {
        result = JSON.parse(body);
        info = _.extend({}, result);
        return def.resolve(info);
      }
    });
    return def.promise;
  };

  Api.prototype.getAccountAssets = function(endpoint, account) {

    /**
    Get Account Assets
    @description
    Do a lookup on the given account to see which assets for which it has 
    received issues. Typically these will be ACC records in the ledger under 
    `/p2pkh/` by default.
    @function
    @name Api#getAccountAssets
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL to pass into Openchain.
    @param {String} account A path to query for.
    @returns {AccountAssets}
     */
    var baseUrl, def, encodingService, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    urlClause = 'query/account';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    encodingService = new EncodingService;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        account: account
      }
    };
    request(requestConfig, function(error, response, body) {
      var accountAssets, result;
      if (error) {
        console.log(error);
        return def.reject({
          error: error
        });
      } else {
        result = JSON.parse(body);
        if (result.length === 0) {
          def.resolve([]);
          return;
        }
        accountAssets = _.map(result, function(item) {
          var accountAsset, encodedAccountKey;
          encodedAccountKey = encodingService.encodeAccount(item.account, item.asset);
          return accountAsset = {
            key: encodedAccountKey,
            account: item.account,
            asset: item.asset,
            balance: new long.fromString(item.balance),
            version: bytebuffer.fromHex(item.version)
          };
        });
        return def.resolve(accountAssets);
      }
    });
    return def.promise;
  };

  Api.prototype.getTransaction = function(endpoint, mutation_hash, format) {
    var baseUrl, def, encodingService, requestConfig, rootUrl, urlClause;
    if (format == null) {
      format = 'json';
    }

    /**
    @param {Object} endpoint ...
    @param {String} endpoint.rootUrl ...
    @param {String} mutation_hash ...
    @param {String} format (raw|json)
    @returns {Transaction}
     */
    def = $q.defer();
    urlClause = 'query/transaction';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    encodingService = new EncodingService;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        mutation_hash: mutation_hash,
        format: format
      }
    };
    request(requestConfig, function(error, response, body) {
      var result, transaction;
      if (error) {
        console.log(error);
        return def.reject({
          error: error
        });
      } else {
        result = JSON.parse(body);
        transaction = {
          transaction_hash: bytebuffer.fromHex(result.transaction_hash),
          mutation_hash: bytebuffer.fromHex(result.mutation_hash),
          timestamp: bytebuffer.fromHex(result.timestamp),
          transaction_metadata: bytebuffer.fromHex(result.transaction_metadata),
          mutation: {
            namespace: bytebuffer.fromHex(result.mutation.namespace),
            records: _.map(result.mutation.records, function(record) {
              var _record;
              return _record = {
                key: bytebuffer.fromHex(record.key),
                value: bytebuffer.fromHex(record.value),
                version: bytebuffer.fromHex(record.version)
              };
            })
          }
        };
        return def.resolve(transaction);
      }
    });
    return def.promise;
  };

  Api.prototype.getVersion = function(endpoint, key) {

    /**
    Retrieve a specific version of a record.
    @returns {RecordVersion}
     */
    var baseUrl, def, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    urlClause = 'query/recordversion';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        key: key.toHex()
      }
    };
    request(requestConfig, function(error, response, body) {
      var result, version;
      if (error) {
        return def.reject({
          error: error
        });
      } else {
        result = JSON.parse(body);
        version = {
          key: bytebuffer.fromHex(result.key),
          value: bytebuffer.fromHex(result.value),
          version: bytebuffer.fromHex(result.version)
        };
        return def.resolve(version);
      }
    });
    return def.promise;
  };

  Api.prototype.getMutations = function(endpoint, key) {

    /**
    Retrieve all the mutations that have affected a given record.
    @returns {RecordMutations}
     */
    var baseUrl, def, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    urlClause = 'query/recordmutations';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        key: key.toHex()
      }
    };
    request(requestConfig, function(error, response, body) {
      var i, item, len, mutation, mutations, result;
      if (error) {
        return def.reject({
          error: error
        });
      } else {
        mutations = [];
        result = JSON.parse(body);
        for (i = 0, len = result.length; i < len; i++) {
          item = result[i];
          mutation = {
            mutation_hash: item.mutation_hash
          };
          mutations.push(mutation);
        }
        return def.resolve(mutations);
      }
    });
    return def.promise;
  };

  Api.prototype.getRecordsByName = function(endpoint, name, type) {

    /**
    Retrieve all the mutations that have affected a given record.
    @param {Object} endpoint ...
    @param {String} endpoint.rootUrl ...
    @param {String} name ...
    @param {String} type ACC|DATA
    @returns {RecordsByName}
     */
    var baseUrl, def, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    urlClause = 'query/recordsbyname';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        name: name,
        type: type
      }
    };
    request(requestConfig, function(error, response, body) {
      var i, item, len, record, records, result;
      if (error) {
        console.log(error);
        return def.reject({
          error: error
        });
      } else {
        records = [];
        result = JSON.parse(body);
        for (i = 0, len = result.length; i < len; i++) {
          item = result[i];
          record = {
            key: bytebuffer.fromHex(item.key),
            value: bytebuffer.fromHex(item.value),
            version: bytebuffer.fromHex(item.version)
          };
          records.push(record);
        }
        return def.resolve(records);
      }
    });
    return def.promise;
  };

  Api.prototype.getSubaccounts = function(endpoint, account) {

    /**
    @function
    @name Api#getSubaccounts
    @param {Object} endpoint An endpoint dictionary.
    @param {String} endpoint.rootUrl A root URL to pass into Openchain.
    @param {String} account A path to query for.
    @returns {Subaccounts}
     */
    var baseUrl, def, ledgerRecord, requestConfig, rootUrl, urlClause;
    def = $q.defer();
    ledgerRecord = new LedgerRecord;
    urlClause = 'query/subaccounts';
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    baseUrl = rootUrl + urlClause;
    requestConfig = {
      url: baseUrl,
      method: 'get',
      qs: {
        account: account
      }
    };
    request(requestConfig, function(error, response, body) {
      var i, item, key, len, result, subaccountRecord, subaccountRecords;
      if (error) {
        return def.reject({
          error: error
        });
      } else {
        subaccountRecords = [];
        result = JSON.parse(body);
        for (i = 0, len = result.length; i < len; i++) {
          item = result[i];
          key = bytebuffer.fromHex(result[item].key);
          subaccountRecord = {
            key: key,
            recordKey: ledgerRecord.parse(key),
            value: bytebuffer.fromHex(result[item].value),
            version: bytebuffer.fromHex(result[item].version)
          };
          subaccountRecords.push(subaccountRecord);
        }
        return def.resolve(subaccountRecords);
      }
    });
    return def.promise;
  };

  return Api;

})();

module.exports = Api;
