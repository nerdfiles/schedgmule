
/**
@fileOverview ./schedgmule/services/transactionBuilder.js
@description
Transaction Builder Service.
 */
var $q, Api, EncodingService, ProtobufBuilder, TransactionBuilder, TransactionManager, TransactionSession, _, bytebuffer, e, isArray, long;

try {
  $q = require('q');
} catch (_error) {
  e = _error;
  $q = require('Q');
}

EncodingService = require('./encoding');

ProtobufBuilder = require('./protobufBuilder');

Api = require('./api');

long = require('long');

bytebuffer = require('bytebuffer');

isArray = require('../utils/isArray');

_ = require('lodash');


/**
@class TransactionManager
@description
Transaction Manager is an observer pattern for
 */

TransactionManager = (function() {
  TransactionManager.prototype.logged = null;

  function TransactionManager(logging) {
    if (logging == null) {
      logging = [];
    }
    this.logged = logging;
  }

  TransactionManager.prototype.notify = function(item) {
    var subscriber;
    return $q.all((function() {
      var i, len, ref, results;
      ref = this.logged;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        subscriber = ref[i];
        if (subscriber.item === item) {
          results.push(subscriber.callback(item));
        }
      }
      return results;
    }).call(this)).done(function(d) {
      return console.log(d);
    });
  };

  TransactionManager.prototype.introspect = function(to, onNew) {
    return this.logged.push({
      'item': to,
      'callback': onNew
    });
  };

  return TransactionManager;

})();

TransactionSession = (function() {
  function TransactionSession() {}

  TransactionSession.prototype.pipe = function() {
    return function(item) {
      var def;
      def = $q.defer();
      console.dir(item);
      def.resolve(item);
      return def.promise;
    };
  };

  return TransactionSession;

})();

TransactionBuilder = (function() {
  TransactionBuilder.prototype.storedTransactions = [];

  TransactionBuilder.prototype.endpoint = null;

  TransactionBuilder.prototype.records = null;

  TransactionBuilder.prototype.logged = [];

  function TransactionBuilder(_endpoint) {

    /**
    Creates a new Transaction.
    @class TransactionBuilder
    @param {String} endpoint An endpoint base URL.
     */
    var endpoint, rootUrl;
    endpoint = _endpoint;
    try {
      rootUrl = isArray(endpoint) ? _.last(endpoint) : endpoint.rootUrl;
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    console.log(rootUrl);
    this.endpoint = rootUrl;
    this.records = [];
  }

  TransactionBuilder.prototype.addRecord = function(key, value, version) {

    /**
    @memberof TransactionBuilder#
    @description
    Add Record.
    @param {String} key A key for the given asset.
    @param {Number} value A number value.
    @param {String} version A version for the record.
     */
    var label, newRecord, recordNamespace;
    this.manager = new TransactionManager;
    this.logger = new TransactionSession('==================================== addRecord');
    newRecord = {
      key: key,
      version: version
    };
    if (value !== null) {
      newRecord.value = {
        data: value
      };
    } else {
      newRecord.value = null;
    }
    label = "Record Check " + (newRecord.version.toString());
    recordNamespace = label;
    this.manager.introspect(recordNamespace, this.logger.pipe(newRecord));
    this.records.push(newRecord);
    return this;
  };

  TransactionBuilder.prototype.addAccountRecord = function(previous, change) {

    /**
    @memberof TransactionBuilder#
    @description
    Add Account Record.
    @param {Object} previous A previous transaction record mutation object.
    @param {Object} previous.key The public key of the previous transaction 
    record mutation.
    @param {Object} change A change object.
    @returns {Object} record A record object.
     */
    var balance, encodingService, k, p, record, updatedBalance, v;
    encodingService = new EncodingService;
    if (!previous) {
      console.log('No previous record provided.');
      return;
    }
    if (!previous.key) {
      console.log('No key provided on previous record.');
      return;
    }
    balance = new long(previous.balance.low, previous.balance.high);
    console.log('========================');
    console.log(balance);
    updatedBalance = balance.add(change);
    try {
      k = previous.key;
      p = encodingService.encodeInt64(updatedBalance);
      v = previous.version;
    } catch (_error) {
      e = _error;
      console.log(e);
      throw e;
    }
    try {
      record = this.addRecord(k, p, v);
    } catch (_error) {
      e = _error;
      console.log(e);
      throw e;
    }
    return record;
  };

  TransactionBuilder.prototype.fetchAndAddAccountRecord = function(account, asset, change) {

    /**
    @memberof TransactionBuilder#
    @description
    Resolve name accounts
    @param {String} account A root account key.
    @param {Object} asset An asset object (typically from asset definition download).
    @param {Object} change A change object.
     */
    var apiService;
    if (account.slice(0, 1) === '@') {
      account = '/aka/' + account.slice(1, account.length) + '/';
    }
    apiService = new Api;
    return apiService.loadData(this.endpoint, account, 'goto').then((function(_this) {
      return function(result) {
        if (result.data === null) {
          return account;
        } else {
          _this.addRecord(result.key, null, result.version);
          return result.data;
        }
      };
    })(this)).then((function(_this) {
      return function(accountResult) {
        var l;
        try {
          l = apiService.loadAccount(_this.endpoint, accountResult, asset);
        } catch (_error) {
          e = _error;
          console.log(e);
          throw e;
        }
        return l;
      };
    })(this)).then((function(_this) {
      return function(currentRecord) {
        _this.addAccountRecord(currentRecord, change);
      };
    })(this));
  };

  TransactionBuilder.prototype.submit = function(key, metadataConstruct, privateKey, publicKey) {

    /**
    @memberof TransactionBuilder#
    @description
    Submit.
    @param {String} key A root account key.
     */
    var _key, _metadata, _url, apiService, constructedTransaction, def, encodingService, nr, protobufBuilder;
    _key = {
      key: key,
      privateKey: privateKey,
      publicKey: publicKey
    };
    def = $q.defer();
    protobufBuilder = new ProtobufBuilder;
    encodingService = new EncodingService;
    apiService = new Api;
    try {
      _metadata = bytebuffer.fromHex('');
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    try {
      console.log('============================= TRY ENCODING');
      console.log(this.endpoint);
      _url = encodingService.encodeString(this.endpoint);
      console.log(_url);
    } catch (_error) {
      e = _error;
      console.log(e);
    }
    nr = this.records;
    console.log(nr);
    try {
      constructedTransaction = new protobufBuilder.Mutation({
        namespace: _url,
        records: nr,
        metadata: _metadata
      });
      console.dir(constructedTransaction);
      console.log(JSON.stringify(constructedTransaction));
    } catch (_error) {
      e = _error;
      console.log(e);
      throw e;
    }
    try {
      apiService.postTransaction(this.endpoint, constructedTransaction.encode(), _key).then(function(d) {
        return def.resolve(d);
      }, function(error) {
        console.log('====================== ?');
        return def.reject({
          error: error
        });
      });
    } catch (_error) {
      e = _error;
      console.log(e);
      throw e;
    }
    return def.promise;
  };

  TransactionBuilder.prototype.insert = function(key) {

    /**
    Return this in response. Client is ready to submit transaction.
    @memberof TransactionBuilder#
    @param {String} key A root account key.
    @returns {Object} transactionConstruct A transaction construct to be submitted.
     */
    var transactionConstruct;
    this.storedTransactions.push(this);
    return transactionConstruct = {
      transaction: _.last(this.storedTransactions),
      key: key
    };
  };

  TransactionBuilder.prototype.error = function() {

    /**
    UI Error on Transaction
    @memberof TransactionBuilder#
    @returns {Object} error An error object for display.
     */
    var error;
    this.storedTransactions = [];
    return error = {
      error: "Invalid transaction."
    };
  };

  return TransactionBuilder;

})();

module.exports = TransactionBuilder;
