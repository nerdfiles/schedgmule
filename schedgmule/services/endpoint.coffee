###
@fileOverview ./schedgmule/services/endpoint.js
@description
Endpoint model.
###

try
  $q = require('q')
catch e
  $q = require('Q')

EncodingService = require('./encoding')

Api = require('./api')
bytebuffer = require('bytebuffer')

class Endpoint

  ###*
  @class Endpoint
  ###

  properties : null
  assets     : null
  rootUrl    : null

  constructor: (endpointUrl=null) ->

    ###*
    @constructs Endpoint
    ###

    @properties = {}
    @assets = {}
    @rootUrl = endpointUrl
    @

  loadEndpointInfo: (url) ->

    ###*
    @memberof Endpoint
    @description
    Load Endpoint Info
    ###

    apiService = new Api
    apiService.getData(@, "/", "info").then((result) ->
      if result.data == null
        @properties = {}
      else
        properties = JSON.parse(result.data)
        @properties =
          name         : properties.name
          validatorUrl : properties.validator_url
          tos          : properties.tos
          webpageUrl   : properties.webpage_url
    , () ->
      @properties = {}
    )

  downloadAssetDefinition: (assetPath) ->

    ###*
    @memberof Endpoint
    @description
    Download Asset Definition
    ###

    encodingService = new EncodingService
    apiService = new Api
    def = $q.defer()
    apiService.getRecord(@, encodingService.encodeData(assetPath, "asdef")).then((result) ->
      if result.value.remaining() == 0
        a =
          key     : result.key,
          value   : null,
          version : result.version
      else
        a =
          key     : result.key,
          value   : JSON.parse(encodingService.decodeString(result.value)),
          version : result.version
      def.resolve a
    )
    def.promise

  getAssetDefinition: (assetPath, noCache) ->

    ###*
    @memberof Endpoint
    @description
    Get Asset Definition
    ###

    def = $q.defer()
    if !noCache && assetPath in @assets
      def.resolve(@assets[assetPath])
    else
      @downloadAssetDefinition(assetPath).then((result) =>

        if result.value != null
          assetInfo =
            name      : result.value.name
            nameShort : result.value.name_short
            iconUrl   : result.value.icon_url
            path      : assetPath
            key       : result.key
            version   : result.version
        else
          assetInfo =
            version: result.version

        @assets[assetPath] = assetInfo

        def.resolve(assetInfo)
      )

    def.promise

module.exports = Endpoint
