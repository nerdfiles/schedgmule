###*
@fileOverview ./schedgmule/services/validator.js
@description
Data Defender Validators.
###

class Validator

  ###*
  @interface Validator
  ###

  isNumber: (number) ->

    ###*
    @function
    @name Validator#isNumber
    ###

    regex = /^[\-]?\d+(\.\d+)?$/
    regex.test(number)

module.exports = Validator
