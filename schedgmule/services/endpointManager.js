
/**
@fileOverview ./schedgmule/services/endpointManager.js
@description
Endpoint Manager.
 */
var $q, Account, EndpointManager, EndpointService, WalletSettings, _, e;

try {
  $q = require('q');
} catch (_error) {
  e = _error;
  $q = require('Q');
}

_ = require('lodash');

Account = require('../models/Account');

WalletSettings = require('./wallet');

EndpointService = require('./endpoint');

EndpointManager = (function() {

  /**
  @class EndpointManager
   */

  /**
  @typedef Endpoint
  @param {String} rootUrl A root url.
   */
  EndpointManager.prototype.endpoints = null;

  EndpointManager.prototype.initialEndpoints = null;

  function EndpointManager(endpointsList) {
    var a, i, storedEndpoints;
    this.endpointsList = endpointsList != null ? endpointsList : null;
    this.endpoints = {};
    storedEndpoints = this.endpointsList;
    if (storedEndpoints) {
      this.initialEndpoints = storedEndpoints;
    } else {
      this.initialEndpoints = [];
    }
    if (this.initialEndpoints && this.initialEndpoints.length === 0) {
      throw 'Could not load initial endpoints.';
      return;
    }
    i = 0;
    while (i < this.initialEndpoints.length) {
      a = new EndpointService(this.initialEndpoints[i]);
      this.endpoints[this.initialEndpoints[i]] = a;
      i++;
    }
  }

  EndpointManager.prototype.loadEndpoints = function() {

    /**
    @function
    @name EndpointManager#loadEndpoints
     */
    return $q.all(this.initialEndpoints.map(function(url) {
      return this.endpoints[url].loadEndpointInfo();
    }));
  };

  EndpointManager.prototype.addEndpoint = function(endpoint) {

    /**
    @function
    @name EndpointManager#addEndpoint
    @param {Endpoint}
     */
    this.endpoints[endpoint.rootUrl] = endpoint;
    this.saveEndpoints();
  };

  EndpointManager.prototype.saveEndpoints = function() {

    /**
    @function
    @name EndpointManager#saveEndpoints
     */
    var endpoints, key;
    endpoints = [];
    for (key in this.endpoints) {
      endpoints.push(this.endpoints[key].rootUrl);
    }
    Account.findOne({
      _id: this.uuid
    }, function(err, instance) {
      var defaultEndpoint;
      defaultEndpoint = {
        meta: {
          version: 'v2'
        },
        endpoints: ['http://107.170.46.60:8080/']
      };
      instance.endpoint = defaultEndpoint;
      instance.markModified('endpoint');
      instance.save(function() {
        console.log("Saved endpoint data to Account.");
      });
    });
  };

  return EndpointManager;

})();

module.exports = EndpointManager;
