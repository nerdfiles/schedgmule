###
@fileOverview ./schedgmule/services/assetData.js
@description
Asset Data services.
###

try
  $q = require('q')
catch e
  $q = require('Q')

LedgerPath = require('./ledgerPath')

class AssetData

  endpoint        : null
  assetPath       : null
  fullPath        : null
  currentRecord   : null
  assetDefinition : null

  constructor: (endpoint, assetPath) ->

    ###*
    @constructs AssetData
    @param {String} endpoint An endpoint base URL.
    @param {String} assetPath An asset path public key address.
    ###

    ledgerPath = new LedgerPath

    @endpoint = endpoint
    @assetPath = ledgerPath.parse(assetPath)
    position = 1
    orientation = endpoint.rootUrl.length - 1

    if endpoint.rootUrl.slice(orientation, endpoint.rootUrl.length) == '/'
      @fullPath = endpoint.rootUrl + assetPath.slice(position, assetPath.length)
    else
      @fullPath = endpoint.rootUrl + assetPath
    @


  setAccountBalance: (balanceData) ->

    ###*
    @memberof AssetData#setAccountBalance
    @param {Object} balanceData A balance object to set.
    ###

    @currentRecord = balanceData
    return


  fetchAssetDefinition: () ->

    ###*
    @memberof AssetData#fetchAssetDefinition
    @description
    Prepares the AssetData instance.
    ###

    def = $q.defer()
    @endpoint.getAssetDefinition(@assetPath).then (result) =>
      @assetDefinition = result
      def.resolve(@assetDefinition)
    def.promise


module.exports = AssetData
