###*
@fileOverview ./schedgmule/services/protobufBuilder.js
@description
ProtoBuf Builder.
@usage

var constructedTransaction = new ProtobufBuilder.Mutation({
    "namespace" : encodingService.encodeString(_this.endpoint.rootUrl),
    "records"   : _this.records,
    "metadata"  : ByteBuffer.fromHex("")
});

apiService.postTransaction(_this.endpoint, constructedTransaction.encode(), key);
###

path = require 'path'
ProtoBuf = require('protobufjs')

class ProtobufBuilder

  ###*
  @class ProtobufBuilder
  ###

  Mutation: null
  Transaction: null

  constructor: () ->
    builder = ProtoBuf.loadProtoFile(path.join(__dirname, '/content/schema.proto'))

    try
      root = builder.build()
    catch e
      console.log '================ ProtoBuf construction'
      console.log e

    try
      @Mutation = root.Openchain.Mutation
      @Transaction = root.Openchain.Transaction
    catch e
      console.log '================ Openchain Mutation and Transaction'
      console.log e
    return

module.exports = ProtobufBuilder
