
/*
@fileOverview ./schedgmule/services/endpoint.js
@description
Endpoint model.
 */
var $q, Api, EncodingService, Endpoint, bytebuffer, e,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

try {
  $q = require('q');
} catch (_error) {
  e = _error;
  $q = require('Q');
}

EncodingService = require('./encoding');

Api = require('./api');

bytebuffer = require('bytebuffer');

Endpoint = (function() {

  /**
  @class Endpoint
   */
  Endpoint.prototype.properties = null;

  Endpoint.prototype.assets = null;

  Endpoint.prototype.rootUrl = null;

  function Endpoint(endpointUrl) {
    if (endpointUrl == null) {
      endpointUrl = null;
    }

    /**
    @constructs Endpoint
     */
    this.properties = {};
    this.assets = {};
    this.rootUrl = endpointUrl;
    this;
  }

  Endpoint.prototype.loadEndpointInfo = function(url) {

    /**
    @memberof Endpoint
    @description
    Load Endpoint Info
     */
    var apiService;
    apiService = new Api;
    return apiService.getData(this, "/", "info").then(function(result) {
      var properties;
      if (result.data === null) {
        return this.properties = {};
      } else {
        properties = JSON.parse(result.data);
        return this.properties = {
          name: properties.name,
          validatorUrl: properties.validator_url,
          tos: properties.tos,
          webpageUrl: properties.webpage_url
        };
      }
    }, function() {
      return this.properties = {};
    });
  };

  Endpoint.prototype.downloadAssetDefinition = function(assetPath) {

    /**
    @memberof Endpoint
    @description
    Download Asset Definition
     */
    var apiService, def, encodingService;
    encodingService = new EncodingService;
    apiService = new Api;
    def = $q.defer();
    apiService.getRecord(this, encodingService.encodeData(assetPath, "asdef")).then(function(result) {
      var a;
      if (result.value.remaining() === 0) {
        a = {
          key: result.key,
          value: null,
          version: result.version
        };
      } else {
        a = {
          key: result.key,
          value: JSON.parse(encodingService.decodeString(result.value)),
          version: result.version
        };
      }
      return def.resolve(a);
    });
    return def.promise;
  };

  Endpoint.prototype.getAssetDefinition = function(assetPath, noCache) {

    /**
    @memberof Endpoint
    @description
    Get Asset Definition
     */
    var def;
    def = $q.defer();
    if (!noCache && indexOf.call(this.assets, assetPath) >= 0) {
      def.resolve(this.assets[assetPath]);
    } else {
      this.downloadAssetDefinition(assetPath).then((function(_this) {
        return function(result) {
          var assetInfo;
          if (result.value !== null) {
            assetInfo = {
              name: result.value.name,
              nameShort: result.value.name_short,
              iconUrl: result.value.icon_url,
              path: assetPath,
              key: result.key,
              version: result.version
            };
          } else {
            assetInfo = {
              version: result.version
            };
          }
          _this.assets[assetPath] = assetInfo;
          return def.resolve(assetInfo);
        };
      })(this));
    }
    return def.promise;
  };

  return Endpoint;

})();

module.exports = Endpoint;
