###*
@fileOverview ./schedgmule/services/wallet.js
@description
Wallet Settings.
###

Api = require './api'
_ = require('lodash')

class WalletSettings

  ###*
  @class WalletSettings
  ###

  defaultAccount:
    rawAddress  : 'XjsApTFjTL63YTototE8rX3NF5aqRqcari'
    rootAddress : '/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/'
    initialized : false

  permissions : () ->

    apiService = new Api

  constructor : (initAccount=null) ->

    if initAccount
      _.extend @defaultAccount, initAccount
    @

module.exports = WalletSettings
