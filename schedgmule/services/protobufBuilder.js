
/**
@fileOverview ./schedgmule/services/protobufBuilder.js
@description
ProtoBuf Builder.
@usage

var constructedTransaction = new ProtobufBuilder.Mutation({
    "namespace" : encodingService.encodeString(_this.endpoint.rootUrl),
    "records"   : _this.records,
    "metadata"  : ByteBuffer.fromHex("")
});

apiService.postTransaction(_this.endpoint, constructedTransaction.encode(), key);
 */
var ProtoBuf, ProtobufBuilder, path;

path = require('path');

ProtoBuf = require('protobufjs');

ProtobufBuilder = (function() {

  /**
  @class ProtobufBuilder
   */
  ProtobufBuilder.prototype.Mutation = null;

  ProtobufBuilder.prototype.Transaction = null;

  function ProtobufBuilder() {
    var builder, e, root;
    builder = ProtoBuf.loadProtoFile(path.join(__dirname, '/content/schema.proto'));
    try {
      root = builder.build();
    } catch (_error) {
      e = _error;
      console.log('================ ProtoBuf construction');
      console.log(e);
    }
    try {
      this.Mutation = root.Openchain.Mutation;
      this.Transaction = root.Openchain.Transaction;
    } catch (_error) {
      e = _error;
      console.log('================ Openchain Mutation and Transaction');
      console.log(e);
    }
    return;
  }

  return ProtobufBuilder;

})();

module.exports = ProtobufBuilder;
