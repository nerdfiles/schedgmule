###*
@fileOverview ./schedgmule/services/encoding.js
@description
Encoding services.
###

bytebuffer = require('bytebuffer')

class EncodingService

  ###*
  @interface EncodingService
  ###

  encodeString: (value) ->

    ###*
    @function
    @name EncodingService#encodeString
    @param {String} value An unencoded string.
    ###

    bytebuffer.wrap value, 'utf8', true

  encodeRecordKey: (path, type, name) ->

    ###*
    @function
    @name EncodingService#encodeRecordKey
    @param {String} path A path string.
    @param {String} type A type designation.
    @param {String} name A name label.
    ###

    @encodeString path + ':' + type + ':' + name

  encodeAccount: (account, asset) ->

    ###*
    @function
    @name EncodingService#encodeAccount
    @param {String} account A root account key string.
    @param {Object} asset An asset construct.
    ###

    @encodeRecordKey account, 'ACC', asset

  encodeData: (path, name) ->

    ###*
    @function
    @name EncodingService#encodeData
    @param {String} path A path string.
    @param {String} name A name string.
    ###

    @encodeRecordKey path, 'DATA', name

  encodeInt64: (value, usage) ->

    ###*
    @function
    @name EncodingService#encodeInt64
    @param {Number} value A numerical value string.
    @param {String} usage A string specifying usage.
    ###

    result = new bytebuffer(null, true)
    result.BE()
    result.writeInt64 value
    result.flip()
    result

  decodeInt64: (buffer) ->

    ###*
    @function
    @name EncodingService#decodeInt64
    @param {Buffer} buffer A buffer object to be read.
    ###

    buffer.BE()
    result = buffer.readInt64()
    buffer.flip()
    result

  decodeString: (buffer) ->

    ###*
    @function
    @name EncodingService#decodeString
    @param {Buffer} buffer A buffer object to be read.
    ###

    result = buffer.readUTF8String(buffer.remaining())
    buffer.flip()
    result

module.exports = EncodingService
