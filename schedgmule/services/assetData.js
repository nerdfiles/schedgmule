
/*
@fileOverview ./schedgmule/services/assetData.js
@description
Asset Data services.
 */
var $q, AssetData, LedgerPath, e;

try {
  $q = require('q');
} catch (_error) {
  e = _error;
  $q = require('Q');
}

LedgerPath = require('./ledgerPath');

AssetData = (function() {
  AssetData.prototype.endpoint = null;

  AssetData.prototype.assetPath = null;

  AssetData.prototype.fullPath = null;

  AssetData.prototype.currentRecord = null;

  AssetData.prototype.assetDefinition = null;

  function AssetData(endpoint, assetPath) {

    /**
    @constructs AssetData
    @param {String} endpoint An endpoint base URL.
    @param {String} assetPath An asset path public key address.
     */
    var ledgerPath, orientation, position;
    ledgerPath = new LedgerPath;
    this.endpoint = endpoint;
    this.assetPath = ledgerPath.parse(assetPath);
    position = 1;
    orientation = endpoint.rootUrl.length - 1;
    if (endpoint.rootUrl.slice(orientation, endpoint.rootUrl.length) === '/') {
      this.fullPath = endpoint.rootUrl + assetPath.slice(position, assetPath.length);
    } else {
      this.fullPath = endpoint.rootUrl + assetPath;
    }
    this;
  }

  AssetData.prototype.setAccountBalance = function(balanceData) {

    /**
    @memberof AssetData#setAccountBalance
    @param {Object} balanceData A balance object to set.
     */
    this.currentRecord = balanceData;
  };

  AssetData.prototype.fetchAssetDefinition = function() {

    /**
    @memberof AssetData#fetchAssetDefinition
    @description
    Prepares the AssetData instance.
     */
    var def;
    def = $q.defer();
    this.endpoint.getAssetDefinition(this.assetPath).then((function(_this) {
      return function(result) {
        _this.assetDefinition = result;
        return def.resolve(_this.assetDefinition);
      };
    })(this));
    return def.promise;
  };

  return AssetData;

})();

module.exports = AssetData;
