
/**
@fileOverview ./schedgmule/services/ledgerPath.js
@description
Ledger Path.
 */
var LedgerPath;

LedgerPath = (function() {
  function LedgerPath(parts) {

    /**
    @class LedgerPath
     */
    this.parts = parts;
  }

  LedgerPath.prototype.toString = function() {

    /**
    @memberof! LedgerPath#toString
    @returns {String} outputString A string that has been constructed to 
    represent the ledger path.
     */
    return '/' + this.parts.map(function(item) {
      return item + '/';
    }).join('');
  };

  LedgerPath.prototype.parse = function(value) {

    /**
    @function
    @name LedgerPath#parse
    @param {String} value A string construct for ledger path to be parsed.
    @instance
     */
    var parts;
    if (!value) {
      return;
    }
    parts = value.split('/');
    if (parts.length < 2 || parts[0] !== '' || parts[parts.length - 1] !== '') {
      throw 'Invalid path';
    }
    return new LedgerPath(parts.slice(1, parts.length - 1));
  };

  return LedgerPath;

})();

module.exports = LedgerPath;
