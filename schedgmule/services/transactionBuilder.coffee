###*
@fileOverview ./schedgmule/services/transactionBuilder.js
@description
Transaction Builder Service.
###

try
  $q = require('q')
catch e
  $q = require('Q')

EncodingService = require('./encoding')
ProtobufBuilder = require('./protobufBuilder')
Api = require('./api')
long = require('long')
bytebuffer = require('bytebuffer')
isArray = require('../utils/isArray')
_ = require('lodash')


#EventEmitter = require('events').EventEmitter

###*
@class TransactionManager
@description
Transaction Manager is an observer pattern for 
###

class TransactionManager

  logged: null
  constructor: (logging=[]) ->
    @logged = logging

  notify: (item) ->
    $q.all(subscriber.callback(item) for subscriber in @logged when subscriber.item is item)
      .done (d) ->
        console.log d

  introspect: (to, onNew) ->
    @logged.push { 'item': to, 'callback': onNew }

class TransactionSession

  pipe: () ->
    (item) ->
      def = $q.defer()
      console.dir item
      def.resolve item
      def.promise

class TransactionBuilder

  storedTransactions : []
  endpoint           : null
  records            : null
  logged             : []


  constructor: (_endpoint) ->

    ###*
    Creates a new Transaction.
    @class TransactionBuilder
    @param {String} endpoint An endpoint base URL.
    ###

    endpoint = _endpoint # JSON.parse(_endpoint)
    try
      rootUrl = if isArray(endpoint) then _.last(endpoint) else endpoint.rootUrl
    catch e
      console.log e

    console.log rootUrl
    @endpoint = rootUrl
    @records = []
    #@


  addRecord: (key, value, version) ->

    ###*
    @memberof TransactionBuilder#
    @description
    Add Record.
    @param {String} key A key for the given asset.
    @param {Number} value A number value.
    @param {String} version A version for the record.
    ###

    @manager = new TransactionManager
    @logger = new TransactionSession '==================================== addRecord'

    newRecord =
      key     : key
      version : version
    if value != null
      newRecord.value = data: value
    else
      newRecord.value = null

    label = "Record Check #{newRecord.version.toString()}"
    recordNamespace = label
    @manager.introspect recordNamespace, @logger.pipe(newRecord)

    @records.push newRecord
    @


  addAccountRecord: (previous, change) ->

    ###*
    @memberof TransactionBuilder#
    @description
    Add Account Record.
    @param {Object} previous A previous transaction record mutation object.
    @param {Object} previous.key The public key of the previous transaction 
    record mutation.
    @param {Object} change A change object.
    @returns {Object} record A record object.
    ###

    encodingService = new EncodingService
    if !previous
      console.log 'No previous record provided.'
      return

    if !previous.key
      console.log 'No key provided on previous record.'
      return

    balance = new long(previous.balance.low, previous.balance.high)

    console.log '========================'
    console.log balance
    updatedBalance = balance.add(change)

    try
      k = previous.key
      p = encodingService.encodeInt64(updatedBalance)
      v = previous.version
    catch e
      console.log e
      throw e

    try
      record = @addRecord( k, p, v )
    catch e
      console.log e
      throw e

    record


  fetchAndAddAccountRecord: (account, asset, change) ->

    ###*
    @memberof TransactionBuilder#
    @description
    Resolve name accounts
    @param {String} account A root account key.
    @param {Object} asset An asset object (typically from asset definition download).
    @param {Object} change A change object.
    ###

    if account.slice(0, 1) == '@'
      account = '/aka/' + account.slice(1, account.length) + '/'

    apiService = new Api
    return apiService.loadData(@endpoint, account, 'goto').then((result) =>
      if result.data == null
        return account
      else
        # If a goto DATA record exists, we use the redirected path
        @addRecord( result.key, null, result.version )
        return result.data

    ).then((accountResult) =>
      try
        l = apiService.loadAccount @endpoint, accountResult, asset
      catch e
        console.log e
        throw e
      l

    ).then (currentRecord) =>
      @addAccountRecord currentRecord, change
      return


  submit: (key, metadataConstruct, privateKey, publicKey) ->

    ###*
    @memberof TransactionBuilder#
    @description
    Submit.
    @param {String} key A root account key.
    ###

    _key =
      key        : key
      privateKey : privateKey
      publicKey  : publicKey

    def = $q.defer()

    protobufBuilder = new ProtobufBuilder
    encodingService = new EncodingService
    apiService = new Api

    try
      _metadata = bytebuffer.fromHex('')
      #_metadata : JSON.stringify(metadataConstruct)
    catch e
      console.log e

    try
      console.log '============================= TRY ENCODING'
      console.log @endpoint
      _url = encodingService.encodeString(@endpoint)
      console.log _url
    catch e
      console.log e

    nr = @records
    #nr = _.filter(_.map @records, (record) =>
      #label = "Record Check #{record.version.toString()}"
      #recordNamespace = label
      #@manager.notify recordNamespace

      #if record.version
        #return record
    #, (r) ->
      #r
    #)
    console.log nr

    try
      constructedTransaction = new protobufBuilder.Mutation(
        namespace : _url
        records   : nr
        metadata  : _metadata
      )
      console.dir constructedTransaction
      console.log JSON.stringify(constructedTransaction)

    catch e
      console.log e
      throw e

    try
      apiService.postTransaction(@endpoint, constructedTransaction.encode(), _key).then (d) ->
        def.resolve d
      , (error) ->
        console.log '====================== ?'
        def.reject error: error
    catch e
      console.log e
      throw e

    def.promise

  insert: (key) ->

    ###*
    Return this in response. Client is ready to submit transaction.
    @memberof TransactionBuilder#
    @param {String} key A root account key.
    @returns {Object} transactionConstruct A transaction construct to be submitted.
    ###

    @storedTransactions.push @

    transactionConstruct =
      transaction : _.last(@storedTransactions)
      key         : key


  error: () ->

    ###*
    UI Error on Transaction
    @memberof TransactionBuilder#
    @returns {Object} error An error object for display.
    ###

    @storedTransactions = []
    error =
      error: "Invalid transaction."


module.exports = TransactionBuilder
