
/**
@fileOverview ./schedgmule/services/validator.js
@description
Data Defender Validators.
 */
var Validator;

Validator = (function() {
  function Validator() {}


  /**
  @interface Validator
   */

  Validator.prototype.isNumber = function(number) {

    /**
    @function
    @name Validator#isNumber
     */
    var regex;
    regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(number);
  };

  return Validator;

})();

module.exports = Validator;
