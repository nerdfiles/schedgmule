###*
@fileOverview ./schedgmule/services/ledgerRecord.js
@description
Ledger Record.
###

bytebuffer = require('bytebuffer')

EncodingService = require('./encoding')

class LedgerRecord

  path: null
  recordType: null
  name: null

  constructor: () ->

    ###*
    @class LedgerRecord
    ###

  record: ( path, recordType, name ) ->
    {
      path       : path
      recordType : recordType
      name       : name
    }

  toString: ->

    ###*
    @memberof! LedgerRecord#toString
    ###

    @record.path.toString() + ':' + @record.recordType + ':' + @record.name

  toByteBuffer: ->

    ###*
    @memberof! LedgerRecord#toByteBuffer
    ###

    bytebuffer.wrap @toString(), 'utf8', true

  parse: (value) ->

    ###*
    @function
    @name LedgerRecord#parse
    @instance
    ###

    encodingService = new EncodingService

    text = value
    if typeof text != 'string'
      text = encodingService.decodeString(text)
    parts = text.split(':')
    if parts.length < 1
      throw 'Invalid record key'

    try
      ledgerRecord = @record(parts[0], parts[1], parts.slice(2, parts.length).join(':'))
    catch e
      throw e

    ledgerRecord

module.exports = LedgerRecord
