###*
@fileOverview ./schedgmule/models/Endpoint.js
@module models/endpoint
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID

EndpointSchema = new Schema(
  meta:
    version: String
  endpoints: [String]
, _id: false)

Endpoint = mongoose.model('Endpoint', EndpointSchema)

module.exports = Endpoint

