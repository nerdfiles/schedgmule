###*
@fileOverview ./schedgmule/models/Action.js
@module models/account
@description
Implements http://schema.org/Person
@schema
    [
        {
            "subjects": [
                {
                    "addresses": [ "XcPu898xQ9Cb72cvWNR3yX1jSJifBMPYX3", "XjsApTFjTL63YTototE8rX3NF5aqRqcari" ],
                    "required": 1
                }
            ],
            "recursive": true,
            "record_name": "asdef",
            "record_name_matching": "Prefix",
            "permissions": {
                "account_negative": "Permit",
                "account_spend": "Permit",
                "account_modify": "Permit",
                "account_create": "Permit",
                "data_modify": "Permit"
            }
        }
    ]

    Subject = new Schema(
      addresses: [String]
      required: Number
    )

###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID

ActionSchema = new Schema(
  subjects: [mongoose.Schema.Types.Mixed]
  recursive: Boolean
  record_name: String
  record_name_matching: String
  permissions:
    account_negative: String
    account_spend: String
    account_modify: String
    account_create: String
    data_modify: String
  _id: type: UUID
)

Action = mongoose.model('Action', ActionSchema)

module.exports = Action
