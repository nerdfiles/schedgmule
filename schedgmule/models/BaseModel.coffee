###*
@fileOverview ./schedgmule/models/BaseModel.js
@module models/coringsample
@description
Implements http://schema.org/DataCatalog
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID

Account = require('./Account')
AssetGroup = require('./AssetGroup')
DepthRange = require('./DepthRange')
SampleType = require('./SampleType')

URL = String
Person = String
AlignmentObject = String
Text = String
MediaObject = String
CreativeWork = URL

BaseModelSchema = new Schema(
  dateCreated          : Date
  dateModified         : Date
  datePublished        : Date # Date of first broadcast/publication.
  discussionUrl        : URL # A link to the page containing the comments of the CreativeWork.
  educationalAlignment : AlignmentObject # An alignment to an established educational framework.
  educationalUse       : Text # The purpose of a work in the context of education; for example, 'assignment', 'group work'.
  encoding             : MediaObject # A media object that encodes this CreativeWork. This property is a synonym for associatedMedia. Supersedes encodings.
  exampleOfWork        : CreativeWork # A creative work that this work is an example/instance/realization/derivation of.
  fileFormat           : Text # Media type (aka MIME format, see IANA site) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each MediaObject alongside particular fileFormat information.
  #sampleType:
    #type: mongoose.Schema.Types.ObjectId
    #ref: 'SampleType'
    #index: true
  sampleType: [String]
  #depthRange:
    #type: mongoose.Schema.Types.ObjectId
    #ref: 'DepthRange'
    #index: true
  depthRange:
    min: Number
    max: Number
  #author:
    #type: mongoose.Schema.Types.ObjectId
    #ref: 'Account'
    #index: true
  author:
    address: String
  #assetGroupApi:
    #type: mongoose.Schema.Types.ObjectId
    #ref: 'AssetGroup'
    #index: true
  assetGroupApi    : String
  meta:
    version  : Number
    dochash  : String
    alias    : String
    uri      : String
    children : Array
  isHidden: Boolean
  _id: type: UUID
)

BaseModelSchema.methods.getChildren = (cb) ->
  @model('BaseModelSchema').meta.children

BaseModelSchema.methods.getCommentsIndex = (cb) ->
  @model('BaseModelSchema').discussionUrl

BaseModel = mongoose.model('BaseModel', BaseModelSchema)

module.exports = BaseModel
