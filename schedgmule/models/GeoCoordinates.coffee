###*
@fileOverview ./schedgmule/models/GeoCoordinates.js
@module models/geocoordinates
@description
Implements http://schema.org/GeoCoordinates
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID

GeoCoordinatesSchema = new Schema(
  elevation : Number
  latitude  : Number
  longitude : Number
  _id: type: UUID
)

GeoCoordinates = mongoose.model('GeoCoordinates', GeoCoordinatesSchema)

module.exports = GeoCoordinates
