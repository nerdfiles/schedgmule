###*
@fileOverview ./schedgmule/models/File.js
@module models/file
@description
Object model for File that extends http://schema.org/DataDownload.
###

mongoose = require 'mongoose'
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID
Account = require('./Account')


FileSchema = new Schema(
  contentSize    : String
  contentUrl     : String
  uploadDate     : String
  encodingFormat : String
  uploader       :
    type: mongoose.Schema.Types.ObjectId
    ref: 'Account'
    index: true
  meta:
    alias   : String
    version : Number
    dochash : String
    uri     : String
  isHidden: Boolean
  _id: type: UUID
)

File = mongoose.model('File', FileSchema)

module.exports = File
