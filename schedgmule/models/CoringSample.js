
/**
@fileOverview ./schedgmule/models/BaseModel.js
@module models/coringsample
@description
Implements http://schema.org/DataCatalog
 */
var Account, AlignmentObject, BaseModel, BaseModelSchema, CreativeWork, DepthRange, MediaObject, Person, SampleType, Schema, Text, URL, UUID, AssetGroup, mongoose;

mongoose = require('mongoose');

Schema = mongoose.Schema;

require('mongoose-uuid2').loadType(mongoose);

UUID = mongoose.Types.UUID;

Account = require('./Account');

AssetGroup = require('./AssetGroup');

DepthRange = require('./DepthRange');

SampleType = require('./SampleType');

URL = String;

Person = String;

AlignmentObject = String;

Text = String;

MediaObject = String;

CreativeWork = URL;

BaseModelSchema = new Schema({
  dateCreated: Date,
  dateModified: Date,
  datePublished: Date,
  discussionUrl: URL,
  educationalAlignment: AlignmentObject,
  educationalUse: Text,
  encoding: MediaObject,
  exampleOfWork: CreativeWork,
  fileFormat: Text,
  sampleType: [String],
  depthRange: {
    min: Number,
    max: Number
  },
  author: {
    address: String
  },
  assetGroupApi: String,
  meta: {
    version: Number,
    dochash: String,
    alias: String,
    uri: String,
    children: Array
  },
  isHidden: Boolean,
  _id: {
    type: UUID
  }
});

BaseModelSchema.methods.getChildren = function(cb) {
  return this.model('BaseModelSchema').meta.children;
};

BaseModelSchema.methods.getCommentsIndex = function(cb) {
  return this.model('BaseModelSchema').discussionUrl;
};

BaseModel = mongoose.model('BaseModel', BaseModelSchema);

module.exports = BaseModel;
