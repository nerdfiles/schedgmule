###*
@fileOverview ./schedgmule/models/Address.js
@module models/address
@description
Implements http://schema.org/PostalAddress
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID
GeoCoordinates = require('./GeoCoordinates')

AddressSchema = new Schema(
  areaServed:
    type : mongoose.Schema.Types.ObjectId
    ref  : 'GeoCoordinates'
  streetAddress   : String
  addressRegion   : String
  addressLocality : String
  addressCountry  : String
  postalCode      : String
  _id: type: UUID
)

Address = mongoose.model('Address', AddressSchema)

module.exports = Address
