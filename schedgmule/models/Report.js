
/**
@fileOverview ./schedgmule/models/Report.js
@module models/file
@description
Object model for Report that extends http://schema.org/DataDownload.
 */
var Account, Report, ReportSchema, Schema, UUID, mongoose;

mongoose = require('mongoose');

Schema = mongoose.Schema;

require('mongoose-uuid2').loadType(mongoose);

UUID = mongoose.Types.UUID;

Account = require('./Account');

ReportSchema = new Schema({
  contentSize: String,
  contentUrl: String,
  uploadDate: String,
  encodingFormat: String,
  uploader: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account',
    index: true
  },
  meta: {
    alias: String,
    version: Number,
    dochash: String,
    uri: String
  },
  isHidden: Boolean,
  _id: {
    type: UUID
  }
});

Report = mongoose.model('Report', ReportSchema);

module.exports = Report;
