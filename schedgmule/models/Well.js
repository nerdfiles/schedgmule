
/**
@fileOverview ./schedgmule/models/AssetGroup.js
@description
Implements http://schema.org/Place
 */
var Address, Schema, UUID, AssetGroup, AssetGroupSchema, mongoose;

mongoose = require('mongoose');

Schema = mongoose.Schema;

require('mongoose-uuid2').loadType(mongoose);

UUID = mongoose.Types.UUID;

Address = require('./Address');

AssetGroupSchema = new Schema({
  dateAdded: Date,
  assetGroupApi: String,
  address: {
    streetAddress1: String,
    streetAddress2: String,
    city: String,
    state: String,
    postalCode: String,
    latitude: Number,
    longitude: Number
  },
  meta: {
    version: Number,
    dochash: String,
    alias: String,
    uri: String
  },
  isHidden: Boolean,
  _id: {
    type: UUID
  }
});

AssetGroup = mongoose.model('AssetGroup', AssetGroupSchema);

module.exports = AssetGroup;
