
/**
@fileOverview ./schedgmule/models/DepthRange.js
@module models/depthrange
 */
var DepthRange, DepthRangeSchema, Schema, UUID, mongoose;

mongoose = require('mongoose');

Schema = mongoose.Schema;

require('mongoose-uuid2').loadType(mongoose);

UUID = mongoose.Types.UUID;

DepthRangeSchema = new Schema({
  min: Number,
  max: Number,
  _id: {
    type: UUID
  }
});

DepthRange = mongoose.model('DepthRange', DepthRangeSchema);
