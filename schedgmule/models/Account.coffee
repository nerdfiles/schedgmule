###*
@fileOverview ./schedgmule/models/Account.js
@module models/account
@description
Implements http://schema.org/Person
###

uuid = require 'node-uuid'
mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID

Address = require('./Address')
Endpoint = require('./Endpoint')

AccountSchema = Schema(
  firstName      : String
  additionalName : String
  lastName       : String
  birthDate      : Date
  publicKey      : String
  dateCreated    : Date
  meta:
    version  : Number
    alias    : String
    dk       : String
    dochash  : String
    uri      : String
  isHidden : Boolean
  accountId: type: UUID
  endpoint:
    meta:
      version: String
    endpoints: [String]
  #endpoint:
    #type   : mongoose.Schema.Types.ObjectId
    #ref    : 'Endpoint'
  #address:
    #type : mongoose.Schema.Types.ObjectId
    #ref  : 'Address'
)

Account = mongoose.model('Account', AccountSchema)

module.exports = Account

