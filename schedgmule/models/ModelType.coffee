###*
@fileOverview ./schedgmule/models/SampleType.js
@module models/sampletype
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID


SampleTypeSchema = new Schema(
  options: [String]
  _id: type: UUID
)

SampleType = mongoose.model('SampleType', SampleTypeSchema)
