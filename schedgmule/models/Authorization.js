
/**
@fileOverview ./schedgmule/models/Permission.js
@module models/permission
 */
var Authorization, AuthorizationSchema, Schema, UUID, mongoose;

mongoose = require('mongoose');

Schema = mongoose.Schema;

require('mongoose-uuid2').loadType(mongoose);

UUID = mongoose.Types.UUID;

AuthorizationSchema = new Schema({
  subjects: [mongoose.Schema.Types.Mixed],
  recursive: Boolean,
  record_name: String,
  record_name_matching: String,
  permissions: {
    account_negative: String,
    account_spend: String,
    account_modify: String,
    account_create: String,
    data_modify: String
  },
  _id: {
    type: UUID
  }
});

Authorization = mongoose.model('Authorization', AuthorizationSchema);

module.exports = Authorization;
