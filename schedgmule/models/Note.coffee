###*
@fileOverview ./schedgmule/models/Note.js
@module models/note
@description
Implements http://schema.org/DataFeedItem
###

mongoose = require('mongoose')
Schema = mongoose.Schema
require('mongoose-uuid2').loadType(mongoose)
UUID = mongoose.Types.UUID
Account = require './Account'

NoteSchema = new Schema(
  title       : String
  dateCreated : Date
  content     : String
  author      :
    type: mongoose.Schema.Types.ObjectId
    ref: 'Account'
    index: true
  meta :
    version : Number
    alias   : String
    dk      : String
    dochash : String
    uri     : String
  isHidden : Boolean
  _id : type : UUID
)

Note = mongoose.model('Note', NoteSchema)

module.exports = Note
