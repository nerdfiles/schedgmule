var createControl = require('./templates/create');
var loginControl = require('./templates/login');
var settingsControl = require('./templates/settings');

module.exports = {
  create   : createControl,
  login    : loginControl,
  settings : settingsControl
};
