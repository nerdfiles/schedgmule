###*
@fileOverview ./schedgmule/server.js
@description
Initialize application and components.
###

#csurf          = require('csurf')
http           = require('http')
mongoose       = require('mongoose')
winston        = require('winston')
expressWinston = require('express-winston')
bodyParser     = require('body-parser')
cookieParser   = require('cookie-parser')
session        = require('express-session')
MongoDBStore   = require('connect-mongodb-session')(session)

config = port: process.env['PORT'] or 3000
environment = require('./config')

server = (app) ->

  ###*
  Server
  @module schedgmule.app/server
  @param {object} Express application instance.
  @return {object} Simple interface for main application.
  ###

  ###*
  @typedef Store
  @param {String} uri
  @param {String} collection
  ###

  # Configure application: Port
  app.set 'port', config.port

  # Configure middleware: Serializer
  app.use bodyParser.json()

  # Configure middleware: Cookies
  app.use cookieParser()

  connectWinston = () ->

    ###*
    Configure middleware: Logging
    ###

    app.use(expressWinston.logger({
      transports: [
        new winston.transports.Console({
          json     : true,
          colorize : true
        })
      ]
      meta          : true
      msg           : "HTTP {{req.method}} {{req.url}}"
      expressFormat : true
      colorStatus   : true
    }))

    return

  setupMongoDbServerSession = () ->

    ###*
    Configure middleware: MongoDB for middleware#Session
    @returns {Store}
    ###

    store = new MongoDBStore({
      uri        : environment.__meta__.sessiondb
      collection : 'session'
    })

    store.on('error', (error) ->
      assert.ifError(error)
      assert.ok(false)
    )

    store

  connectMongoDbServerSession = () ->

    ###*
    Configure middleware: Session
    ###

    app.use(require('express-session')(
      secret: "XkFRPBzRNpe8N693U1gmdXLasDYgD3W1WX"
      cookie:
        maxAge: 1000 * 60 * 60 * 24 * 7 # 1 week
      store: setupMongoDbServerSession()
    ))

  connectMongoDbServer = () ->

    ###*
    Configure middleware: Document Store
    ###

    #options =
      #server:
        #socketOptions:
          #keepAlive: 1
    options = 
      server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
      replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }

    mongoose.connect environment.__meta__.db, options
    return

  manageMongoDbServer = () ->

    ###*
    Configure close conditions for MongoDB
    ###

    app.use (req, res, next) ->

      afterResponse = ->
        #console.dir { req: req }, 'End request'
        mongoose.connection.close ->
          console.log 'Mongoose connection disconnected'
          return
        return

      res.on 'finish', afterResponse
      res.on 'close', afterResponse

      next()
      return

  gracefulExit = () ->

    ###*
    Graceful Exit Mongoose
    ###

    mongoose.connection.close(() ->
      console.log('Mongoose default connection with DB is disconnected through app termination.')
      process.exit(0)
    )

  connectElasticSearchServer = () ->

    ###*
    Configure middleware: Full-text Search
    ###

    ElasticSearchClient = require('elasticsearchclient')
    serverOptions =
      host : 'localhost'
      port : 9200
    elasticSearchClient = new ElasticSearchClient(serverOptions)

  connectExpressServer = () ->

    ###*
    Initialize Application Server
    ###

    server = http.createServer(app)

  #connectWinston()
  #manageMongoDbServer()
  process
    .on('SIGINT', gracefulExit)
    .on('SIGTERM', gracefulExit)

  conn = mongoose.connection
  conn.on 'disconnected', connectMongoDbServer
  conn.on 'error', console.error.bind(console, 'connection error:')

  connectMongoDbServer()
  connectMongoDbServerSession()

  {
    app    : app
    server : connectExpressServer()
    search : connectElasticSearchServer()
  }

module.exports = server
