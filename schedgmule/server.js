
/**
@fileOverview ./schedgmule/server.js
@description
Initialize application and components.
 */
var MongoDBStore, bodyParser, config, cookieParser, environment, expressWinston, http, mongoose, server, session, winston;

http = require('http');

mongoose = require('mongoose');

winston = require('winston');

expressWinston = require('express-winston');

bodyParser = require('body-parser');

cookieParser = require('cookie-parser');

session = require('express-session');

MongoDBStore = require('connect-mongodb-session')(session);

config = {
  port: process.env['PORT'] || 3000
};

environment = require('./config');

server = function(app) {

  /**
  Server
  @module schedgmule.app/server
  @param {object} Express application instance.
  @return {object} Simple interface for main application.
   */

  /**
  @typedef Store
  @param {String} uri
  @param {String} collection
   */
  var conn, connectElasticSearchServer, connectExpressServer, connectMongoDbServer, connectMongoDbServerSession, connectWinston, gracefulExit, manageMongoDbServer, setupMongoDbServerSession;
  app.set('port', config.port);
  app.use(bodyParser.json());
  app.use(cookieParser());
  connectWinston = function() {

    /**
    Configure middleware: Logging
     */
    app.use(expressWinston.logger({
      transports: [
        new winston.transports.Console({
          json: true,
          colorize: true
        })
      ],
      meta: true,
      msg: "HTTP {{req.method}} {{req.url}}",
      expressFormat: true,
      colorStatus: true
    }));
  };
  setupMongoDbServerSession = function() {

    /**
    Configure middleware: MongoDB for middleware#Session
    @returns {Store}
     */
    var store;
    store = new MongoDBStore({
      uri: environment.__meta__.sessiondb,
      collection: 'session'
    });
    store.on('error', function(error) {
      assert.ifError(error);
      return assert.ok(false);
    });
    return store;
  };
  connectMongoDbServerSession = function() {

    /**
    Configure middleware: Session
     */
    return app.use(require('express-session')({
      secret: "XkFRPBzRNpe8N693U1gmdXLasDYgD3W1WX",
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 7
      },
      store: setupMongoDbServerSession()
    }));
  };
  connectMongoDbServer = function() {

    /**
    Configure middleware: Document Store
     */
    var options;
    options = {
      server: {
        socketOptions: {
          keepAlive: 1,
          connectTimeoutMS: 30000
        }
      },
      replset: {
        socketOptions: {
          keepAlive: 1,
          connectTimeoutMS: 30000
        }
      }
    };
    mongoose.connect(environment.__meta__.db, options);
  };
  manageMongoDbServer = function() {

    /**
    Configure close conditions for MongoDB
     */
    return app.use(function(req, res, next) {
      var afterResponse;
      afterResponse = function() {
        mongoose.connection.close(function() {
          console.log('Mongoose connection disconnected');
        });
      };
      res.on('finish', afterResponse);
      res.on('close', afterResponse);
      next();
    });
  };
  gracefulExit = function() {

    /**
    Graceful Exit Mongoose
     */
    return mongoose.connection.close(function() {
      console.log('Mongoose default connection with DB is disconnected through app termination.');
      return process.exit(0);
    });
  };
  connectElasticSearchServer = function() {

    /**
    Configure middleware: Full-text Search
     */
    var ElasticSearchClient, elasticSearchClient, serverOptions;
    ElasticSearchClient = require('elasticsearchclient');
    serverOptions = {
      host: 'localhost',
      port: 9200
    };
    return elasticSearchClient = new ElasticSearchClient(serverOptions);
  };
  connectExpressServer = function() {

    /**
    Initialize Application Server
     */
    return server = http.createServer(app);
  };
  process.on('SIGINT', gracefulExit).on('SIGTERM', gracefulExit);
  conn = mongoose.connection;
  conn.on('disconnected', connectMongoDbServer);
  conn.on('error', console.error.bind(console, 'connection error:'));
  connectMongoDbServer();
  connectMongoDbServerSession();
  return {
    app: app,
    server: connectExpressServer(),
    search: connectElasticSearchServer()
  };
};

module.exports = server;
