###*
@fileOverview ./schedgmule/config.js
@module environment
@description
Dynamic application environment variables.
###

_ = require 'lodash'
fs = require 'fs'
path = require 'path'

Array::twine = (spacer='') ->
  @.join spacer

class Environment

  ###*
  @class Environment
  ###

  debug           : null
  baseUrl         : null
  db              : null
  sessiondb       : null
  defaultEndpoint : null

  constructor: () ->

    ###*
    @function
    @name Environment#constructor
    ###

    # File config
    environmentJson = path.join(__dirname, '/environment.json')

    # Url config
    baseProtocol = 'http'
    sep = '://'
    baseDomainOrIp = 'localhost'
    basePort = ':3000'
    basePath = '/'

    # Url prepare
    applicationUrl = [
      baseProtocol, sep,
      baseDomainOrIp, basePort,
      basePath
    ]

    # Database setups
    localDatabase = 'mongodb://localhost:27017/schedgmule_test'
    localSessionStore = 'mongodb://localhost:27017/schedgmule_development_session'
    productionDatabase = 'mongodb://localhost:27017/schedgmule_production'
    productionSessionStore = 'mongodb://localhost:27017/schedgmule_session_production'

    # Ledger setups
    defaultLedger = 'http://107.170.46.60:8080/'

    local = @debug and fs.statSync(environmentJson).isFile()

    # Initialize environment
    if local isnt true
      @debug = true
      @baseUrl = applicationUrl.twine()
      @db = if @debug then localDatabase else productionDatabase
      @sessiondb = if @debug then localSessionStore else productionSessionStore
      @defaultEndpoint = [defaultLedger]
    else
      fs.readFile(environmentJson, 'utf8', (err, configData) ->
        throw err if err
        _.extend @, JSON.parse(configData)
      )

# Export environment
__meta__ = new Environment
module.exports =
  __meta__: __meta__

