###*
@fileOverview ./schedgmule/index.js
@description
Main Routes for the schedgmule application. Also initiates bitcore.
###

proxy = require('express-http-proxy')
cors = require('cors')
bitcore = require('bitcore')
require('events').EventEmitter.prototype._maxListeners = 100

# Primitives and Assets
Mnemonic = require('./assets/Mnemonic/index')
Account = require('./assets/Account/index')
BaseModel = require('./assets/BaseModel/index')
AssetGroup = require('./assets/AssetGroup/index')

# Related Actions
Search = require('./assets/Search/index')
File = require('./assets/File/index')
Note = require('./assets/Note/index')

versionPrefix = '/v1'
versionPrefixControls = '/v1'

module.exports = (__interface__) ->

  ###*
  Server interface for modular REST-ful actions and assets.
  @module schedgmule.app/interface
  ###

  __interface__.versionPrefix = '/v1'

  whitelist = [ '*' ]
  corsOptions =
    preflightContinue: true
    methods: [
      'GET'
      'PUT'
      'DELETE'
      'PATCH'
      'POST'
      'OPTIONS'
    ]
    credentials: true
    origin: (origin, callback) ->
      originIsWhitelisted = whitelist.indexOf(origin) != -1
      callback null, originIsWhitelisted
      return

  #__interface__.app.use cors()

  allowCrossDomain = (req, res, next) ->
    res.header 'Access-Control-Allow-Origin', '*'
    res.header 'Access-Control-Allow-Methods', 'POST,GET,PUT,PATCH,DELETE'
    res.header 'Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,X-CSRF-TOKEN,Accept,Origin,Authorization'
    next()
    return

  __interface__.app.use(allowCrossDomain)

  mnemonicRoute = new Mnemonic(
    __interface__.app
  )

  accountRoute = new Account(
    __interface__.app
  )

  coringSampleRoute = new BaseModel(
    __interface__.app
  )

  assetGroupRoute = new AssetGroup(
    __interface__.app
  )

  #subaccountRoute = new Subaccount(
    #__interface__.app
  #)

  searchRoute = new Search(
    __interface__.app,
    __interface__.search
  )

  fileRoute = new File(
    __interface__.app
  )

  __interface__
