
/**
@fileOverview ./schedgmule/start.js
@description
Start Web application.
 */
var start;

start = function(__interface__) {

  /**
  Server initialization at a specified port.
  @module schedgmule.app/start
   */
  var port;
  port = __interface__.app.get('port');
  __interface__.server.listen(port, function() {
    var loadedPort, logo;
    loadedPort = __interface__.server.address().port;
    logo = ['                              _            _', '   _                         | |          | |', ' _| |_ _____  ____ ____ _____| | _____  __| | ____ _____  ____', '(_   _) ___ |/ ___) ___|____ | || ___ |/ _  |/ _  | ___ |/ ___)', '  | |_| ____| |  | |   / ___ | || ____( (_| ( (_| | ____| |', '   \\__)_____)_|  |_|   \\_____|\\_)_____)\\____|\\___ |_____)_|', '                                            (_____|'];
    console.log(logo.join('\n'));
    console.log('Running on http://localhost:' + loadedPort);
  });
};

module.exports = start;
