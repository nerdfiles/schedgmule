# schedgmule

schedgmule provides for REST API and API Controls to facilitate a 
dual-wallet asset tracking system which uses HD Private Keys for 
client node differentiation (uniqueness) such that authentication 
of transaction actions and proof-of-identity are decoupled.

## REST Operations Contract

The schedgmule API is versioned with the following base URL:

    https://api.schedgmule.com/v1/...

## Hypertext API Controls

schedgmule’s API Controls are also versioned with the following base URL:

    https://api.schedgmule.com/v1/controls/...

### Rationale

Hypertext API Controls are loaded via [HTTP OPTIONS][1], a method which

    [...] allows the client to determine the options and/or requirements 
    associated with a resource, or the capabilities of a server, without 
    implying a resource action or initiating a resource retrieval.

—
[1]: http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
