
/**
@fileOverview ./schedgmule/config.js
@module environment
@description
Dynamic application environment variables.
 */
var Environment, _, __meta__, fs, path;

_ = require('lodash');

fs = require('fs');

path = require('path');

Array.prototype.twine = function(spacer) {
  if (spacer == null) {
    spacer = '';
  }
  return this.join(spacer);
};

Environment = (function() {

  /**
  @class Environment
   */
  Environment.prototype.debug = null;

  Environment.prototype.baseUrl = null;

  Environment.prototype.db = null;

  Environment.prototype.sessiondb = null;

  Environment.prototype.defaultEndpoint = null;

  function Environment() {

    /**
    @function
    @name Environment#constructor
     */
    var applicationUrl, baseDomainOrIp, basePath, basePort, baseProtocol, defaultLedger, environmentJson, local, localDatabase, localSessionStore, productionDatabase, productionSessionStore, sep;
    environmentJson = path.join(__dirname, '/environment.json');
    baseProtocol = 'http';
    sep = '://';
    baseDomainOrIp = 'localhost';
    basePort = ':3000';
    basePath = '/';
    applicationUrl = [baseProtocol, sep, baseDomainOrIp, basePort, basePath];
    localDatabase = 'mongodb://localhost:27017/schedgmule_test';
    localSessionStore = 'mongodb://localhost:27017/schedgmule_development_session';
    productionDatabase = 'mongodb://localhost:27017/schedgmule_production';
    productionSessionStore = 'mongodb://localhost:27017/schedgmule_session_production';
    defaultLedger = 'http://107.170.46.60:8080/';
    local = this.debug && fs.statSync(environmentJson).isFile();
    if (local !== true) {
      this.debug = true;
      this.baseUrl = applicationUrl.twine();
      this.db = this.debug ? localDatabase : productionDatabase;
      this.sessiondb = this.debug ? localSessionStore : productionSessionStore;
      this.defaultEndpoint = [defaultLedger];
    } else {
      fs.readFile(environmentJson, 'utf8', function(err, configData) {
        if (err) {
          throw err;
        }
        return _.extend(this, JSON.parse(configData));
      });
    }
  }

  return Environment;

})();

__meta__ = new Environment;

module.exports = {
  __meta__: __meta__
};
