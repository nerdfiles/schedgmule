
/**
@fileOverview ./schedgmule/assets/Search/index.js
@description
Advanced Search.
 */
var _, config, indexRoute, request;

_ = require('lodash');

request = require('request');

config = require('../../config');

indexRoute = function(app, esClient) {

  /**
  Index Route for Advanced Search.
  @module routes/search
  @description
  Index Route for Advanced Search
   */
  var basicSearch;
  basicSearch = require('./actions/base')(app, esClient);
};

module.exports = indexRoute;
