###*
@fileOverview ./schedgmule/assets/Search/index.js
@description
Advanced Search.
###

_ = require('lodash')
request = require('request')
config = require('../../config')

indexRoute = (app, esClient) ->

  ###*
  Index Route for Advanced Search.
  @module routes/search
  @description
  Index Route for Advanced Search
  ###

  basicSearch = require('./actions/base')(app, esClient)
  return

module.exports = indexRoute
