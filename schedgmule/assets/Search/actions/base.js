
/**
@fileOverview ./schedgmule/assets/Search/actions/basic.js
 */
var basicSearch, e, q;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

basicSearch = function(app, esClient) {
  var searcyByTerm;
  searcyByTerm = function(termToSearch, index, type) {

    /**
    Search By Term
    @inner
     */
    var deferred, qryObj;
    deferred = q.defer();
    qryObj = {
      "query": {
        "term": {
          "name": termToSearch
        }
      }
    };
    esClient.search(index, type, qryObj).on('data', function(data) {
      return deferred.resolve(JSON.parse(data));
    }).on('error', function(err) {
      return deferred.resolve(err);
    }).exec();
    return deferred.promise;
  };
  return app.all('/v1/search', function(req, outerRes, next) {

    /*
    @api {get} /search/ Request Advanced Search
    @apiName GetAdvancedSearch
    @apiGroup User
    @apiParam {String} field Field to search by: BUID, etc.
    @apiParam {String} term Search term.
    @apiDescription
    Advanced Search Engine for Driving Dashboard. Mocks available:  
        1. http://alpha.schedgmule.com/api/mocks/v1/search/post.json
    @apiExample {curl} Example usage:
        curl -i http://alpha.schedgmule.com/v1/search/?term=a+search+query&field=buid
    @apiSuccessExample {json} Success-Response:
        HTTP/1.1 200 OK
        {
          "keys" : [
            {
              "key" : "<string:base58||alias>"
            }
          ],
          "links" : {
            "self" : "<string:url>",
            "next" : "<string:url>",
            "last" : "<string:url>"
          },
          "data" : [{
            "id"   : "<string:uuid>",
            "type" : "<string>"
            "attributes" : {
              "query" : "<string>"
            },
            "files": [
              {
                "id"        : "<string>",
                "fileName"  : "<string>",
                "filePath"  : "<string>"
              }
            ],
            "timeline" : [
              {
                "id"        : "<string>",
                "timestamp" : "<string>",
                "type"      : "<string>",
                "label"     : "<string>",
                "notes"     : [
                  {
                    "id"        : "<string>",
                    "timestamp" : "<string>",
                    "label"     : "<string>",
                    "content"   : "<string>"
                  }
                ]
              }
            ]
          }]
        }
     */
    var field, index, mockSearchResponse, term, type;
    mockSearchResponse = {
      "id": "",
      "files": [
        {
          "id": "",
          "fileName": "",
          "filePath": ""
        }
      ],
      "timeline": [
        {
          "id": "",
          "timestamp": "",
          "type": "",
          "label": "",
          "notes": [
            {
              "id": "",
              "timestamp": "",
              "label": "",
              "content": ""
            }
          ]
        }
      ]
    };
    field = type = req.params.field;
    term = req.params.term;
    index = '_all';
    return q(searcyByTerm(term, index, type)).then(function(data) {
      return outerRes.json(data);
    }, function() {
      return outerRes.json(mockSearchResponse);
    });
  });
};

module.exports = basicSearch;
