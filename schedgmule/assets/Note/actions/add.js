var ApiService, AssetData, EndpointManager, Note, _, addRoute, config, e, mongoose, q, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

Note = require('../../../models/Note');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

addRoute = function(app) {

  /**
   */
  app.post('/v1/coringsample/:uuid/note', function(req, outerRes, next) {

    /**
     */
    var uuid;
    app.on('event:findError', function(errorData) {
      console.dir(errorData);
    });
    uuid = req.params.uuid || null;
    BaseModel.findOne({
      uuid: uuid
    }).exec(function(err, instance) {
      if (err) {
        app.emit('event:findError', err);
      }
      outerRes.json(instance);
    });
  });
};

module.exports = addRoute;
