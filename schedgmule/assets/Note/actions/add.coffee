
try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
Note = require '../../../models/Note'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

addRoute = (app) ->

  ###*
  ###

  app.post '/v1/coringsample/:uuid/note', (req, outerRes, next) ->

    ###*
    ###

    app.on 'event:findError', (errorData) ->
      console.dir errorData
      return

    uuid = req.params.uuid or null

    BaseModel.findOne(uuid: uuid).exec (err, instance) ->

      if err
        app.emit 'event:findError', err

      outerRes.json instance
      return

    return
  return

module.exports = addRoute
