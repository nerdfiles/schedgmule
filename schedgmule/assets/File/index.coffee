apiService = require('../../services/api')
fs = require('fs')
mkdirp = require('mkdirp')
Chance = require('chance')
formidable = require('formidable')

__interface__ = {}

__interface__.indexRoute = (app) ->

  ###*
  Index Route
  @module routes/file
  @description
  Index Module for File. Prepares all HTTP VERBS for Files.
  @param {Express:object} An Express instance.
  @return {undefined}
  ###

  app.options('/v1/file', (req, outerRes, next) ->

    ###*
    @api {options} /file/ Info File
    @apiName OptionsFile
    @apiGroup User
    @apiDescription
      None.
    ###

    outerRes.status(200).send null
  )

  app.get('/v1/file', (req, outerRes, next) ->

    ###*
    @api {get} /file/ Download File
    @apiName DownloadFile
    @apiGroup User
    @apiParam {String} path File path.
    @apiDescription
      None.
    ###

    outerRes.json({
      links: {
        self: ''
      },
      data: [{
      }]
    })
  )

  app.post('/v1/file', (req, outerRes, next) ->
    ###*
    @api {post} /file/ Upload File
    @apiName UploadFile
    @apiGroup User
    @apiParam {String} API_TOKEN Issued API Token.
    @apiParamExample {json} Request-Example:
        {
          "coringSampleId": "2DSJcFPJGWkJun9ekrghNG",
          "fileName": "some-file.png",
          "filePath": "http://domain.com/"
        }
    @apiDescription

    Review https://github.com/nervgh/angular-file-upload/. Therefore, we are 
    expecting `multipart/form-data` https://github.com/nervgh/angular-file-upload/blob/master/src/services/FileUploader.js#L507.  
    We are also expecting https://github.com/nervgh/angular-file-upload/blob/master/src/services/FileUploader.js#L441  
    so you should not really have to modify the form as $window except probably to augment item{} with { coringSampleId: "<string:uuid>" }.  
    This will be known in the Dashboard itself so the modal window can be given a $on('customEvent') that listens for a $broadcast which  
    might be injected into the Directive of angular-file-upload. We may have to modify the AngularJS app itself.  

    Mocks available:  
        1. http://alpha.schedgmule.com/api/mocks/v1/file/post.json
    @apiExample {curl} Example usage:
        curl -i http://alpha.schedgmule.com/v1/file/ -d '{ "fileName": "", "filePath": "" }''
    @apiSuccessExample {json} Success-Response:
        HTTP/1.1 200 OK
        {
          "keys" : [
            {
              "key" : <string:base58||alias>
            }
          ],
          "links" : {
            "self"   : <string:url>,
            "update" : <string:url>,
            "delete" : <string:url>
          },
          "data" : [{
            "id"   : <File._id>,
            "type" : "files",
            "attributes" : {
              "coringSampleId": <BaseModel._id>
            },
            "relationships": {
              "files": {
                "links": {
                  "self": "http://alpha.schedgmule.com/v1/file/{{transaction_hash}}"
                },
                "data": [{
                  "id"   : <string>,
                  "type" : "file"
                }]
              }
            }
          }],
          "included": [{
            "id"         : <string>,
            "type"       : "file",
            "attributes" : {
              "mimeType" : "application/pdf",
              "fileName" : <string>,
              "filePath" : <string>
            }
          }]
        }
    ###

    fileStats = (filePath) ->

      ###
      @inner
      @name fileStats
      @description
      Created time checker for file path.
      @param {String} filePath A file path.
      ###

      try
        f = fs.statSync(filePath)
        return {
          isFile : f.isFile()
          ctime  : f.ctime()
        }
      catch err
        return {
          isFile : false
          ctime  : null
        }

    form = new formidable.IncomingForm()
    uploadedAt = new Date()
    directoryPath = uploadedAt.toLocaleDateString().split('/').reverse().join('/')
    _directoryPath = __dirname + '/../../../public/' + directoryPath

    mkdirp.sync _directoryPath

    try
      console.log('Attempting to upload to: ' + _directoryPath)
      fs.accessSync(_directoryPath, fs.F_OK)
    catch e
      console.log(e)

    form.uploadDir = _directoryPath

    form.type = true
    files = []
    fields = []
    chance = new Chance()

    form.on('field', (field, value) ->
      fields.push([
        field
        value
      ])
    )

    form.on('file', (field, file) ->
      sep = '/'
      _hash = chance.hash(length: 10)
      fName = file.name
      _fName = fName.split('.')
      _fNameNominal = _fName[0]
      _fNameExt = _fName[1]
      _baseName = [_fNameNominal, '-', _hash, '.', _fNameExt].join('')

      fileMeta = fileStats(_directoryPath + sep + _baseName)
      _chance = new Chance(fileMeta.ctime)
      _chash = _chance.hash(lenghth: 10)
      _contingentName = [_fNameNominal, '-', _chash, '.', _fNameExt].join('')

      _f = file.path.split('/')
      _f.pop()
      source = fs.createReadStream(file.path)

      if !fileMeta.isFile
        files.push([
          field
          #file + sep + _baseName
          _baseName
        ])
        dest = fs.createWriteStream(_f.join('/') + sep + _baseName)
      else
        files.push([
          field
          #file + sep + _contingentName
          _contingentName
        ])
        dest = fs.createWriteStream(_f.join('/') + sep + _contingentName)

      source.pipe(dest)

      source.on('end', () ->
        if !fileMeta.isFile
          console.log('Copied file: ' + file.name + ' as ' + _baseName)
        else
          console.log('Copied file: ' + file.name + ' as ' + _contingentName)
        fs.unlink file.path
      )

      source.on('error', (err) ->
        console.log(err)
      )
    )

    form.on('end', () ->

      payloadConstruct =
        links:
          'self': 'http://alpha.schedgmule.com/v1/file/{{transaction_hash}}'
        data: []
        included: []

      fileResponseList =
        type: "files"
        attributes:
          title: "Uploaded File List"
        relationships:
          uploader:
            links:
              self: 'http://alpha.schedgmule.com/v1/account/{{uuid}}'
            data: []
          files:
            links:
              self: 'http://alpha.schedgmule.com/v1/file/{{transaction_hash}}/list'
            data: []

      # Dummy data that should be consistent with Openchain's public key/alias naming convention
      authorKey = chance.hash(length: 34)
      _id = authorKey
      authorKey =
        id   : _id
        type : "author"
      authorData =
        id   : _id
        type : "author"
        attributes:
          firstName : "{{firstName}}"
          lastName  : "{{lastName}}"
        links:
          self: 'http://alpha.schedgmule.com/v1/account/' + _id

      fileKeys = []
      fileData = []

      for i in files
        coringSampleDummyKey = chance.hash(length: 34) # ID for BaseModel
        _fileId = chance.guid()

        fileKeys.push
          id   : _fileId
          type : i[0]

        u = {
          id         : _fileId
          type       : 'fileUpload'
          attributes :
            coringSampleId : coringSampleDummyKey
            fileName       : i[1]
            fieldName      : i[0]
            filePath       : 'http://alpha.schedgmule.com/public/' + directoryPath
          links:
            self: 'http://alpha.schedgmule.com/v1/file/' + coringSampleDummyKey + '/' + _fileId
        }

        fileData.push u

      fileResponseList.relationships.uploader.data.push authorKey
      fileResponseList.relationships.files.data = fileKeys.concat(fileResponseList.relationships.files.data)
      payloadConstruct.data.push fileResponseList
      payloadConstruct.included.push authorData
      for i in fileData
        payloadConstruct.included.push i
      outerRes.json payloadConstruct

      console.log('Uploading done.')
    )

    form.parse(req)

  )

  app.delete('/v1/file', (req, outerRes, next) ->

    ###*
    @api {delete} /file/ Remove File
    @apiName DeleteFile
    @apiGroup User
    @apiParam {String} key Key.
    @apiParam {String} path File path.
    @apiDescription
    None.
    ###

    outerRes.status(200).send null

  )

  app.patch('/v1/file', (req, outerRes, next) ->

    ###*
    @api {patch} /file/ Update File
    @apiName UpdateFile
    @apiGroup User
    @apiParam {String} key Key.
    @apiParam {String} oldPath Old File path.
    @apiParam {String} newPath New File path.
    @apiDescription
    None.
    ###
  )

  return

module.exports = __interface__.indexRoute
