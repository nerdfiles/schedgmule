###*
@fileOverview ./schedgmule/assets/File/actions/tree.js
@description
A tree is a simple metaphor on the directorization of ledger.
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
File = require '../../../models/File'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')


treeRoute = (app) ->

  ###*
  Tree Control Route

  @description
  Tree Control Route directives should speak to this guy.
  ###

  app.get '/v1/file/tree', (req, outerRes, next) ->

    getFileTree = () ->
      def = q.defer()
      File.find().exec (err, fileDataList) ->
        def.resolve true
        return
      def.promise

    getFileTree().then (fileTreeData) ->
      outerRes.json fileTreeData
      return

    return
  return

module.exports = treeRoute
