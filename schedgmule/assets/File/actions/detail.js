
/**
@fileOverview ./schedgmule/assets/File/actions/detail.js
 */
var detailView;

detailView = function(app) {
  app.get('/v1/file', function(req, outerRes, next) {

    /**
    @api {get} /file/ Detail View of File
    @apiName DetailView
    @apiGroup User
    @apiParam {String} path File path.
    @apiDescription
    May essentially contain an explicit statement of the @links and @data 
    attributes of the relevant JSON payload.
     */
    return outerRes.json({
      links: {
        self: ''
      },
      data: [{}]
    });
  });
};

module.exports = detailView;
