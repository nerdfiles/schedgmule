
/**
@fileOverview ./schedgmule/assets/File/actions/tree.js
@description
A tree is a simple metaphor on the directorization of ledger.
 */
var ApiService, AssetData, EndpointManager, File, _, config, e, mongoose, q, request, treeRoute;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

File = require('../../../models/File');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

treeRoute = function(app) {

  /**
  Tree Control Route
  
  @description
  Tree Control Route directives should speak to this guy.
   */
  app.get('/v1/file/tree', function(req, outerRes, next) {
    var getFileTree;
    getFileTree = function() {
      var def;
      def = q.defer();
      File.find().exec(function(err, fileDataList) {
        def.resolve(true);
      });
      return def.promise;
    };
    getFileTree().then(function(fileTreeData) {
      outerRes.json(fileTreeData);
    });
  });
};

module.exports = treeRoute;
