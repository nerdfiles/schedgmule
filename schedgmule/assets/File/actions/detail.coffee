###*
@fileOverview ./schedgmule/assets/File/actions/detail.js
###

detailView = (app) ->

  app.get('/v1/file', (req, outerRes, next) ->

    ###*
    @api {get} /file/ Detail View of File
    @apiName DetailView
    @apiGroup User
    @apiParam {String} path File path.
    @apiDescription
    May essentially contain an explicit statement of the @links and @data 
    attributes of the relevant JSON payload.
    ###

    outerRes.json({
      links: {
        self: ''
      },
      data: [{
      }]
    })
  )

  return

module.exports = detailView

