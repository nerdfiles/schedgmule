var downloadView;

downloadView = function(app) {
  app.get('/v1/file', function(req, outerRes, next) {

    /**
    @api {get} /file/ Download File
    @apiName DownloadFile
    @apiGroup User
    @apiParam {String} path File path.
    @apiDescription
      None.
     */
    return outerRes.json({
      links: {
        self: ''
      },
      data: [{}]
    });
  });
};

module.exports = downloadView;
