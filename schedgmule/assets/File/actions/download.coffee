downloadView = (app) ->

  app.get('/v1/file', (req, outerRes, next) ->

    ###*
    @api {get} /file/ Download File
    @apiName DownloadFile
    @apiGroup User
    @apiParam {String} path File path.
    @apiDescription
      None.
    ###

    outerRes.json({
      links: {
        self: ''
      },
      data: [{
      }]
    })
  )

  return

module.exports = downloadView
