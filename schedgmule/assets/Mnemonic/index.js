
/**
@fileOverview ./schedgmule/assets/Mnemonic/index.js
@description
Mnemonic generation.
 */
var Mnemonic, indexRoute;

Mnemonic = require('bitcore-mnemonic');

indexRoute = function(app) {

  /**
  Index Route for Mnemonic
  @module routes/mnemonic
  @description
  Index Route for Mnemonic generation.
   */
  app.all('/v1/generate/', function(req, outerRes, next) {

    /**
    @api {get} /generate/ Request Passphrase List
    @apiName RequestPassphraseList
    @apiGroup User
    @apiDescription
    Implements https://github.com/openchain/wallet/blob/master/www/js/derive.js
    @apiParam {String} API_TOKEN Issued API Token.
    @apiSuccessExample {json} Success-Response:
        {
          "links": {
            "self" : "http://alpha.schedgmule.com/v1/generate/",
            "next" : "http://alpha.schedgmule.com/v1/generate/"
          },
          "data": [{
            "type": "passphrases",
            "attributes": {
              "title": "Passphrase List"
            },
            "relationships": {
              "passphrases": {
                "links": {
                  "self": "http://alpha.schedgmule.com/v1/generate/"
                },
                "data": [{
                  "id"   : <number>,
                  "type" : "passphrase"
                }]
              }
            }
          }],
          "included": [{
            "id"         : <number>,
            "type"       : "passphrase",
            "attributes" : {
              "passphrase": "<string:mnemonic>"
            }
          }]
        }
     */
    var _id, generatedMnemonic, passphrase, passphraseReponseData, passphraseReponseKey, passphraseResponseList, payloadConstruct;
    generatedMnemonic = new Mnemonic;
    passphrase = generatedMnemonic.toString();
    _id = Math.floor(Math.random() * 100000000);
    passphraseReponseKey = {
      id: _id,
      type: "passphrase"
    };
    passphraseReponseData = {
      id: _id,
      type: "passphrase",
      attributes: {
        passphrase: passphrase
      }
    };
    passphraseResponseList = {
      type: "passphrases",
      attributes: {
        title: "Passphrase List"
      },
      relationships: {
        passphrases: {
          links: {
            self: "http://alpha.schedgmule.com/v1/generate/"
          },
          data: []
        }
      }
    };
    payloadConstruct = {
      links: {
        'self': 'http://alpha.schedgmule.com/v1/generate/',
        'next': 'http://alpha.schedgmule.com/v1/generate/'
      },
      data: [],
      included: []
    };
    passphraseResponseList.relationships.passphrases.data.push(passphraseReponseKey);
    payloadConstruct.data.push(passphraseResponseList);
    payloadConstruct.included.push(passphraseReponseData);
    outerRes.json(payloadConstruct);
  });
};

module.exports = indexRoute;
