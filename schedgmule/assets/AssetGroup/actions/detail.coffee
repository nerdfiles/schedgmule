###*
@fileOverview ./schedgmule/assets/AssetGroup/actions/list.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
AssetGroup = require '../../../models/AssetGroup'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

detailView = (app) ->
  app.get '/v1/assetGroup/:api', (req, outerRes, next) ->
    assetGroupApi = req.params.api or null
    if !assetGroupApi
      outerRes.json error: "No AssetGroup API Number provided."
      return

    AssetGroup.findOne(assetGroupApi: assetGroupApi).exec (err, instance) ->
      outerRes.json instance
    , () ->
      outerRes.json error: "No AssetGroup instance found."
    return
  return

module.exports = detailView
