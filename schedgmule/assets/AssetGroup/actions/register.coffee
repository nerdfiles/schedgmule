###*
@fileOverview ./schedgmule/assets/AssetGroup/actions/register.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
AssetGroup = require '../../../models/AssetGroup'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')
TransactionBuilder = require('../../../services/transactionBuilder')
Validator = require('../../../services/validator')

registerView = (app) ->

  app.post('/v1/assetGroup', (req, outerRes, next) ->

    ###*
    @api
    @apiDescription
    Register AssetGroup.
    @apiParamExample {json} Request-Example:
        {
            "assetGroupApi" : <string>,
            "geoCoordinates" : {
                "address" : {
                    "streetAddress1" : <string>,
                    "streetAddress2" : <string>,
                    "city"           : <string>,
                    "state"          : <string>,
                    "postalCode"     : <string>
                },
                "latitude"  : <number>,
                "longitude" : <number>
            }
        }
    ###

    if !req.body
      return

    body = req.body
    preparedAssetGroupData = _.extend({
      dateAdded: new Date
    }, body)

    assetGroup = new AssetGroup preparedAssetGroupData

    assetGroup.post 'save', (savedAssetGroup) ->
      console.log savedAssetGroup

    assetGroup.save (savedAssetGroup) ->
      outerRes.json savedAssetGroup
      return
    , (error) ->
      outerRes.json error: error
      return
    return
  )

  return

module.exports = registerView

