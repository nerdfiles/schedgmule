###*
@fileOverview ./schedgmule/assets/AssetGroup/actions/list.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
AssetGroup = require '../../../models/AssetGroup'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

listView = (app) ->
  app.get '/v1/assetGroup', (req, res, next) ->

    AssetGroup.find().exec (err, instanceList) ->
      outerRes.json instanceList
    , () ->
      outerRes.json error: "No AssetGroup data found."
    return
  return

module.exports = listView


