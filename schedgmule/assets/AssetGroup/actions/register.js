
/**
@fileOverview ./schedgmule/assets/AssetGroup/actions/register.js
 */
var ApiService, AssetData, AssetGroup, EndpointManager, TransactionBuilder, Validator, _, config, e, mongoose, q, registerView, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

AssetGroup = require('../../../models/AssetGroup');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

TransactionBuilder = require('../../../services/transactionBuilder');

Validator = require('../../../services/validator');

registerView = function(app) {
  app.post('/v1/assetGroup', function(req, outerRes, next) {

    /**
    @api
    @apiDescription
    Register AssetGroup.
    @apiParamExample {json} Request-Example:
        {
            "assetGroupApi" : <string>,
            "geoCoordinates" : {
                "address" : {
                    "streetAddress1" : <string>,
                    "streetAddress2" : <string>,
                    "city"           : <string>,
                    "state"          : <string>,
                    "postalCode"     : <string>
                },
                "latitude"  : <number>,
                "longitude" : <number>
            }
        }
     */
    var assetGroup, body, preparedAssetGroupData;
    if (!req.body) {
      return;
    }
    body = req.body;
    preparedAssetGroupData = _.extend({
      dateAdded: new Date
    }, body);
    assetGroup = new AssetGroup(preparedAssetGroupData);
    assetGroup.post('save', function(savedAssetGroup) {
      return console.log(savedAssetGroup);
    });
    assetGroup.save(function(savedAssetGroup) {
      outerRes.json(savedAssetGroup);
    }, function(error) {
      outerRes.json({
        error: error
      });
    });
  });
};

module.exports = registerView;
