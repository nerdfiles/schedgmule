
/**
@fileOverview ./schedgmule/assets/BaseModel/actions/register.js
@description
Create BaseModel as Extension of http://schema.org/DataCatalog
 */
var BaseModel, DefaultTransaction, _, config, e, mongoose, q, registerView, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

BaseModel = require('../../../models/BaseModel');

DefaultTransaction = require('../../../utils/defaultTransaction');

registerView = function(app) {

  /**
  @namespace /v1/coringsample/
   */
  app.post('/v1/coringsample', function(req, outerRes, next) {

    /**
    @api
    @apiName RegisterBaseModel
    @apiDescription `sendTo` must be the master ACC — `sendTo` will be handled 
    implicitly. `currentRecord` must be the relevant "originator_permission". 
    The given client should select the relevant permission from the given ACC 
    assets in order to pass the permission to the server-side as 
    `currentRecord.`
    @apiParamExample {json} Request-Example:
    
        {
          "assetGroupApi"     : <string>,
          "dateCreated" : <string>,
          "sampleType" : [
            <string>
          ],
          "depthRange" : {
            "min" : <number>,
            "max" : <number>
          },
          "author" : <string>,
          "permission" : {
            "currentRecord"   : <object>,
            "assetDefinition" : <object>,
            "endpoint"        : <array>,
            "privateKey"      : <string>,
            "publicKey"       : <string>
          }
        }
     */
    var account, assetDefinition, assetGroupApi, body, currentRecord, dt, endpoint, invoiceConstruct, metadataConstruct, preparedBaseModelData, privateKey, publicKey, sendAmount, sendTo;
    body = req.body || null;
    assetGroupApi = body.assetGroupApi;
    if (!body) {
      outerRes.status(404).json({
        error: "No registration data provided for Base Model."
      });
      return;
    }
    sendTo = body.sendTo || '@sparecraft_permission';
    sendAmount = 1;
    currentRecord = body.permission.currentRecord;
    assetDefinition = body.permission.assetDefinition;
    endpoint = body.permission.endpoint;
    privateKey = body.permission.privateKey || null;
    publicKey = body.permission.publicKey || null;
    account = currentRecord.account;
    if (!assetGroupApi) {
      outerRes.status(404).json({
        error: "No AssetGroup API provided."
      });
      return;
    }
    if (!currentRecord) {
      outerRes.status(404).json({
        error: "No asset permission provided."
      });
      return;
    }
    invoiceConstruct = {
      sendTo: sendTo,
      account: account,
      sendAmount: sendAmount,
      currentRecord: currentRecord,
      endpoint: endpoint,
      privateKey: privateKey,
      publicKey: publicKey
    };
    preparedBaseModelData = {
      dateCreated: body.dateCreated || new Date,
      sampleType: body.sampleType,
      depthRange: body.depthRange,
      author: account,
      assetGroupApi: body.assetGroupApi
    };
    metadataConstruct = preparedBaseModelData;
    dt = new DefaultTransaction(invoiceConstruct, metadataConstruct);
    dt.create().then(function(postedTransaction) {
      var coringsample;
      console.log(postedTransaction);
      coringsample = new BaseModel(preparedBaseModelData);
      coringsample.post('save', function() {
        console.log('Successfully saved Base Model after transaction attempt...');
        console.log(postedTransaction);
      });
      return coringsample.save(function() {
        outerRes.json(postedTransaction);
      }, function(error) {
        outerRes.status(500).json({
          error: error
        });
      });
    }, function(error) {
      outerRes.status(500).json({
        error: error
      });
    });
  });
};

module.exports = registerView;
