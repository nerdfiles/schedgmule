###*
@fileOverview ./schedgmule/assets/BaseModel/actions/register.js
@description
Create BaseModel as Extension of http://schema.org/DataCatalog
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
BaseModel = require '../../../models/BaseModel'
DefaultTransaction = require '../../../utils/defaultTransaction'

#uuid = require('node-uuid')
#SHA256 = require('crypto-js/sha256')
#SHA256( {{data}} )

registerView = (app) ->

  ###*
  @namespace /v1/coringsample/
  ###

  app.post '/v1/coringsample', (req, outerRes, next) ->

    ###*
    @api
    @apiName RegisterBaseModel
    @apiDescription `sendTo` must be the master ACC — `sendTo` will be handled 
    implicitly. `currentRecord` must be the relevant "originator_permission". 
    The given client should select the relevant permission from the given ACC 
    assets in order to pass the permission to the server-side as 
    `currentRecord.`
    @apiParamExample {json} Request-Example:

        {
          "assetGroupApi"     : <string>,
          "dateCreated" : <string>,
          "sampleType" : [
            <string>
          ],
          "depthRange" : {
            "min" : <number>,
            "max" : <number>
          },
          "author" : <string>,
          "permission" : {
            "currentRecord"   : <object>,
            "assetDefinition" : <object>,
            "endpoint"        : <array>,
            "privateKey"      : <string>,
            "publicKey"       : <string>
          }
        }

    ###

    body = req.body or null
    assetGroupApi = body.assetGroupApi

    if !body
      outerRes.status(404).json error: "No registration data provided for Base Model."
      return

    sendTo = body.sendTo or '@sparecraft_permission' # send back to the originator (technically)
    sendAmount = 1

    currentRecord = body.permission.currentRecord
    assetDefinition = body.permission.assetDefinition
    endpoint = body.permission.endpoint
    privateKey = body.permission.privateKey or null
    publicKey = body.permission.publicKey or null
    account = currentRecord.account

    if !assetGroupApi
      outerRes.status(404).json error: "No AssetGroup API provided."
      return

    if !currentRecord
      outerRes.status(404).json error: "No asset permission provided."
      return

    invoiceConstruct =
      sendTo        : sendTo
      account       : account
      sendAmount    : sendAmount
      currentRecord : currentRecord
      endpoint      : endpoint
      privateKey    : privateKey
      publicKey     : publicKey

    preparedBaseModelData =
      dateCreated : body.dateCreated or new Date
      sampleType  : body.sampleType
      depthRange  : body.depthRange
      author      : account
      assetGroupApi     : body.assetGroupApi

    metadataConstruct = preparedBaseModelData

    dt = new DefaultTransaction invoiceConstruct, metadataConstruct

    dt.create().then (postedTransaction) ->
      console.log postedTransaction
      coringsample = new BaseModel preparedBaseModelData

      coringsample.post 'save', () ->
        console.log 'Successfully saved Base Model after transaction attempt...'
        console.log postedTransaction
        return

      coringsample.save () ->
        outerRes.json postedTransaction
        return
      , (error) ->
        outerRes.status(500).json error: error
        return

    , (error) ->
      outerRes.status(500).json error: error
      return

    return
  return

module.exports = registerView
