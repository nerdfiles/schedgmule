
/**
@fileOverview ./schedgmule/assets/BaseModel/actions/list.js
 */
var ApiService, AssetData, BaseModel, EndpointManager, _, config, listView, mongoose, request;

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

BaseModel = require('../../../models/BaseModel');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

listView = function(app) {

  /**
  @description
  List of BaseModels.
   */
  app.get('/v1/assetGroup', function(req, outerRes, next) {
    var mapBaseModels;
    mapBaseModels = function(err, data) {

      /**
      @inner
      @description
      Map BaseModels.
       */
      var uuidList;
      uuidList = data.map(function(coringSample) {
        return coringSample.uuid;
      });
      outerRes.json(uuidList);
    };
    BaseModel.find().exec(mapBaseModels);
  });
};

module.exports = listView;
