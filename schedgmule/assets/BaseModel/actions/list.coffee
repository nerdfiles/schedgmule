###*
@fileOverview ./schedgmule/assets/BaseModel/actions/list.js
###

#try
  #q = require 'q'
#catch e
  #q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
BaseModel = require '../../../models/BaseModel'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

listView = (app) ->

  ###*
  @description
  List of BaseModels.
  ###

  app.get '/v1/assetGroup', (req, outerRes, next) ->

    #def = q.defer()

    mapBaseModels = (err, data) ->

      ###*
      @inner
      @description
      Map BaseModels.
      ###

      uuidList = data.map((coringSample) ->
        return coringSample.uuid
      )

      #def.resolve(uuidList)
      outerRes.json uuidList
      #def.promise
      return

    BaseModel.find().exec(mapBaseModels)

    return
  return

module.exports = listView

