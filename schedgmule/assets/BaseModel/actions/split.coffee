###*
@fileOverview ./schedgmule/assets/BaseModel/actions/split.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
BaseModel = require '../../../models/BaseModel'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')


splitRoute = (app) ->

  ###*
  @api
  @apiName SplitBaseModel
  @apiParamExample {json} Request-Example:
      {
          "uuid": <string>,
          "newBaseModel": {
            "range": {
              "min": <number>,
              "max": <number
            }
          }
      }
  ###

  app.post '/v1/coringsample/:uuid/split', (req, outerRes, next) ->

    BaseModel.findOne(uuid: uuid).exec (err, instance) ->
      if err
        outerRes.json error: err
        return

      BaseModel.post 'save', (err, instance) ->
        return

      BaseModel.save (err, instance) ->
        outerRes.json instance
        return

    return
  return

module.exports = splitRoute
