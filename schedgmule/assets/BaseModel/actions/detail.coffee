###*
@fileOverview ./schedgmule/assets/BaseModel/actions/list.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
BaseModel = require '../../../models/BaseModel'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

detailView = (app) ->

  ###*
  Detail View for Base Model
  @description
  Should provide @links on the payload for discoverability.
  ###

  app.get '/v1/coringsample/:uuid', (req, outerRes, next) ->

    uuid = req.params.uuid or null

    if !uuid
      outerRes.json error: "No Base Model ID provided."
      return

    BaseModel.findOne(uuid: uuid).exec (err, instance) ->
      if err
        outerRes.json error: "No Base Model instance found."
      outerRes.json instance
      return

    return
  return

module.exports = detailView

