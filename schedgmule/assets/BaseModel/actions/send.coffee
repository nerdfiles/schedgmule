###*
@fileOverview ./schedgmule/assets/BaseModel/actions/send.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
BaseModel = require '../../../models/BaseModel'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')

sendView = (app) ->

  ###*
  @api
  @apiName SendBaseModel
  @apiDescription
  Sending Base Models to other users by Public Key.
  ###

  app.post '/v1/coringsample/:uuid/send', (req, outerRes, next) ->
    uuid = req.params.uuid or null

    if !uuid
      outerRes.json error: "No coring sample ID provided."
      return

    BaseModel.findOne(uuid: uuid).exec (err, instance) ->
      if err
        outerRes.json error: "No Base Model instance found."
      outerRes.json instance
      return
    return
  return

module.exports = sendView
