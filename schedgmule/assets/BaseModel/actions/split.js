
/**
@fileOverview ./schedgmule/assets/BaseModel/actions/split.js
 */
var ApiService, AssetData, BaseModel, EndpointManager, _, config, e, mongoose, q, request, splitRoute;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

BaseModel = require('../../../models/BaseModel');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

splitRoute = function(app) {

  /**
  @api
  @apiName SplitBaseModel
  @apiParamExample {json} Request-Example:
      {
          "uuid": <string>,
          "newBaseModel": {
            "range": {
              "min": <number>,
              "max": <number
            }
          }
      }
   */
  app.post('/v1/coringsample/:uuid/split', function(req, outerRes, next) {
    BaseModel.findOne({
      uuid: uuid
    }).exec(function(err, instance) {
      if (err) {
        outerRes.json({
          error: err
        });
        return;
      }
      BaseModel.post('save', function(err, instance) {});
      return BaseModel.save(function(err, instance) {
        outerRes.json(instance);
      });
    });
  });
};

module.exports = splitRoute;
