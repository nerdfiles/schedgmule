
/**
@fileOverview ./schedgmule/assets/BaseModel/actions/list.js
 */
var ApiService, AssetData, BaseModel, EndpointManager, _, config, detailView, e, mongoose, q, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

BaseModel = require('../../../models/BaseModel');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

detailView = function(app) {

  /**
  Detail View for Base Model
  @description
  Should provide @links on the payload for discoverability.
   */
  app.get('/v1/coringsample/:uuid', function(req, outerRes, next) {
    var uuid;
    uuid = req.params.uuid || null;
    if (!uuid) {
      outerRes.json({
        error: "No Base Model ID provided."
      });
      return;
    }
    BaseModel.findOne({
      uuid: uuid
    }).exec(function(err, instance) {
      if (err) {
        outerRes.json({
          error: "No Base Model instance found."
        });
      }
      outerRes.json(instance);
    });
  });
};

module.exports = detailView;
