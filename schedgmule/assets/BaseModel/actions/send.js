
/**
@fileOverview ./schedgmule/assets/BaseModel/actions/send.js
 */
var ApiService, AssetData, BaseModel, EndpointManager, _, config, e, mongoose, q, request, sendView;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

BaseModel = require('../../../models/BaseModel');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

sendView = function(app) {

  /**
  @api
  @apiName SendBaseModel
  @apiDescription
  Sending Base Models to other users by Public Key.
   */
  app.post('/v1/coringsample/:uuid/send', function(req, outerRes, next) {
    var uuid;
    uuid = req.params.uuid || null;
    if (!uuid) {
      outerRes.json({
        error: "No coring sample ID provided."
      });
      return;
    }
    BaseModel.findOne({
      uuid: uuid
    }).exec(function(err, instance) {
      if (err) {
        outerRes.json({
          error: "No Base Model instance found."
        });
      }
      outerRes.json(instance);
    });
  });
};

module.exports = sendView;
