
/**
 * @fileOver schedgmule/assets/BaseModel/index.js
 * @description
 * Base Model covers a range of core schedgmule capabilities:  
 * 1. /basemodel   GET   Download Asset Definition of Base Model for Account.  
 * 2.                 POST  Register a new Base Model.
 */
var ApiService, AssetData, _, bytebuffer, config, e, indexRoute, long, q, request;

_ = require('lodash');

request = require('request');

config = require('../../config');

bytebuffer = require('bytebuffer');

long = require('long');

ApiService = require('../../services/api');

AssetData = require('../../services/assetData');

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

indexRoute = function(app) {

  /**
  Index Route
  @module routes/coringsample
  @description
  Base Model Route Module Interface.
   */
  var coringSampleDetail, coringSampleList, coringSampleRegister, coringSampleSend, coringSampleSplit;
  coringSampleDetail = require('./actions/detail')(app);
  coringSampleList = require('./actions/list')(app);
  coringSampleRegister = require('./actions/register')(app);
  coringSampleSend = require('./actions/send')(app);
  coringSampleSplit = require('./actions/split')(app);
};

module.exports = indexRoute;
