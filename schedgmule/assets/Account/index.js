
/**
@fileOverview ./schedgmule/assets/Account/index.js
@description
Account Index covers a range of core schedgmule capabilities:  
1. /account    GET   Wrapper for query/account in Openchain.  
2.             POST  Access wallet.  
3. /register   GET   Generation of Mnemonics for wallet access.
 */
var indexRoute;

indexRoute = function(app) {

  /**
  Index Route
  @module routes/account
  @description
  Index Module for Account. Prepares all HTTP VERBS for Accounts.
  @param {Object:Express} app An Express instance.
  @return {undefined}
   */
  var accountGetAll, accountGetUnique, accountLogin, accountOptions, accountSettings;
  accountOptions = require('./actions/options')(app);
  accountLogin = require('./actions/login')(app);
  accountGetUnique = require('./actions/get')(app);
  accountGetAll = require('./actions/getAll')(app);
  accountSettings = require('./actions/settings')(app);
};

module.exports = indexRoute;
