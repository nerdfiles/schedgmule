
/**
@fileOverview ./schedgmule/assets/Account/actions/settings.js
 */
var settingsRoute;

settingsRoute = function(app) {

  /**
  @namespace /v1/account/settings
   */
  app.post('/v1/account/settings', function(req, outerRes, next) {

    /**
    @api {post} /account/settings/ Create Root Account Settings
    @apiName CreateRootAccountSettings
    @apiGroup P2PKH
     */
    outerRes.json({});
    outerRes.end();
  });
  return app.patch('/v1/account/settings', function(req, outerRes, next) {

    /**
    @api {patch} /account/settings/ Patch Root Account Settings
    @apiName PatchRootAccountSettings
    @apiGroup P2PKH
     */
    outerRes.json({});
    outerRes.end();
  });
};

module.exports = settingsRoute;
