
/**
@fileOverview ./schedgmule/assets/Account/actions/get.js
 */
var Account, ApiService, AssetData, EndpointManager, _, config, e, getAccountRoute, mongoose, q, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

request = require('request');

config = require('../../../config');

Account = require('../../../models/Account');

AssetData = require('../../../services/assetData');

ApiService = require('../../../services/api');

EndpointManager = require('../../../services/endpointManager');

getAccountRoute = function(app) {

  /**
  @namespace /v1/account/:uuid
   */
  return app.get('/v1/account/:uuid', function(req, outerRes, next) {

    /**
    @api {get} /account/:uuid Request Root Account Details
    @apiName RequestRootAccountDetails
    @apiGroup P2PKH
    @apiParam {String} uuid UUID of the given user.
    @apiParam {String} account Derived key of the given user.
    @apiSuccess {Object} accountData An Account document to inspect.
    @apiDescription
    Implements https://docs.openchain.org/en/latest/api/method-calls.html#query-an-account-query-account
     */
    var account, accountPayloadDeferred, balanceDeferred, uuid;
    uuid = req.params.uuid || null;
    account = req.query.account || null;
    accountPayloadDeferred = q.defer();
    balanceDeferred = q.defer();
    if (!uuid || !account) {
      outerRes.json({
        error: "Please provide relevant account credentials."
      });
      outerRes.end();
      return;
    }
    Account.findOne({
      accountId: uuid
    }).exec(function(err, instance) {
      var balance, endpointManager, endpointsList, key, loadEndpoint;
      if (err) {
        outerRes.json({
          error: 'Could not load endpoints.'
        });
        outerRes.end();
        return;
      }
      endpointsList = instance.endpoint.endpoints;
      endpointManager = new EndpointManager(endpointsList);
      balance = [];
      loadEndpoint = function(key) {

        /**
        Load Endpoint from Session or DB instance.
        @function
        @name loadEndpoint
        @inner
        @param {String} key An endpoint key store in user session or on user model.
         */
        var apiService, dataModel, endpoint;
        if (endpointManager && !endpointManager.endpoints) {
          outerRes.json({
            error: 'Could not find endpoints.'
          });
          outerRes.end();
          return;
        }
        endpoint = endpointManager.endpoints[key];
        dataModel = {
          endpoint: endpoint,
          state: "loading",
          assets: []
        };
        apiService = new ApiService;
        return apiService.getAccountAssets(endpoint, account).then(function(_result) {
          _.each(_result, function(itemKey, i) {
            var assetData;
            assetData = new AssetData(endpoint, _result[i].asset);
            assetData.setAccountBalance(_result[i]);
            return dataModel.assets.push(assetData);
          });
          return dataModel.state = "loaded";
        }, function() {
          dataModel.state = "error";
          balance.push(dataModel);
          return balanceDeferred.resolve(balance);
        }).fin(function() {
          if (dataModel.assets.length === 0) {
            balance.push(dataModel);
            balanceDeferred.resolve(balance);
            return;
          }
          return q.allSettled(dataModel.assets.map(function(asset) {
            return asset.fetchAssetDefinition();
          })).done(function(loadedAssets) {
            balance.push(dataModel);
            return balanceDeferred.resolve(balance);
          });
        });
      };
      try {
        for (key in endpointManager.endpoints) {
          loadEndpoint(key);
        }
      } catch (_error) {
        e = _error;
        console.log('Could not load Endpoint Manager.');
        throw e;
      }
      return mongoose.disconnect();
    });
    balanceDeferred.promise.then(function(updatedBalance) {
      var _assets, assets, assetsConstruct;
      _assets = _.last(updatedBalance).assets;
      assets = _.map(_assets, function(asset) {
        return {
          currentRecord: asset.currentRecord,
          assetDefinition: asset.assetDefinition
        };
      });
      assetsConstruct = {
        links: {
          self: "http://alpha.schedgmule.com/v1/account/" + uuid
        },
        data: [
          {
            type: "Available Assets",
            attributes: {
              title: "Asset Data List"
            },
            relationships: {
              assets: []
            }
          }
        ],
        included: assets
      };
      return accountPayloadDeferred.resolve(assetsConstruct);
    });
    return accountPayloadDeferred.promise.then(function(accountPayload) {
      return outerRes.status(200).json(accountPayload).end();
    });
  });
};

module.exports = getAccountRoute;
