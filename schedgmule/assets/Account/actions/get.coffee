###*
@fileOverview ./schedgmule/assets/Account/actions/get.js
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

request = require 'request'
config = require '../../../config'
Account = require '../../../models/Account'
AssetData = require('../../../services/assetData')
ApiService = require('../../../services/api')
EndpointManager = require('../../../services/endpointManager')


getAccountRoute = (app) ->

  ###*
  @namespace /v1/account/:uuid
  ###

  app.get '/v1/account/:uuid', (req, outerRes, next) ->

    ###*
    @api {get} /account/:uuid Request Root Account Details
    @apiName RequestRootAccountDetails
    @apiGroup P2PKH
    @apiParam {String} uuid UUID of the given user.
    @apiParam {String} account Derived key of the given user.
    @apiSuccess {Object} accountData An Account document to inspect.
    @apiDescription
    Implements https://docs.openchain.org/en/latest/api/method-calls.html#query-an-account-query-account
    ###

    uuid = req.params.uuid or null
    account = req.query.account or null
    accountPayloadDeferred = q.defer()
    balanceDeferred = q.defer()

    if !uuid or !account
      outerRes.json error: "Please provide relevant account credentials."
      outerRes.end()
      return

    Account.findOne(accountId: uuid).exec (err, instance) ->

      if err
        outerRes.json error: 'Could not load endpoints.'
        outerRes.end()
        return

      endpointsList = instance.endpoint.endpoints
      endpointManager = new EndpointManager(endpointsList)

      balance = []

      loadEndpoint = (key) ->

        ###*
        Load Endpoint from Session or DB instance.
        @function
        @name loadEndpoint
        @inner
        @param {String} key An endpoint key store in user session or on user model.
        ###

        if endpointManager and !endpointManager.endpoints
          outerRes.json error: 'Could not find endpoints.'
          #mongoose.disconnect()
          outerRes.end()
          return

        endpoint = endpointManager.endpoints[key]

        dataModel =
          endpoint : endpoint,
          state    : "loading",
          assets   : []

        apiService = new ApiService
        apiService.getAccountAssets(endpoint, account).then((_result) ->
          _.each _result, (itemKey, i) ->

            assetData = new AssetData(endpoint, _result[i].asset)
            assetData.setAccountBalance(_result[i])
            dataModel.assets.push(assetData)

          dataModel.state = "loaded"
        , () ->
          dataModel.state = "error"
          balance.push(dataModel)
          balanceDeferred.resolve balance
        ).fin () ->

          if dataModel.assets.length == 0
            balance.push(dataModel)
            balanceDeferred.resolve balance
            return

          q.allSettled(dataModel.assets.map((asset) ->
            asset.fetchAssetDefinition()
          )).done (loadedAssets) ->
            #req.session.loadedAssets = loadedAssets
            balance.push(dataModel)
            balanceDeferred.resolve balance

      try
        for key of endpointManager.endpoints
          loadEndpoint(key)
      catch e
        console.log 'Could not load Endpoint Manager.'
        throw e

      mongoose.disconnect()

    balanceDeferred.promise.then (updatedBalance) ->

      _assets = _.last(updatedBalance).assets
      assets = _.map _assets, (asset) ->
        {
          currentRecord: asset.currentRecord,
          assetDefinition: asset.assetDefinition
        }

      assetsConstruct =
        links:
          self: "http://alpha.schedgmule.com/v1/account/" + uuid
        data: [{
          type: "Available Assets"
          attributes:
            title: "Asset Data List"
          relationships:
            assets: []
        }]
        included: assets # frex. endpoint.assets.{{publickey}}.name:'sparecraft_permission' — should become "currentRecord" to pass with transaction
      #req.session.loadedAssets = assets

      accountPayloadDeferred.resolve assetsConstruct

    accountPayloadDeferred.promise.then (accountPayload) ->
      outerRes.status(200).json(accountPayload).end()

module.exports = getAccountRoute
