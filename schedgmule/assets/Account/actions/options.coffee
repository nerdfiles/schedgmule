###*
@fileOverview ./schedgmule/assets/Account/actions/options.js
###

accountOptionsRoute = (app) ->

  app.options '/v1/account/', (req, outerRes, next) ->

    ###*
    @api {options} /account/ Info Root Account
    @apiName InfoRootAccount
    @apiGroup User
    @apiParam {String} API_TOKEN Issued API Token.
    ###

    outerRes.status(200).send null
    outerRes.end()
    return

module.exports = accountOptionsRoute
