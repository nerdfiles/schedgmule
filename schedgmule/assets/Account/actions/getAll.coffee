###*
@fileOverview ./schedgmule/assets/Account/actions/getAll.js
###

getAllRoute = (app) ->

  ###*
  @namespace /v1/account/
  ###

  app.get '/v1/account/', (req, outerRes, next) ->

    ###*
    @api {get} /account/ RequestAllAccounts
    @apiName RequestAllAccounts
    @apiGroup User
    @apiParam {String} account Derived key of the given user.
    @apiSuccess {Object} accountsList An Accounts list document to inspect.
    @apiDescription
    Get all acounts.
    ###

    outerRes.json [{}]
    outerRes.end()
    return

module.exports = getAllRoute
