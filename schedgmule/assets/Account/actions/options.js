
/**
@fileOverview ./schedgmule/assets/Account/actions/options.js
 */
var accountOptionsRoute;

accountOptionsRoute = function(app) {
  return app.options('/v1/account/', function(req, outerRes, next) {

    /**
    @api {options} /account/ Info Root Account
    @apiName InfoRootAccount
    @apiGroup User
    @apiParam {String} API_TOKEN Issued API Token.
     */
    outerRes.status(200).send(null);
    outerRes.end();
  });
};

module.exports = accountOptionsRoute;
