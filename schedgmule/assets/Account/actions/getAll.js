
/**
@fileOverview ./schedgmule/assets/Account/actions/getAll.js
 */
var getAllRoute;

getAllRoute = function(app) {

  /**
  @namespace /v1/account/
   */
  return app.get('/v1/account/', function(req, outerRes, next) {

    /**
    @api {get} /account/ RequestAllAccounts
    @apiName RequestAllAccounts
    @apiGroup User
    @apiParam {String} account Derived key of the given user.
    @apiSuccess {Object} accountsList An Accounts list document to inspect.
    @apiDescription
    Get all acounts.
     */
    outerRes.json([{}]);
    outerRes.end();
  });
};

module.exports = getAllRoute;
