
/**
@fileOverview ./schedgmule/assets/Account/actions/login.js
 */
var Account, Cryptish, Endpoint, Mnemonic, bitcore, config, e, loginRoute, mongoose, q;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

bitcore = require('bitcore');

Mnemonic = require('bitcore-mnemonic');

mongoose = require('mongoose');

config = require('../../../config');

Cryptish = require('../../../utils/cryptish');

Account = require('../../../models/Account');

Endpoint = require('../../../models/Endpoint');

loginRoute = function(app) {

  /**
  @namespace /v1/account/login/
   */
  app.post('/v1/account/login', function(req, outerRes, next) {

    /**
    @api {post} /account/login/ Login Root Account
    @apiName LoginRootAccount
    @apiGroup User
    @apiParam {String} API_TOKEN Issued API Token.
    @apiParamExample {json} Request-Example:
        {
          "uuid" : <string>
        }
    @apiSuccessExample {json} Success-Response:
        {
          "links": {
            "self": "http://alpha.schedgmule.com/v1/account/login/"
          },
          "data": [{
            "type": "authenticationContract",
            "attributes": {
              "title": "Account Authentication"
            },
            "relationships": {
              "keys": {
                "links": {
                  "self": "http://alpha.schedgmule.com/v1/account/login/"
                },
                "data": [{
                  "id"   : <number>,
                  "type" : "derivedKey"
                }]
              }
            }
          }],
          "included": [{
            "id": <number>,
            "type": "derivedKey",
            "attributes": {
              "rawAddress"  : <string>,
              "rootAccount" : <string>,
              "initialized" : <boolean>
            }
          }]
        }
     */
    var _defaultEndpoint, createKey, uuid;
    try {
      uuid = req.body.uuid || null;
    } catch (_error) {
      e = _error;
      outerRes.json({
        error: '404'
      });
      outerRes.end();
      return;
    }
    _defaultEndpoint = req.body.defaultEndpoint || null;
    createKey = function(uuid) {

      /**
      Create Key
      
      @name createKey
      @param {string} uuid A BIP39-compliant Mnemonic string.
      @emits Mnemonic#keyConstruct
      @return {Promise:object} HD Private Key to be used in Endpoint Response
      within the "openchain" namespace.
       */
      var bitcoreNetworksConfig, def, livenet;
      def = q.defer();
      app.on('event:keyConstructPublish', function(hdkey) {

        /**
        Key Construction Publish to livenet.
        @listens Mnemonic:keyConstructPublish
         */
        var _hdPrivateKey, derivedKey, hdPrivateKey;
        if (!hdkey) {
          def.reject(null);
          return;
        }
        hdPrivateKey = new bitcore.HDPrivateKey(hdkey.data);
        hdPrivateKey.network = bitcore.Networks.get('openchain');
        derivedKey = hdPrivateKey.derive(44, true).derive(64, true).derive(0, true).derive(0).derive(0);
        _hdPrivateKey = {};
        _hdPrivateKey.rawAddress = derivedKey.privateKey.toAddress().toString();
        _hdPrivateKey.rootAccount = '/p2pkh/' + derivedKey.privateKey.toAddress().toString() + '/';
        _hdPrivateKey.derivedKey = derivedKey;
        _hdPrivateKey.initialized = true;
        def.resolve(_hdPrivateKey);
      });
      app.emit('event:keyConstruct', uuid);
      livenet = bitcore.Networks.get('livenet');
      bitcoreNetworksConfig = {
        name: 'openchain',
        alias: 'openchain',
        pubkeyhash: 76,
        privatekey: livenet.privatekey,
        scripthash: 78,
        xpubkey: livenet.xpubkey,
        xprivkey: livenet.xprivkey,
        networkMagic: 0,
        port: livenet.port,
        dnsSeeds: livenet.dnsSeeds
      };
      bitcore.Networks.add(bitcoreNetworksConfig);
      return def.promise;
    };
    app.on('event:keyConstruct', function(uuid) {

      /**
      Event for key construction from HD Private Key.
      @param {string} mnemonic
      @listens Mnemonic#keyConstruct
      @emits Mnemonic#keyConstructPublish
       */
      var code, def, derivedKey;
      code = null;
      derivedKey = null;
      def = q.defer();
      if (!uuid) {
        return;
      }
      Account.findOne({
        accountId: uuid
      }, function(err, instance) {
        var _cryptish, account, defaultEndpoint, encUuid, mnemonic;
        _cryptish = new Cryptish('aes-256-ctr');
        if (err) {
          def.reject({
            error: 'Could not find UUID.'
          });
          return;
        }
        defaultEndpoint = new Endpoint({
          meta: {
            version: 'v2'
          },
          endpoints: ['http://107.170.46.60:8080/']
        });
        if (instance) {
          console.log('Found User: ' + uuid);
          console.log('With data: ');
          console.log(instance);
          if (!instance.endpoint) {
            console.log('Could not find endpoint, adding...');
            instance.endpoint = defaultEndpoint;
            instance.markModified('endpoint');
            instance.save(function() {
              return console.log('Successfully updated endpoint on account!');
            });
          }
          mnemonic = _cryptish.decrypt(instance.meta.dk, uuid);
          if (Mnemonic.isValid(mnemonic)) {
            code = new Mnemonic(mnemonic);
            def.resolve(code);
          } else {
            def.reject({
              error: "Mnemonic is invalid."
            });
          }
        } else {
          code = new Mnemonic();
          encUuid = _cryptish.encrypt(code.toString(), uuid);
          account = new Account({
            accountId: uuid,
            meta: {
              dk: encUuid
            }
          });
          account.endpoint = defaultEndpoint;
          console.log(account);
          account.save(function(error, instance) {
            var errorLabel;
            if (!error) {
              console.log('Writing Account: ', code);
              def.resolve(code);
            } else {
              errorLabel = "Could not save UUID.";
              console.log(errorLabel);
              def.reject({
                error: error
              });
            }
            return mongoose.disconnect();
          });
        }
        def.promise.then(function(_code) {
          derivedKey = _code.toHDPrivateKey(null, 'livenet');
          return app.emit('event:keyConstructPublish', derivedKey);
        }, function(err) {
          return console.log('Error in UUID:', err);
        });
      });
    });
    createKey(uuid).then(function(k) {
      var _id, authenticationList, authenticationPayload, keyData, keyKey;
      console.log('Attempting Authentication Handshake...');
      authenticationPayload = {
        links: {
          self: "http://alpha.schedgmule.com/v1/account/login/"
        },
        data: [],
        included: []
      };
      _id = Math.floor(Math.random() * 10000000);
      authenticationList = {
        type: "authenticationContract",
        attributes: {
          title: "Account Authentication"
        },
        relationships: {
          keys: {
            links: {
              self: "http://alpha.schedgmule.com/v1/account/login/"
            },
            data: []
          }
        }
      };
      keyKey = {
        id: _id,
        type: "derivedKey"
      };
      keyData = {
        id: _id,
        type: "derivedKey",
        attributes: k
      };
      authenticationList.relationships.keys.data.push(keyKey);
      authenticationPayload.data.push(authenticationList);
      authenticationPayload.included.push(keyData);
      outerRes.json(authenticationPayload);
      outerRes.end();
      next();
    }, function(errorData) {
      outerRes.json({
        error: '404'
      });
      outerRes.end();
      next();
    });
  });
};

module.exports = loginRoute;
