
/**
@fileOverview ./schedgmule/index.js
@description
Main Routes for the schedgmule application. Also initiates bitcore.
 */
var Account, AssetGroup, BaseModel, File, Mnemonic, Note, Search, bitcore, cors, proxy, versionPrefix, versionPrefixControls;

proxy = require('express-http-proxy');

cors = require('cors');

bitcore = require('bitcore');

require('events').EventEmitter.prototype._maxListeners = 100;

Mnemonic = require('./assets/Mnemonic/index');

Account = require('./assets/Account/index');

BaseModel = require('./assets/BaseModel/index');

AssetGroup = require('./assets/AssetGroup/index');

Search = require('./assets/Search/index');

File = require('./assets/File/index');

Note = require('./assets/Note/index');

versionPrefix = '/v1';

versionPrefixControls = '/v1';

module.exports = function(__interface__) {

  /**
  Server interface for modular REST-ful actions and assets.
  @module schedgmule.app/interface
   */
  var accountRoute, allowCrossDomain, assetGroupRoute, coringSampleRoute, corsOptions, fileRoute, mnemonicRoute, searchRoute, whitelist;
  __interface__.versionPrefix = '/v1';
  whitelist = ['*'];
  corsOptions = {
    preflightContinue: true,
    methods: ['GET', 'PUT', 'DELETE', 'PATCH', 'POST', 'OPTIONS'],
    credentials: true,
    origin: function(origin, callback) {
      var originIsWhitelisted;
      originIsWhitelisted = whitelist.indexOf(origin) !== -1;
      callback(null, originIsWhitelisted);
    }
  };
  allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST,GET,PUT,PATCH,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With,X-CSRF-TOKEN,Accept,Origin,Authorization');
    next();
  };
  __interface__.app.use(allowCrossDomain);
  mnemonicRoute = new Mnemonic(__interface__.app);
  accountRoute = new Account(__interface__.app);
  coringSampleRoute = new BaseModel(__interface__.app);
  assetGroupRoute = new AssetGroup(__interface__.app);
  searchRoute = new Search(__interface__.app, __interface__.search);
  fileRoute = new File(__interface__.app);
  return __interface__;
};
