
/**
@fileOverview ./schedgmule/theme.js
@description
1. Documentation  
2. API Mocks
 */
var devRouteConfig, express, path;

express = require('express');

path = require('path');

devRouteConfig = function(__interface__) {

  /**
  Dev Route Config
  @module schedgmule.app/dev
   */
  __interface__.app.use('/docs/', express["static"](path.join(__dirname, '../docs')));
  __interface__.app.use('/docs/api', express["static"](path.join(__dirname, '../docs')));
  __interface__.app.use('/docs/src', express["static"](path.join(__dirname, '../src')));
  __interface__.app.use('/docs/analysis', express["static"](path.join(__dirname, '../analysis')));
  __interface__.app.use('/test/unit', express["static"](path.join(__dirname, '../mochawesome-reports')));
  __interface__.app.use('/test/coverage', express["static"](path.join(__dirname, '../test/coverage/PhantomJS\ 1.9.8\ (Mac\ OS\ X\ 0.0.0)')));
  __interface__.app.use('/test/e2e', express["static"](path.join(__dirname, '../test')));
  __interface__.app.use('/bower_components', express["static"](path.join(__dirname, '../bower_components')));
  __interface__.app.use('/api/mocks/v1/', express["static"](path.join(__dirname, './mocks/v1')));
  return __interface__;
};

module.exports = devRouteConfig;
