###*
@fileOverview ./schedgmule/utils/defaultTransaction.js
@description
Default Transaction is a utility-level class.
###

try
  q = require 'q'
catch e
  q = require 'Q'

_ = require 'lodash'
mongoose = require 'mongoose'

bytebuffer = require 'bytebuffer'
request = require 'request'
config = require '../config'
bitcore = require 'bitcore'
AssetData = require('../services/assetData')
ApiService = require('../services/api')
EndpointManager = require('../services/endpointManager')
TransactionBuilder = require('../services/transactionBuilder')
Validator = require('../services/validator')
Long = require 'long'


###*
@class DefaultTransaction
@description
schedgmule encodes and cryptographically secures business object data as DATA 
within Openchain according to the validation rules of the Default Ledger 
behavior. This technological gesture allows for the abstraction of the 
transactional stream into generic action spaces, extending http://schema.org/Action; 
the consequent here is that all "actions" are valued at 1 of all 
permission classes.
###

class DefaultTransaction

  sendAmount        : null
  endpoint          : null
  currentRecord     : null
  account           : null
  sentTo            : null
  privateKey        : null
  publicKey         : null
  metadataConstruct : null

  constructor: (invoiceConstruct={}, metadataConstruct={}) ->

    ###*
    @description
    Constructor for default transaction.
    ###

    @sendTo            = invoiceConstruct.sendTo
    @account           = invoiceConstruct.account
    @privateKey        = invoiceConstruct.privateKey
    @publicKey         = invoiceConstruct.publicKey
    #sendAmount        = Long.fromString(invoiceConstruct.sendAmount)
    @sendAmount        = invoiceConstruct.sendAmount
    @currentRecord             = invoiceConstruct.currentRecord
    @metadataConstruct = metadataConstruct
    @endpoint          = invoiceConstruct.endpoint
    @

  create: () ->

    ###*
    Create Default Transaction
    @function
    @name create
    @memberof! DefaultTransaction#create
    @description
    Create a Promised transaction construct to be passed into a View.
    ###

    def = q.defer()

    if !@sendTo
      def.reject error: "Invalid invoice for transaction construct."

    # @TODO Add metadata record as JSON object of POST to DATA record?
    validator = new Validator
    if validator.isNumber(@sendAmount)

      transactionConstruct = new TransactionBuilder(@endpoint)

      console.dir @endpoint
      console.dir @currentRecord.key
      _key = @currentRecord.key
      _key_buffer = _key.buffer
      _version = @currentRecord.version
      _version_buffer = _version.buffer
      @currentRecord.key = bytebuffer.wrap _key_buffer.data, _key.encoding, _key.littleEndian, _key.noAssert
      @currentRecord.version = bytebuffer.wrap _version_buffer.data, _version.encoding, _version.littleEndian, _version.noAssert

      try
        newSendAmount = new Long(@sendAmount)
        transactionConstruct.addAccountRecord(@currentRecord, newSendAmount.negate())
      catch e
        console.log e

      if !@currentRecord or (@currentRecord and !@currentRecord.asset)
        def.reject error: "No current record provided."
      else

        transactionConstruct.fetchAndAddAccountRecord(@sendTo, @currentRecord.asset, @sendAmount).then (updatedAccountRecord) =>
          _publicKey = @publicKey
          transactionConstruct.submit(@account, @metadataConstruct, @privateKey, _publicKey).then (postedTransaction) ->
            def.resolve postedTransaction
          , () ->
            def.reject error: "Invalid posted transaction."
        , () ->
          def.reject error: "Error in fetchAndAddAccountRecord."

    else
      def.reject error: "Invalid send amount."

    def.promise

module.exports = DefaultTransaction
