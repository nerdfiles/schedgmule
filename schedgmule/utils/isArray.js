
/**
@fileOverview ./schedgmule/utils/isArray.js
 */
var isArray;

isArray = function(value) {
  return value && typeof value === 'object' && value instanceof Array && typeof value.length === 'number' && typeof value.splice === 'function' && !(value.propertyIsEnumerable('length'));
};

module.exports = isArray;
