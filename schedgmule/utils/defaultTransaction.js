
/**
@fileOverview ./schedgmule/utils/defaultTransaction.js
@description
Default Transaction is a utility-level class.
 */
var ApiService, AssetData, DefaultTransaction, EndpointManager, Long, TransactionBuilder, Validator, _, bitcore, bytebuffer, config, e, mongoose, q, request;

try {
  q = require('q');
} catch (_error) {
  e = _error;
  q = require('Q');
}

_ = require('lodash');

mongoose = require('mongoose');

bytebuffer = require('bytebuffer');

request = require('request');

config = require('../config');

bitcore = require('bitcore');

AssetData = require('../services/assetData');

ApiService = require('../services/api');

EndpointManager = require('../services/endpointManager');

TransactionBuilder = require('../services/transactionBuilder');

Validator = require('../services/validator');

Long = require('long');


/**
@class DefaultTransaction
@description
schedgmule encodes and cryptographically secures business object data as DATA 
within Openchain according to the validation rules of the Default Ledger 
behavior. This technological gesture allows for the abstraction of the 
transactional stream into generic action spaces, extending http://schema.org/Action; 
the consequent here is that all "actions" are valued at 1 of all 
permission classes.
 */

DefaultTransaction = (function() {
  DefaultTransaction.prototype.sendAmount = null;

  DefaultTransaction.prototype.endpoint = null;

  DefaultTransaction.prototype.currentRecord = null;

  DefaultTransaction.prototype.account = null;

  DefaultTransaction.prototype.sentTo = null;

  DefaultTransaction.prototype.privateKey = null;

  DefaultTransaction.prototype.publicKey = null;

  DefaultTransaction.prototype.metadataConstruct = null;

  function DefaultTransaction(invoiceConstruct, metadataConstruct) {
    if (invoiceConstruct == null) {
      invoiceConstruct = {};
    }
    if (metadataConstruct == null) {
      metadataConstruct = {};
    }

    /**
    @description
    Constructor for default transaction.
     */
    this.sendTo = invoiceConstruct.sendTo;
    this.account = invoiceConstruct.account;
    this.privateKey = invoiceConstruct.privateKey;
    this.publicKey = invoiceConstruct.publicKey;
    this.sendAmount = invoiceConstruct.sendAmount;
    this.currentRecord = invoiceConstruct.currentRecord;
    this.metadataConstruct = metadataConstruct;
    this.endpoint = invoiceConstruct.endpoint;
    this;
  }

  DefaultTransaction.prototype.create = function() {

    /**
    Create Default Transaction
    @function
    @name create
    @memberof! DefaultTransaction#create
    @description
    Create a Promised transaction construct to be passed into a View.
     */
    var _key, _key_buffer, _version, _version_buffer, def, newSendAmount, transactionConstruct, validator;
    def = q.defer();
    if (!this.sendTo) {
      def.reject({
        error: "Invalid invoice for transaction construct."
      });
    }
    validator = new Validator;
    if (validator.isNumber(this.sendAmount)) {
      transactionConstruct = new TransactionBuilder(this.endpoint);
      console.dir(this.endpoint);
      console.dir(this.currentRecord.key);
      _key = this.currentRecord.key;
      _key_buffer = _key.buffer;
      _version = this.currentRecord.version;
      _version_buffer = _version.buffer;
      this.currentRecord.key = bytebuffer.wrap(_key_buffer.data, _key.encoding, _key.littleEndian, _key.noAssert);
      this.currentRecord.version = bytebuffer.wrap(_version_buffer.data, _version.encoding, _version.littleEndian, _version.noAssert);
      try {
        newSendAmount = new Long(this.sendAmount);
        transactionConstruct.addAccountRecord(this.currentRecord, newSendAmount.negate());
      } catch (_error) {
        e = _error;
        console.log(e);
      }
      if (!this.currentRecord || (this.currentRecord && !this.currentRecord.asset)) {
        def.reject({
          error: "No current record provided."
        });
      } else {
        transactionConstruct.fetchAndAddAccountRecord(this.sendTo, this.currentRecord.asset, this.sendAmount).then((function(_this) {
          return function(updatedAccountRecord) {
            var _publicKey;
            _publicKey = _this.publicKey;
            return transactionConstruct.submit(_this.account, _this.metadataConstruct, _this.privateKey, _publicKey).then(function(postedTransaction) {
              return def.resolve(postedTransaction);
            }, function() {
              return def.reject({
                error: "Invalid posted transaction."
              });
            });
          };
        })(this), function() {
          return def.reject({
            error: "Error in fetchAndAddAccountRecord."
          });
        });
      }
    } else {
      def.reject({
        error: "Invalid send amount."
      });
    }
    return def.promise;
  };

  return DefaultTransaction;

})();

module.exports = DefaultTransaction;
