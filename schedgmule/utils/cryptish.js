
/*
@fileOverview ./schedgmule/utils/cryptish.js
 */
var Cryptish, crypto, root;

crypto = require('crypto');

root = typeof exports !== "undefined" && exports !== null ? exports : this;

module.exports = Cryptish = (function() {
  function Cryptish(algorithm) {
    this.algorithm = algorithm || 'aes-256-ctr';
  }

  Cryptish.prototype.encrypt = function(text, password) {

    /*
    @inner
     */
    var cipher, crypted;
    cipher = crypto.createCipher(this.algorithm, password);
    crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  };

  Cryptish.prototype.decrypt = function(text, password) {

    /*
    @inner
     */
    var dec, decipher;
    decipher = crypto.createDecipher(this.algorithm, password);
    dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  };

  return Cryptish;

})();
