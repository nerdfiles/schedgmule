###
@fileOverview ./schedgmule/utils/cryptish.js
###

crypto = require('crypto')
#console.log 'Loading Cryptish...'
root = exports ? this

module.exports = class Cryptish

  constructor: (algorithm) ->
    #console.log('aes-256-ctr')
    @algorithm = algorithm or 'aes-256-ctr'

  encrypt: (text, password) ->

    ###
    @inner
    ###

    cipher = crypto.createCipher @algorithm, password
    crypted = cipher.update text, 'utf8', 'hex'
    crypted += cipher.final 'hex'
    return crypted

  decrypt: (text, password) ->

    ###
    @inner
    ###

    decipher = crypto.createDecipher @algorithm, password
    dec = decipher.update text, 'hex', 'utf8'
    dec += decipher.final 'utf8'
    return dec

