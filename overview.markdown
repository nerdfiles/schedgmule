# Overview

schedgmule is an asset management middleware for authorization microservices 
facilitated by tokenization strategy implemented in Openchain, a centralized 
cryptographic database which uses distributed observer nodes to preserve 
validity of the transaction stream while anchoring against public blockchains 
to ensure immutability of the transaction stream.

### Generic Workflow

1. Client-node authenticates against application to associate a centrally located 
private key to a user account.
2. Client-node receives authorization asset which authorizes capabilities of the 
application. (Capabilities are 1:1 with Endpoints of the application.)
3. Client-node performs requests for certain capabilities which are themselves 
bound to assets.
4. Therefore, Authorization Assets structure the capabilities which correspond 
to Action Assets. Actions have issuance and Authorizations have issuance.

## Contribute

1. REST-ful Unit Tests http://alpha.schedgmule.com/test/unit/  
   HTTP-based Unit Tests cover test cases for the pure HTTP request outside of the browser. Passing all tests implies that the server is fully capable of serving all HTTP VERBS, for browsers *and* other servers. 
2. E2E Test Coverage http://alpha.schedgmule.com/test/coverage/  
   "Coverage" answers the question: How much code has test coverage? E2E Tests in this case address: Under ideal client-side implementation (assuming all client-side capabilities are normalized and cross-browser implemented, XMLHttpRequest for AJAX), will the code "work"? For instance, ideally code that is 1000 SLOC should have 500+ lines of "coverage" — tests that imply working E2E code (client making requests to server). E2E Tests may also include Events and Behaviors that are purely client-side modeling of "working" behavior.
3. Static Analysis http://alpha.schedgmule.com/docs/analysis  
   Static Analysis describes the Complexity and Difficulty of working within the codebase from a purely syntactical standpoint. Static Analysis for schedgmule currently only applies to the "$PROJECT_ROOT/schedgmule" application module, which lives server-side. No static analysis of the client-side code is relevant at this moment (as of 2016-01-05).
4. Source Code http://alpha.schedgmule.com/docs/src/  
   Source Code Documentation using jsdoc (http://usejsdoc.org/). Describes modules, dependencies, descriptions, parameters, outputs, etc. of the "$PROJECT_ROOT/schedgmule" application module.
5. REST API http://alpha.schedgmule.com/docs/api  
   REST API is described using apiDoc (http://apidocjs.com/).
6. E2E Tests http://alpha.schedgmule.com/test/e2e  
   E2E Tests, unlike 2 which describes *what* of the source code is covered under ideal conditions of client-side requests, the tests themselves describe the actual (1) inputs/outputs and (2) triggers/events within the client-side context. Some overlap exists between Unit Tests of the API (REST-ful Unit Tests) and this aspect of the Test Code Ecosystem, but 6 pertains to the context of which the code is executed (where, when, how — instead of "what"). ***CURRENTLY ONLY WORKS ON LOCAL DEV (SEE SCREENSHOT FOR EXAMPLE OF WHAT TESTS LOOK LIKE*** — These tests can be integrated with Third Party Integration Test systems like Sauce Labs for Automated Browser Testing across platforms and devices.
7. Test Wallet http://alpha.schedgmule.com:8999  
   schedgmule’s Test Wallet for managing Assets.
