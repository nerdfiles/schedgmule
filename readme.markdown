# [Schedgmule](http://schedgmule.com)

This is so badass. The Internet won’t even know what hit it. It’s like finding out your smokin’ hot blind-date loves D&D and hot-cheetos-drilled-into-sour-pickles on the same day. But really, it kind of turns all “permissions” into “feats” modeled in D&D. Sexy, amirite? Yes, your DevOps does think of herself as a Dungeon Master. Who are you trying to kid? Schedgmule is perfect for this kind of person. It’s even better for people who hate spreadsheets. Because fuck spreadsheets.

Imagine HATEOAS fist-pumping a WebSocket with distributed channels in a Web browser all validating 4,000+ transactions at subsecond intervals with bitchin’ modularized Django-like drag-and-drop permissions. Maybe it’s like Smalltalk. Every entity with cryptographically secured contextual actions. I don’t even

And the hottest AngularJS lipstick you could buy in Las Vegas at 3am. Heuristic automation at its finest, generating polynomially time bounded-error for real-time smart contract resolution. In Node.js. With a sweet ass API docs. Oh shit. Schedgmule, who wouldn’t get behind that? Read the [white](https://www.researchgate.net/publication/285288610_File_Microservices_and_Federated_Available_and_Reliable_Reporting_Storage_for_a_Trustless_Co-located_Environment) [papers](https://medium.com/@filesofnerds/modular-composable-domain-restricted-cascading-microauthorizations-in-cryptographic-ledgers-f823aa8757da#.lya485kaw).

## Overview

Schedgmule is a way of using WebSockets as a means to build Fault-Tolerant hypermedia REST microservices with durable, cryptographically localizable authorizations and action spaces (Schema.org Actions). Its goal is to "decentralize the Web" by taking on the definition of the Web as fundamentally a distributed hypermedia application. Schedgmule competes with tools like [Tendermint](http://tendermint.com/) or might support IoT Initiative's like [Lelylan](https://github.com/lelylan/lelylan)'s authentication microservices. 

Schedgmule, however, does not found itself on Proof of Stake or Proof of Work. Rather, Schedgmule brings directorization to the world of tokenized microservices as a client-server architecture for "anchoring" within an ecosystem arguably more decentralized than Bitcoin, yet nevertheless "peggable" to the Bitcoin Blockchain. Fault Tolerance Retry, Parallelism, and other mechanisms for discoverability (Proof of Discovery) are leveraged to demarcate and establish Distributed Availability Routing as Boolean Satisfiability, instead of the "trustless" network-constraint designs of "Stake" or "First", for Time-locked Action Spaces which persist in an HTTP-like fully-distributed software-governed bidirectional-validation mesh network of Oracle nodes and Observer nodes.

Schedgmule is more important than the most important invention since sliced bread. Schedgmule is probably more important than the slice itself; or more important than the first slice ever, anyway. Schedgmule is software's software. It's the light at the end of all Turing Complete Tunnels. Protocols tremble at the idea of being nudged and investigated by Schedgmule. How glorious is this day. 

## Moar features (more badassery)

It’ll be clausal. Like, a query-langauge is in the works. But it’s not a VM. It’s all just 
text, right? Don’t worry about it. One day, you’ll be able to *think* in schedgmulese. It’s 
going to be so sweet. You can qualify schedules with schedules. It’ll be a schedule package 
system; or a schedule calculus.

It’ll take 10 different schedules and mash them all up into the most robust itinerary maximally 
available. It’ll also merge schedules between devices. And allow for easy-to-use integration 
of your disparate schedules into your disparate social media accounts. Pure schedgasm.

## Schedule Meta-marketplaces

Like, this opens up a schedule marketplace for the IoT, bro. Any programmable thing by the 
Internet’s tech stack can consume a sequence of commands. Schedgmule is in the business of 
translating all structured command sets bidirectionally from a Universal Schedule Exchange Format.
This will involve Schema.org, a standards organization. So Standards Compliance and paying homage 
to Open Source is critical. We’ll be hiring OS devs, and going OS all the way. Schedgmule is about 
Universally Standardized Schedules. Fuckin’ A. 

Want a set of scheduled actions for your toaster this holiday season? Schedgmule.
Want to wrangle your workcalendar into your lifecalendar to achieve maximum worklife parity? Schedgmule.
Want to generate a pulsating signature in your favorite dildo’s API? Schedgmule.
Want to coordinate a serialization of concurrently generated datapoints representing constraint-based controls of a rocket? Schedgmule.
Want to schedule the opening of your baby crib based on a low cost WeMo detection system that APIs into your microgrid? Schedgmule.

It’s not that Schedgmule can do anything. It’s that Schedgmule helps you schedule it if you can 
do it, universally, intergalactically. Fuckin’ A.

## Really...?

It’s future-proof schema for human-machine interactions. It’s the Winograd schema of machine intelligence.

    M1: Do X, conditionally Y, adjacently Z.

Where time-bounded error of boolean satisfiability determines the concurrent and asynchronous processing of the veridicality of each clause in the statement.
Every uttered sentence implies a possible world where a certain range of sentences is true and a certain range of sentences is not true. Schedgmule sorts 
through the wheat and the chaff of monolithic schedule complexity to give you and your surrogate intelligences sanity amongst increasing schizoid digital 
realities.

Schedgmule is more like a superhero, akin to the sandwich or the idea of ideas. Schedgmule will save your life.

Learn more about Schedgmule's ['pataphysical options](https://medium.com/@filesofnerds/super-neat-mvp-idea-schedgmule-com-c68c4decc57c#.bxtj11sj3) and dynamical solutions here.

## Installation

Make sure that `npm` and `node` and `bower` are available. To start things off:

    $ (clone repository)
    $ npm install
    $ bower install

We have a hard-dependency on `bitcore`. Ensure that `bitcore` and `bitcore-lib` 
are in:

    $ ls node_modules

If not:

    $ npm install bitcore --save

Other dependencies may be missing:

    $ npm install uuid-key-generator --save
    $ npm install -g mongoose-repl
    $ npm install karma-jasmine karma-requirejs karma-coverage karma-chrome-launcher karma-phantomjs-launcher --save-dev
    $ npm install --save-dev mochawesome
    $ npm install karma-jshint-preprocessor --save-dev
    $ npm install csurf
    $ npm install request-promise
    $ npm install body-parser

## Overview

Read `./overview.markdown` for an overview of Test Code and Documentation links.

## Development

Run the server with `pm2` or `node` itself:

    $ pm2 start server/www

More thoroughly:

    $ pm2 kill; pm2 start server/www; pm2 logs

Or more simply:

    $ node server/www

### Grunt

Install Grunt per your OS. Our main development task is:

    $ grunt devel

### CoffeeScript

Install CoffeeScript per your OS. `grunt-contrib-coffee` is available.

### ElasticSearch

Install ElasticSearch per your OS.

### Mongoose

Install Mongoose per your OS.

### Openchain

Install Openchain per your OS.

## Deployment

Use the following to merge from branch `develop` to branch `master` with `-X theirs`:

    $ grunt deploy/merge/dev

If expecting important merge conflicts:

    $ grunt deploy/pre/dev

Deployments are cancelled if the local E2E Tests do not pass.

## Testing

### Unit Testing API with HTTP

Review `./test/server-tests.js`.

Run tests with:

    $ grunt test/api

### Unit Testing API from Browser

Review `./test/browser-tests.js`.

Run tests with:

    $ grunt test/browser

## Documentation

### API

    $ grunt docs/api

There is a general overview of Schedgmule’s HATEOAS REST-ful strategy at:

    ./schedgmule/README.md

### Analysis

    $ grunt docs/analysis

### Source

    $ grunt docs/src

## Theme

    $ grunt compass

## TODO

1. Integration of Pikulu with angularAMD for Production.
2. Browser-based Viewer of E2E Tests (only works locally right now).

## Contribute

1. http://apidocjs.com/
2. http://usejsdoc.org/
