atomic_action = (res) ->
  body = res.body or {}
  errorLabel = 'Atomic action not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'atomic_action'
    throw new Error(errorLabel)
  return

module.exports = atomic_action
