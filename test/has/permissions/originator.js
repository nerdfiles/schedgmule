var originator;

originator = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Originator send not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'originator_permission') {
    throw new Error(errorLabel);
  }
};

module.exports = originator;
