var metadata_communications;

metadata_communications = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Metadata communications send not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'metadata_communications_permission') {
    throw new Error(errorLabel);
  }
};

module.exports = metadata_communications;
