originator_split = (res) ->
  body = res.body or {}
  errorLabel = 'Split not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'originator_split'
    throw new Error(errorLabel)
  return

module.exports = originator_split




