var atomic_action;

atomic_action = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Atomic action not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'atomic_action') {
    throw new Error(errorLabel);
  }
};

module.exports = atomic_action;
