sparecraft = (res) ->
  body = res.body or {}
  errorLabel = 'Sparecraft send not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'sparecraft_permission'
    throw new Error(errorLabel)
  return

module.exports = sparecraft




