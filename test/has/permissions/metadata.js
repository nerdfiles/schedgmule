var metadata;

metadata = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Metadata upload not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'metadata_permission') {
    throw new Error(errorLabel);
  }
};

module.exports = metadata;
