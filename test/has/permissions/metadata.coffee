metadata = (res) ->
  body = res.body or {}
  errorLabel = 'Metadata upload not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'metadata_permission'
    throw new Error(errorLabel)
  return

module.exports = metadata


