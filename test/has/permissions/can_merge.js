var can_merge;

can_merge = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Can merge not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'can_merge_permission') {
    throw new Error(errorLabel);
  }
};

module.exports = can_merge;
