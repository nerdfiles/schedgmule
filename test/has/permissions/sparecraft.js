var sparecraft;

sparecraft = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Sparecraft send not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'sparecraft_permission') {
    throw new Error(errorLabel);
  }
};

module.exports = sparecraft;
