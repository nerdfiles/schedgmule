var originator_split;

originator_split = function(res) {
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Split not available for account.';
  if (!body.currentRecord || body.currentRecord.name !== 'originator_split') {
    throw new Error(errorLabel);
  }
};

module.exports = originator_split;
