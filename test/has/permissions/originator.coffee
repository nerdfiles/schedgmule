originator = (res) ->
  body = res.body or {}
  errorLabel = 'Originator send not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'originator_permission'
    throw new Error(errorLabel)
  return

module.exports = originator




