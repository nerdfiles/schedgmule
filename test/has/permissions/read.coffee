read = (res) ->
  body = res.body or {}
  errorLabel = 'Read access not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'read_permission'
    throw new Error(errorLabel)
  return

module.exports = read




