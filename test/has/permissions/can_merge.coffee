can_merge = (res) ->
  body = res.body or {}
  errorLabel = 'Can merge not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'can_merge_permission'
    throw new Error(errorLabel)
  return

module.exports = can_merge

