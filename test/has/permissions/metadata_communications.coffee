metadata_communications = (res) ->
  body = res.body or {}
  errorLabel = 'Metadata communications send not available for account.'
  if !body.currentRecord or body.currentRecord.name isnt 'metadata_communications_permission'
    throw new Error(errorLabel)
  return

module.exports = metadata_communications



