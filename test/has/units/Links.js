
/**
@fileOverview ./test/has/units/Links.js
 */
var Links;

Links = function(res) {

  /**
  @function Links
  @memberof external:test/has/units
  @throws Will throw an error if the @links relation is missing from the top-level of the payload.
  @description
  Check if the Success-Response has @links.
  @param {Object} res Response.
  @param {Object} res.links Link relations for discoverability.
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Missing links.';
  if (!body.links) {
    throw new Error(errorLabel);
  }
};

module.exports = Links;
