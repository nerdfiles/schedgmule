
/**
@fileOverview ./test/has/units/Data.js
 */
var Data;

Data = function(res) {

  /**
  Data.
  @function Data
  @memberof external:test/has/units
  @throws Will throw an error if the Object.data property is missing from the payload.
  @description
  Check if the Success-Response has @data.
  @param {Object} res Response.
  @param {Object} res.body.data Data.
  @returns {undefined}
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Missing data.';
  if (!body.data) {
    throw new Error(errorLabel);
  }
};

module.exports = Data;
