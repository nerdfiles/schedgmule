
/**
@fileOverview ./test/has/units/Data.js
 */
var Included;

Included = function(res) {

  /**
  Included.
  @function Included
  @memberof external:test/has/units
  @throws Will throw an error if the Object.data property is missing from the payload.
  @description
  Check if the Success-Response has @data.
  @param {Object} res Response.
  @param {Object} res.body.included Included.
  @returns {undefined}
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Missing included.';
  if (!body.included) {
    throw new Error(errorLabel);
  }
};

module.exports = Included;
