###*
@fileOverview ./test/has/units/PreviousAndNextKeys.js
###

PreviousAndNextKeys = (res) ->

  ###*
  @function PreviousAndNextKeys
  @memberof external:test/has/units
  @throws Will throw an error if the previous and next link relations are not present in the payload.
  @description
  Check if the response has previous and next keys.
  ###

  body = res.body or {}
  if !body.links
    throw new Error('Missing links.')
  if !('next' of body.links)
    throw new Error('Missing next key.')
  if !('prev' of body.links)
    throw new Error('Missing prev key.')
  return

module.exports = PreviousAndNextKeys
