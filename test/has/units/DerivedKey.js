
/**
@fileOverview ./test/has/units/Derivedkey.js
 */
var DerivedKey;

DerivedKey = function(res) {

  /**
  Derived key.
  @function DerivedKey
  @memberof external:test/has/units
  @throws Will throw an error if rootAccount is missing from the payload.
  @description
  Check if the Success-Response has a derived key.
  @param {Object} res Response.
  @param {Object} res.body.included[].attributes.rootAccount Root Account key.
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Missing derived key.';
  if (!body.included[0].attributes.rootAccount) {
    throw new Error(errorLabel);
  }
};

module.exports = DerivedKey;
