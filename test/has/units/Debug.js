
/**
@fileOverview ./test/has/units/Debug.js
 */
var Debug, options, prettyjson;

prettyjson = require('prettyjson');

options = {
  noColor: true
};

Debug = function(res) {

  /**
  Debug.
  @function Debug
  @memberof external:test/has/units
  @description
  For debugging HTTP requests
  @param {Object} res Response.
  @param {Object} res.data Data.
  @returns {undefined}
   */
  var body;
  body = res.body || {};
  console.log(JSON.stringify(body));
};

module.exports = Debug;
