###*
@fileOverview ./test/has/units/Data.js
###

Included = (res) ->

  ###*
  Included.
  @function Included
  @memberof external:test/has/units
  @throws Will throw an error if the Object.data property is missing from the payload.
  @description
  Check if the Success-Response has @data.
  @param {Object} res Response.
  @param {Object} res.body.included Included.
  @returns {undefined}
  ###

  body = res.body or {}
  # Labels
  errorLabel = 'Missing included.'
  # Conditions
  if !body.included
    throw new Error(errorLabel)
  return

module.exports = Included

