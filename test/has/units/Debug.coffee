###*
@fileOverview ./test/has/units/Debug.js
###
prettyjson = require('prettyjson')

options = {
  noColor: true
}

Debug = (res) ->

  ###*
  Debug.
  @function Debug
  @memberof external:test/has/units
  @description
  For debugging HTTP requests
  @param {Object} res Response.
  @param {Object} res.data Data.
  @returns {undefined}
  ###

  body = res.body or {}
  #console.log(prettyjson.render(body, options))
  console.log JSON.stringify(body)
  return

module.exports = Debug
