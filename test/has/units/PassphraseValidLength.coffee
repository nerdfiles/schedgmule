###*
@fileOverview ./test/has/units/PassphraseValidLength.js
###

PassphraseValidLength = (res) ->

  ###*
  @function PassphraseValidLength
  @memberof external:test/has/units
  @throws Will throw an error if the passphrase has an invalid length.
  @description
  Check if the passphrase has the minimum and maximum length.
  ###

  body = res.body or {}
  # Labels
  errorLabel = 'Invalid passphrase length.'
  # Conditions
  if !body.included[0].attributes.passphrase.split(' ').length
    throw new Error(errorLabel)
  return

module.exports = PassphraseValidLength
