###*
@fileOverview ./test/has/units/Data.js
###

Data = (res) ->

  ###*
  Data.
  @function Data
  @memberof external:test/has/units
  @throws Will throw an error if the Object.data property is missing from the payload.
  @description
  Check if the Success-Response has @data.
  @param {Object} res Response.
  @param {Object} res.body.data Data.
  @returns {undefined}
  ###

  body = res.body or {}
  # Labels
  errorLabel = 'Missing data.'
  # Conditions
  if !body.data
    throw new Error(errorLabel)
  return

module.exports = Data
