###*
@fileOverview ./test/has/units/Passphrase.js
###

Passphrase = (res) ->

  ###*
  @function Passphrase
  @memberof external:test/has/units
  @throws Will throw an error if the passphrase payload is not copacetic.
  @description
  Check if the passphrase is available within the atomic document structure.
  ###

  body = res.body or {}
  # Labels
  errorLabel = 'Missing passphrase.'
  errorLabelTampering = 'Payload may be corrupted.'
  errorMessageList = [
    errorLabel
    errorLabelTampering
  ].join('\n')
  # Conditions
  if !body.data[0].relationships.passphrases.data[0].type
    throw new Error(errorMessageList)
  if body.data[0].relationships.passphrases.data[0].type != 'passphrase'
    throw new Error(errorMessageList)
  if !body.included[0].id != !body.data[0].relationships.passphrases.data[0].id
    throw new Error(errorMessageList)
  if body.included[0].type != 'passphrase'
    throw new Error(errorMessageList)
  if !body.included[0].attributes.passphrase
    throw new Error(errorMessageList)
  return

module.exports = Passphrase
