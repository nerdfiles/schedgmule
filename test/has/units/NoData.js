
/**
@fileOverview ./test/has/units/Data.js
 */
var NoData;

NoData = function(res) {

  /**
  No Data.
  @function NoData
  @memberof external:test/has/units
  @throws Will throw an error if the Object.data property is populated from the payload.
  @description
  Confirm that the Success-Response has no populated @data.
  @param {Object} res Response.
  @param {Object} res.data Data.
  @returns {undefined}
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Should not have populated data.';
  if (body.data[0].relationships.assets.length > 0) {
    throw new Error(errorLabel);
  }
};

module.exports = NoData;
