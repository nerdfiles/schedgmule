
/**
@fileOverview ./test/has/units/PreviousAndNextKeys.js
 */
var PreviousAndNextKeys;

PreviousAndNextKeys = function(res) {

  /**
  @function PreviousAndNextKeys
  @memberof external:test/has/units
  @throws Will throw an error if the previous and next link relations are not present in the payload.
  @description
  Check if the response has previous and next keys.
   */
  var body;
  body = res.body || {};
  if (!body.links) {
    throw new Error('Missing links.');
  }
  if (!('next' in body.links)) {
    throw new Error('Missing next key.');
  }
  if (!('prev' in body.links)) {
    throw new Error('Missing prev key.');
  }
};

module.exports = PreviousAndNextKeys;
