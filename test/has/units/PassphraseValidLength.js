
/**
@fileOverview ./test/has/units/PassphraseValidLength.js
 */
var PassphraseValidLength;

PassphraseValidLength = function(res) {

  /**
  @function PassphraseValidLength
  @memberof external:test/has/units
  @throws Will throw an error if the passphrase has an invalid length.
  @description
  Check if the passphrase has the minimum and maximum length.
   */
  var body, errorLabel;
  body = res.body || {};
  errorLabel = 'Invalid passphrase length.';
  if (!body.included[0].attributes.passphrase.split(' ').length) {
    throw new Error(errorLabel);
  }
};

module.exports = PassphraseValidLength;
