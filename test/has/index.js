
/**
@fileOverview ./test/has.js
@external test/has/units
@description
Has namespace.
 */
var Data, Debug, DerivedKey, Links, NoData, Passphrase, PassphraseValidLength, PreviousAndNextKeys, atomic_action, can_merge, has, metadata, metadata_communications, originator, originator_split, read, sparecraft;

Links = require('./units/Links');

Data = require('./units/Data');

NoData = require('./units/NoData');

Debug = require('./units/Debug');

Passphrase = require('./units/Passphrase');

PassphraseValidLength = require('./units/PassphraseValidLength');

PreviousAndNextKeys = require('./units/PreviousAndNextKeys');

DerivedKey = require('./units/DerivedKey');

atomic_action = require('./permissions/atomic_action');

can_merge = require('./permissions/can_merge');

metadata = require('./permissions/metadata');

metadata_communications = require('./permissions/metadata_communications');

originator = require('./permissions/originator');

originator_split = require('./permissions/originator_split');

read = require('./permissions/read');

sparecraft = require('./permissions/sparecraft');

has = (function() {

  /**
  @name test/has
  @function
  @memberof external:test/has/units
  @interface
   */
  var __interface__;
  __interface__ = {};
  __interface__.Links = Links;
  __interface__.Data = Data;
  __interface__.NoData = NoData;
  __interface__.Debug = Debug;
  __interface__.Passphrase = Passphrase;
  __interface__.PassphraseValidLength = PassphraseValidLength;
  __interface__.PreviousAndNextKeys = PreviousAndNextKeys;
  __interface__.DerivedKey = DerivedKey;
  __interface__.atomic_action = atomic_action;
  __interface__.can_merge = can_merge;
  __interface__.metadata = metadata;
  __interface__.metadata_communications = metadata_communications;
  __interface__.originator = originator;
  __interface__.originator_split = originator_split;
  __interface__.read = read;
  __interface__.sparecraft = sparecraft;
  return __interface__;
})();

module.exports = has;
