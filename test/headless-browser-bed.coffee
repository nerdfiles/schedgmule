require.config
  urlArgs: 'cacheBuster=' + Math.floor(Math.random() * 10000000)
  baseUrl: ''
  paths:
    'utils/mock': './utils/mock'
    'BaseModel': './models/BaseModel'
    'jquery': '../../bower_components/jquery/dist/jquery'
    'jasmine': '../../bower_components/jasmine/lib/jasmine-core/jasmine'
    'jasmine-html': '../../bower_components/jasmine/lib/jasmine-core/jasmine-html'
    'jasmine-boot': '../../bower_components/jasmine/lib/jasmine-core/boot'
    'jasmine-ajax': '../../bower_components/jasmine-ajax/lib/mock-ajax'
    'chai': '../../bower_components/chai/chai'
    'chai-jquery': '../../bower_components/chai-jquery/chai-jquery'
  shim:
    'chai-jquery': [
      'jquery'
      'chai'
    ]
    'jasmine': exports: 'jasmine'
    'jasmine-ajax': deps: [ 'jasmine' ]
    'jasmine-html':
      deps: [ 'jasmine' ]
      exports: 'jasmine'

require [
  'jquery'
  'jasmine-html'
  'chai'
  'chai-jquery'
], ($, jasmine, chai, chaiJquery) ->
  #var jasmineEnv = jasmine.getEnv();
  #jasmineEnv.updateInterval = 1000;
  #var htmlReporter = new jasmine.HtmlReporter();
  #jasmineEnv.addReporter(htmlReporter);

  ###
  #jasmineEnv.specFilter = function (spec) {
  #  return htmlReporter.specFilter(spec);
  #};
  ###

  window.jasmine = jasmineRequire.core(jasmineRequire)
  #jasmineRequire.html(jasmine);
  #var env = jasmine.getEnv();
  #window.onload();
  specs = []

  #console.dir(window.jasmine);
  specs.push './e2e/browser-tests'

  $ ->
    window.reckon = chai.expect
    should = chai.should
    chai.use chaiJquery
    mocha.setup 'bdd'
    require specs, (spec) ->
      #jasmineEnv.execute();
      if window.mochaPhantomJS
        mochaPhantomJS.run()
      else
        mocha.run()
      return
    return
  return
