
/**
@fileOverview ./test/server/account.js
@description
REST-ful API Unit Tests for Accounts.
@external test/unit/account
 */
var assert, environment, has, mongoose, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

environment = require('../../schedgmule/config');

has = require('../has');

describe('Account API', function() {
  var baseUrl, getAccountByIdWithAssets, getAccountByIdWithNoAssets, getAllAccounts, getGenerate, postAccountLogin, postAccountSettings, subdomain, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(environment.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  postAccountSettings = function() {

    /**
    POST Account Settings
    @name postAccountSettings
    @function
    @memberof external:test/unit/account
    @description Files prefixed with a period (.) should be treated like 
    dotfiles (a convention of directorization).
     */
    it('should correctly create a new .settings DATA record for the given account', function(done) {
      var account;
      account = {
        username: 'vgheri',
        password: 'test',
        firstName: 'Valerio',
        lastName: 'Gheri',
        hdKey: 'jmt0d4y1sth3d4yddk',
        network: 'openchain',
        derivedKey: 'vRTZFozLkYNTj7VcoHMTSpNdUFbbS5CF9lbOFMR1YFv',
        rootAccount: '/p2pkh/' + '8RWANegWHsw5FO44WoVA9Nf01CJLB9in5KRjj9ZGZPi' + '/',
        initialized: true
      };
      request(url).post('/account/settings').send(account).expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  getAccountByIdWithAssets = function() {

    /**
    GET Account By Id with Assets
    @name getAccountByIdWithAssets
    @function
    @memberof external:test/account
     */
    it('should correctly get an existing account assets', function(done) {
      request(url).get('/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/').expect(200).expect(has.Links).expect(has.Data).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  getAccountByIdWithNoAssets = function() {

    /**
    GET Account By Id with No Assets
    @name getAccountByIdWithNoAssets
    @function
    @memberof external:test/account
     */
    it('should get an existing account with no assets populated', function(done) {
      request(url).get('/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjaIUHbbbhjjhbl/').expect(200).expect(has.Links).expect(has.NoData).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  getAllAccounts = function() {

    /**
    GET Accounts
    @name getAccounts
    @function
    @memberof external:test/account
    @description The given unit is an existence check from DB to Ledger
     */
    it('should correctly get all accounts', function(done) {
      request(url).get('/account').expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postAccountLogin = function() {

    /**
    POST Account Login
    @name postAccountLogin
    @function
    @memberof external:test/unit/account
     */
    it('should attempt to login', function(done) {
      var uuidConstruct;
      uuidConstruct = {
        uuid: 'f0d8368d-85e2-54fb-73c4-2d60374295e2'
      };
      request(url).post('/account/login').send(uuidConstruct).expect(200).expect(has.Links).expect(has.Data).expect(has.DerivedKey).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  getGenerate = function() {

    /**
    GET Generate
    @name getGenerate
    @function
    @memberof external:test/unit/account
     */
    it('should return a generated valid mnemonic string', function(done) {
      request(url).get('/generate').expect(200).expect(has.Links).expect(has.Data).expect(has.Passphrase).expect(has.PassphraseValidLength).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('GET /v1/generate', getGenerate);
  describe('POST /v1/account/login', postAccountLogin);
  describe('GET /v1/account', getAllAccounts);
  describe('GET /v1/account/:uuid with assets', getAccountByIdWithAssets);
  describe('GET /v1/account/:uuid with no assets', getAccountByIdWithNoAssets);
  describe('POST /v1/account', postAccountSettings);
});
