
/**
@fileOverview ./test/server/coringsample.js
@description
REST-ful API Tests for Base Models.
@external test/unit/coringsample
 */
var assert, bitcore, config, has, mongoose, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

bitcore = require('bitcore');

config = require('../../schedgmule/config');

has = require('../has');

describe('BaseModel API', function() {
  var baseUrl, getBaseModel, patchBaseModel, postBaseModel, postMultiMessage, postNote, postSingleMessage, postSplitBaseModel, subdomain, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(config.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  postBaseModel = function() {

    /**
    Register Base Model
    @name postBaseModel
    @function
    @memberof external:test/unit/coringsample
     */
    it('should POST to create new a Base Model mutation hash on the transaction ledger', function(done) {
      var assetDefinition, currentRecord, postConstruct, privkey;
      postConstruct = {
        assetGroupApi: 'some-assetGroup-api',
        dateCreated: new Date,
        sampleType: ['fluid'],
        depthRange: {
          min: 1000,
          max: 2000
        },
        author: "/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/",
        permission: {}
      };
      currentRecord = {};
      currentRecord.key = {
        "buffer": {
          "type": "Buffer",
          "data": [47, 112, 50, 112, 107, 104, 47, 88, 106, 115, 65, 112, 84, 70, 106, 84, 76, 54, 51, 89, 84, 111, 116, 111, 116, 69, 56, 114, 88, 51, 78, 70, 53, 97, 113, 82, 113, 99, 97, 114, 105, 47, 58, 65, 67, 67, 58, 47, 97, 115, 115, 101, 116, 47, 112, 50, 112, 107, 104, 47, 88, 98, 85, 78, 100, 56, 50, 86, 88, 103, 115, 76, 78, 115, 117, 122, 78, 54, 119, 67, 84, 106, 117, 122, 122, 118, 70, 107, 83, 68, 80, 109, 120, 52, 47]
        },
        "offset": 0,
        "markedOffset": -1,
        "limit": 95,
        "littleEndian": true,
        "noAssert": false
      };
      currentRecord.account = "/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/";
      currentRecord.asset = "/asset/p2pkh/XbUNd82VXgsLNsuzN6wCTjuzzvFkSDPmx4/";
      currentRecord.balance = {
        low: 188,
        high: 0
      };
      currentRecord.version = {
        "buffer": {
          "type": "Buffer",
          "data": [71, 69, 65, 166, 181, 119, 24, 90, 152, 103, 104, 142, 88, 157, 179, 229, 70, 100, 119, 230, 166, 153, 54, 243, 50, 205, 171, 1, 69, 86, 220, 5]
        },
        "offset": 0,
        "markedOffset": -1,
        "limit": 32,
        "littleEndian": false,
        "noAssert": true
      };
      postConstruct.permission.currentRecord = currentRecord;
      assetDefinition = {};
      assetDefinition.name = 'sparecraft_permission';
      assetDefinition.path = {
        parts: ['asset', 'p2pkh', 'XbUNd82VXgsLNsuzN6wCTjuzzvFkSDPmx4']
      };
      postConstruct.permission.assetDefinition = assetDefinition;
      postConstruct.permission.endpoint = ['http://107.170.46.60:8080/'];
      privkey = "88e215d348cca8cc9b2813bd6029e4ada937dc3f73d06dd1d74eff95f7a8d389";
      postConstruct.permission.privateKey = new bitcore.PrivateKey(privkey);
      postConstruct.permission.publicKey = new bitcore.PublicKey(postConstruct.permission.privateKey);
      request(url).post('/coringsample/').send(postConstruct).expect(200).expect(has.Debug).expect(function(res) {
        return res.body.assetGroupApi = 'some-assetGroup-api';
      }).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  getBaseModel = function() {

    /**
    Load Base Model
    @name getBaseModel
    @function
    @memberof external:test/unit/coringsample
     */
    it('should error to GET a Base Model by Record from the transaction ledger', function(done) {
      request(url).get('/coringsample/a-bad-uuid-for-coring-sample').expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  patchBaseModel = function() {

    /**
    Augment metadata within an asset history
    @name patchBaseModel
    @function
    @memberof external:test/unit/coringsample
     */
    it('should 404 fail to update the metadata of a Base Model record', function(done) {
      request(url).patch('/coringsample/').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSingleMessage = function() {

    /**
    Send Base Model to Single Recipients
    @name postSingleMessage
    @function
    @memberof external:test/unit/coringsample
     */
    return it('should 404 fail to send a message containing reference DATA of coring sample to a single user by public key on the asset', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForBaseModels;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'email@domain.com';
      listForBaseModels = '';
      request(url).post('/coringsample/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('samples', listForBaseModels).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postMultiMessage = function() {

    /**
    Send Base Model to Multiple Recipients
    @name postMultiMessage
    @function
    @memberof external:test/unit/coringsample
     */
    return it('should 404 fail to send a message containing reference DATA of coring sample to multiple users by public key on the asset', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForBaseModels;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'X';
      listForBaseModels = '';
      request(url).post('/coringsample/message').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('samples', listForBaseModels).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSplitBaseModel = function() {

    /**
    Split Base Model
    @name postSplitBaseModel
    @function
    @memberof external:test/unit/coringsample
     */
    return it('should 404 fail to issue a new coring sample asset from P2PKH account to the author with copied metadata and new token', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForBaseModels;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'WjC0XFf7FR8TVL';
      listForBaseModels = '';
      request(url).post('/coringsample/split/').field('key', keyStringForRootAccount).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postNote = function() {

    /**
    Post Note to Base Model
    @name postNote
    @function
    @memberof external:test/unit/coringsample
     */
    it('should 404 fail to post a note to a coring sample by uuid', function(done) {
      var keyStringForRootAccount;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      request(url).post('/coringsample/note/').field('key', keyStringForRootAccount).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('POST /v1/coringsample/:uuid/message/ (single)', postSingleMessage);
  describe('POST /v1/coringsample/:uuid/message/ (multiple)', postMultiMessage);
  describe('POST /v1/coringsample/:uuid/note/', postNote);
  describe('POST /v1/coringsample/:uuid/split/', postSplitBaseModel);
  describe('POST /v1/coringsample/', postBaseModel);
  describe('GET /v1/coringsample/', getBaseModel);
  describe('PATCH /v1/coringsample/:uuid/', patchBaseModel);
});
