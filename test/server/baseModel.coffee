###*
@fileOverview ./test/server/coringsample.js
@description
REST-ful API Tests for Base Models.
@external test/unit/coringsample
###

should = require('should')
assert = require('assert')
request = require('supertest')
mongoose = require('mongoose')
bitcore = require('bitcore')
#var winston = require('winston');
config = require('../../schedgmule/config')
has = require('../has')

describe 'BaseModel API', ->

  subdomain = 'alpha'
  baseUrl = subdomain + '.schedgmule.com/v1'
  baseUrl = 'localhost:3000/v1'
  url = 'http://' + baseUrl

  before (done) ->
    mongoose.connect config.__meta__.db
    done()
    return

  after (done) ->
    mongoose.disconnect()
    done()
    return

  postBaseModel = () ->

    ###*
    Register Base Model
    @name postBaseModel
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should POST to create new a Base Model mutation hash on the transaction ledger', (done) ->
      postConstruct =
        assetGroupApi: 'some-assetGroup-api'
        dateCreated : new Date
        sampleType: ['fluid']
        depthRange: {
          min: 1000,
          max: 2000
        }
        author: "/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/"
        permission: {}

      currentRecord = {}
      currentRecord.key =
        "buffer": {
          "type": "Buffer",
          "data": [47, 112, 50, 112, 107, 104, 47, 88, 106, 115, 65, 112, 84, 70, 106, 84, 76, 54, 51, 89, 84, 111, 116, 111, 116, 69, 56, 114, 88, 51, 78, 70, 53, 97, 113, 82, 113, 99, 97, 114, 105, 47, 58, 65, 67, 67, 58, 47, 97, 115, 115, 101, 116, 47, 112, 50, 112, 107, 104, 47, 88, 98, 85, 78, 100, 56, 50, 86, 88, 103, 115, 76, 78, 115, 117, 122, 78, 54, 119, 67, 84, 106, 117, 122, 122, 118, 70, 107, 83, 68, 80, 109, 120, 52, 47]
        },
        "offset"       : 0,
        "markedOffset" : -1,
        "limit"        : 95,
        "littleEndian" : true,
        "noAssert"     : false
      currentRecord.account = "/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/"
      currentRecord.asset = "/asset/p2pkh/XbUNd82VXgsLNsuzN6wCTjuzzvFkSDPmx4/"
      currentRecord.balance =
        low: 188
        high: 0
      currentRecord.version =
        "buffer":
          "type": "Buffer",
          "data": [71, 69, 65, 166, 181, 119, 24, 90, 152, 103, 104, 142, 88, 157, 179, 229, 70, 100, 119, 230, 166, 153, 54, 243, 50, 205, 171, 1, 69, 86, 220, 5]
        "offset"       : 0,
        "markedOffset" : -1,
        "limit"        : 32,
        "littleEndian" : false,
        "noAssert"     : true
      postConstruct.permission.currentRecord = currentRecord

      assetDefinition = {}

      assetDefinition.name = 'sparecraft_permission' # typically for issuing higher-order ledger tokens
      #assetDefinition.name = 'can_merge_permission' # merge the histories of two tokens
      #assetDefinition.name = 'read_permission' # read DATA such that typically is typically organized during saves to be read (discoverability clause such that links MUST appear in all payloads); also includes note participation @actions
      #assetDefinition.name = 'originator' # create principle data types like coring samples and assetGroups
      #assetDefinition.name = 'originator_split' # split coring samples for geoscientists
      #assetDefinition.name = 'metadata_permission' # upload meta-data-data to the thread with the same scope as notes
      #assetDefinition.name = 'metadata_communications_permission' # send a piece of uploaded data-data to another account via this feat
      #assetDefinition.name = 'atomic_action_permission' # basic test actions like reading arbitrary HTML controls visible or not visible

      assetDefinition.path =
        parts: [
          'asset'
          'p2pkh'
          'XbUNd82VXgsLNsuzN6wCTjuzzvFkSDPmx4'
        ]
      postConstruct.permission.assetDefinition = assetDefinition

      postConstruct.permission.endpoint = ['http://107.170.46.60:8080/']
      privkey = "88e215d348cca8cc9b2813bd6029e4ada937dc3f73d06dd1d74eff95f7a8d389"
      postConstruct.permission.privateKey = new bitcore.PrivateKey(privkey)
      postConstruct.permission.publicKey = new bitcore.PublicKey(postConstruct.permission.privateKey)

      request(url)
        .post('/coringsample/')
          .send(postConstruct)
          .expect(200)
          .expect(has.Debug)
          .expect((res) ->
            res.body.assetGroupApi = 'some-assetGroup-api'
          )
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return
    return

  getBaseModel = () ->

    ###*
    Load Base Model
    @name getBaseModel
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should error to GET a Base Model by Record from the transaction ledger', (done) ->
      request(url)
        .get('/coringsample/a-bad-uuid-for-coring-sample')
          .expect(200)
          #.expect(has.Debug)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  patchBaseModel = () ->

    ###*
    Augment metadata within an asset history
    @name patchBaseModel
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should 404 fail to update the metadata of a Base Model record', (done) ->
      request(url)
        .patch('/coringsample/')
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  postSingleMessage = () ->

    ###*
    Send Base Model to Single Recipients
    @name postSingleMessage
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should 404 fail to send a message containing reference DATA of coring sample to a single user by public key on the asset', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'email@domain.com'
      listForBaseModels = ''
      request(url)
        .post('/coringsample/message/')
          .field('key', keyStringForRootAccount)
          .field('recipients', keyListForRecipients)
          .field('samples', listForBaseModels)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return

  postMultiMessage = () ->

    ###*
    Send Base Model to Multiple Recipients
    @name postMultiMessage
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should 404 fail to send a message containing reference DATA of coring sample to multiple users by public key on the asset', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'X'
      listForBaseModels = ''
      request(url)
        .post('/coringsample/message')
          .field('key', keyStringForRootAccount)
          .field('recipients', keyListForRecipients)
          .field('samples', listForBaseModels)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return

  postSplitBaseModel = () ->

    ###*
    Split Base Model
    @name postSplitBaseModel
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should 404 fail to issue a new coring sample asset from P2PKH account to the author with copied metadata and new token', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'WjC0XFf7FR8TVL'
      listForBaseModels = ''
      request(url)
        .post('/coringsample/split/')
          .field('key', keyStringForRootAccount)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return

  postNote = ->

    ###*
    Post Note to Base Model
    @name postNote
    @function
    @memberof external:test/unit/coringsample
    ###

    it 'should 404 fail to post a note to a coring sample by uuid', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      request(url)
        .post('/coringsample/note/')
          .field('key', keyStringForRootAccount)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return
    return

  describe 'POST /v1/coringsample/:uuid/message/ (single)', postSingleMessage
  describe 'POST /v1/coringsample/:uuid/message/ (multiple)', postMultiMessage
  describe 'POST /v1/coringsample/:uuid/note/', postNote
  describe 'POST /v1/coringsample/:uuid/split/', postSplitBaseModel
  describe 'POST /v1/coringsample/', postBaseModel
  describe 'GET /v1/coringsample/', getBaseModel
  describe 'PATCH /v1/coringsample/:uuid/', patchBaseModel

  return
