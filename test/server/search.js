
/**
@fileOverview ./test/server/search.js
@description
REST-ful API Unit Tests for Search.
@external test/unit/search
 */
var assert, config, has, mongoose, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

config = require('../../schedgmule/config');

has = require('../has');

describe('Search API', function() {
  var baseUrl, getSearch, subdomain, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(config.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  getSearch = function() {
    it('should perform a basic search', function(done) {
      request(url).get('/search/').expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('GET /v1/search/', getSearch);
});
