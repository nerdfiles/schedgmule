###*
@fileOverview ./test/server/account.js
@description
REST-ful API Unit Tests for Accounts.
@external test/unit/account
###

should = require('should')
assert = require('assert')
request = require('supertest')
mongoose = require('mongoose')
environment = require('../../schedgmule/config')
has = require('../has')

describe 'Account API', ->

  subdomain = 'alpha'
  baseUrl = subdomain + '.schedgmule.com/v1'
  baseUrl = 'localhost:3000/v1'
  url = 'http://' + baseUrl

  before (done) ->
    mongoose.connect environment.__meta__.db
    done()
    return

  after (done) ->
    mongoose.disconnect()
    done()
    return

  postAccountSettings = () ->

    ###*
    POST Account Settings
    @name postAccountSettings
    @function
    @memberof external:test/unit/account
    @description Files prefixed with a period (.) should be treated like 
    dotfiles (a convention of directorization).
    ###

    it 'should correctly create a new .settings DATA record for the given account', (done) ->

      account =
        username    : 'vgheri'
        password    : 'test'
        firstName   : 'Valerio'
        lastName    : 'Gheri'
        hdKey       : 'jmt0d4y1sth3d4yddk'
        network     : 'openchain'
        derivedKey  : 'vRTZFozLkYNTj7VcoHMTSpNdUFbbS5CF9lbOFMR1YFv'
        rootAccount : '/p2pkh/' + '8RWANegWHsw5FO44WoVA9Nf01CJLB9in5KRjj9ZGZPi' + '/'
        initialized : true

      request(url)
        .post('/account/settings')
        .send(account)
          .expect(200)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  getAccountByIdWithAssets = () ->

    ###*
    GET Account By Id with Assets
    @name getAccountByIdWithAssets
    @function
    @memberof external:test/account
    ###

    it 'should correctly get an existing account assets', (done) ->

      request(url)
        .get('/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/')
          .expect(200)
          #.expect(has.Debug)
          .expect(has.Links)
          .expect(has.Data)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  getAccountByIdWithNoAssets = () ->

    ###*
    GET Account By Id with No Assets
    @name getAccountByIdWithNoAssets
    @function
    @memberof external:test/account
    ###

    it 'should get an existing account with no assets populated', (done) ->

      request(url)
        .get('/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjaIUHbbbhjjhbl/')
          .expect(200)
          .expect(has.Links)
          .expect(has.NoData)
          #.expect(has.Debug)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  getAllAccounts = () ->

    ###*
    GET Accounts
    @name getAccounts
    @function
    @memberof external:test/account
    @description The given unit is an existence check from DB to Ledger
    ###

    it 'should correctly get all accounts', (done) ->

      request(url)
        .get('/account')
          .expect(200)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  postAccountLogin = () ->

    ###*
    POST Account Login
    @name postAccountLogin
    @function
    @memberof external:test/unit/account
    ###

    it 'should attempt to login', (done) ->

      uuidConstruct = uuid: 'f0d8368d-85e2-54fb-73c4-2d60374295e2'

      request(url)
        .post('/account/login')
        .send(uuidConstruct)
          .expect(200)
          .expect(has.Links)
          .expect(has.Data)
          .expect(has.DerivedKey)
          #.expect(has.Debug)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  getGenerate = () ->

    ###*
    GET Generate
    @name getGenerate
    @function
    @memberof external:test/unit/account
    ###

    it 'should return a generated valid mnemonic string', (done) ->

      request(url)
        .get('/generate')
          .expect(200)
          .expect(has.Links)
          .expect(has.Data)
          .expect(has.Passphrase)
          .expect(has.PassphraseValidLength)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  describe 'GET /v1/generate', getGenerate
  describe 'POST /v1/account/login', postAccountLogin
  describe 'GET /v1/account', getAllAccounts
  describe 'GET /v1/account/:uuid with assets', getAccountByIdWithAssets
  describe 'GET /v1/account/:uuid with no assets', getAccountByIdWithNoAssets
  describe 'POST /v1/account', postAccountSettings

  return

