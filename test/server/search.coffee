###*
@fileOverview ./test/server/search.js
@description
REST-ful API Unit Tests for Search.
@external test/unit/search
###

should = require('should')
assert = require('assert')
request = require('supertest')
mongoose = require('mongoose')
#var winston = require('winston');
config = require('../../schedgmule/config')
has = require('../has')

describe 'Search API', ->

  subdomain = 'alpha'
  baseUrl = subdomain + '.schedgmule.com/v1'
  baseUrl = 'localhost:3000/v1'
  url = 'http://' + baseUrl

  before (done) ->
    mongoose.connect config.__meta__.db
    done()
    return

  after (done) ->
    mongoose.disconnect()
    done()
    return

  getSearch = () ->
    it 'should perform a basic search', (done) ->
      request(url)
        .get('/search/')
          .expect(200)
          #.expect(has.Data)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return
    return

  describe 'GET /v1/search/', getSearch

  return

