
/**
@fileOverview ./test/server/file.js
@description
REST-ful API Unit Tests for Files.
@external test/unit/file
 */
var assert, config, has, mongoose, path, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

config = require('../../schedgmule/config');

has = require('../has');

path = require('path');

describe('File API', function() {
  var baseUrl, postMultiMessage, postSingleMessage, postSingleMessageMultiReps, removeFile, subdomain, updateFile, uploadFile, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(config.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  uploadFile = function() {

    /**
    Upload File
    @name uploadFile
    @function
    @memberof external:test/unit/file
     */
    it('should POST a file with public keys and return a transaction receipt', function(done) {
      request(url).post('/file/').field('key', '"{"key": "WjC0XFf7FR8TVL"}"').expect(200).expect(has.Data).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  removeFile = function() {

    /**
    Remove File
    @name removeFile
    @function
    @memberof external:test/unit/file
     */
    it('should fail to DELETE a file with public keys and return a transaction receipt', function(done) {
      request(url)["delete"]('/file/').expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  updateFile = function() {

    /**
    Update File
    @name updateFile
    @function
    @memberof external:test/unit/file
     */
    it('should fail to find PUT for file with public keys and return a transaction receipt', function(done) {
      request(url).put('/file/').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSingleMessage = function() {

    /**
    Post Single Message
    @name postSingleMessage
    @function
    @memberof external:test/unit/file
     */
    return it('should send a message containing reference DATA of File itself to a single user by public key on the asset', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForFiles;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'email@domain.com';
      listForFiles = '';
      request(url).post('/file/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('samples', listForFiles).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postMultiMessage = function() {

    /**
    Post Multi Message
    @name postMultiMessage
    @function
    @memberof external:test/unit/file
     */
    return it('should send a message containing reference DATA of File itself to multiple users by public key on the asset and associated file names', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForFiles;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'X';
      listForFiles = '';
      request(url).post('/file/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('files', listForFiles).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSingleMessageMultiReps = function() {

    /**
    Post Single Message Multiple Recipients
    @name postSingleMessageMultiReps
    @function
    @memberof external:test/unit/file
     */
    return it('should send a message containing reference DATA of File itself to multiple users by public key on the asset and associated file names', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForFiles;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'X';
      listForFiles = '';
      request(url).post('/file/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('files', listForFiles).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('POST /v1/file/:uuid/message/ (single)', postSingleMessage);
  describe('POST /v1/file/:uuid/message/ (multiple)', postMultiMessage);
  describe('POST /v1/file/:uuid/message/ (multiple recipients)', postSingleMessageMultiReps);
  describe('POST /v1/file', uploadFile);
  describe('PUT /v1/:uuid/file', updateFile);
  describe('DELETE /v1/:uuid/file', removeFile);
});
