###*
@fileOverview ./test/server/report.js
@description
REST-ful API Unit Tests for Reports.
@external test/unit/report
###

should = require('should')
assert = require('assert')
request = require('supertest')
mongoose = require('mongoose')
#var winston = require('winston');
config = require('../../schedgmule/config')
has = require('../has')
path = require('path')

describe 'Report API', ->

  subdomain = 'alpha'
  baseUrl = subdomain + '.schedgmule.com/v1'
  baseUrl = 'localhost:3000/v1'
  url = 'http://' + baseUrl

  before (done) ->
    mongoose.connect config.__meta__.db
    done()
    return

  after (done) ->
    mongoose.disconnect()
    done()
    return

  uploadReport = ->

    ###*
    Upload Report
    @name uploadReport
    @function
    @memberof external:test/unit/report
    ###

    it 'should POST a report with public keys and return a transaction receipt', (done) ->
      request(url)
        .post('/report/')
          #.attach('image', path.join(__dirname + '/public/test-image.jpg'))
          .field('key', '"{"key": "WjC0XFf7FR8TVL"}"')
          .expect(404)
          #.expect(has.Data)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return
    return

  removeReport = ->

    ###*
    Remove Report
    @name removeReport
    @function
    @memberof external:test/unit/report
    ###

    it 'should fail to DELETE a report with public keys and return a transaction receipt', (done) ->
      request(url)
        .delete('/report/')
          .expect(404)
          #.expect(has.Data)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return
    return

  updateReport = ->

    ###*
    Update Report
    @name updateReport
    @function
    @memberof external:test/unit/report
    ###

    it 'should fail to find PUT for report with public keys and return a transaction receipt', (done) ->

      request(url)
        .put('/report/')
          .expect(404)
          #.expect(has.Data)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return
    return

  postSingleMessage = () ->
    it 'should send a message containing reference DATA of Report itself to a single user by public key on the asset', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'email@domain.com'
      listForReports = ''
      request(url)
        .post('/report/message/')
          .field('key', keyStringForRootAccount)
          .field('recipients', keyListForRecipients)
          .field('samples', listForReports)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return

  postMultiMessage = () ->
    it 'should send a message containing reference DATA of Report itself to multiple users by public key on the asset and associated report names', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'X'
      listForReports = ''
      request(url)
        .post('/report/message/')
          .field('key', keyStringForRootAccount)
          .field('recipients', keyListForRecipients)
          .field('reports', listForReports)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return

  postSingleMessageMulti = () ->
    it 'should send a message containing reference DATA of Report itself to multiple users by public key on the asset and associated report names', (done) ->
      keyStringForRootAccount = 'WjC0XFf7FR8TVL'
      keyListForRecipients = 'X'
      listForReports = ''
      request(url)
        .post('/report/message/')
          .field('key', keyStringForRootAccount)
          .field('recipients', keyListForRecipients)
          .field('reports', listForReports)
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return

      return


  describe 'POST /v1/report/:uuid/message/ (single)', postSingleMessage
  describe 'POST /v1/report/:uuid/message/ (multiple)', postMultiMessage
  describe 'POST /v1/report/:uuid/message/ (multiple recipients)', postSingleMessageMulti

  describe 'POST /v1/report/', uploadReport
  describe 'PUT /v1/report/:uuid/', updateReport
  describe 'DELETE /v1/report/:uuid/', removeReport

  return

