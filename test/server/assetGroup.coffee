###*
@fileOverview ./test/server/assetGroup.js
@description
REST-ful API Unit Tests for AssetGroups.
@external test/unit/assetGroup
###

should = require('should')
assert = require('assert')
request = require('supertest')
mongoose = require('mongoose')
#var winston = require('winston');
config = require('../../schedgmule/config')
has = require('../has')

describe 'AssetGroup API', ->

  subdomain = 'alpha'
  baseUrl = subdomain + '.schedgmule.com/v1'
  baseUrl = 'localhost:3000/v1'
  url = 'http://' + baseUrl

  before (done) ->
    mongoose.connect config.__meta__.db
    done()
    return

  after (done) ->
    mongoose.disconnect()
    done()
    return

  registerAssetGroup = () ->

    ###*
    Register AssetGroup
    @name registerAssetGroup
    @function
    @memberof external:test/unit/assetGroup
    ###

    it 'should fail 404 to register a new AssetGroup API with metadata', (done) ->
      request(url)
        .post('/assetGroup/')
          .expect(200)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return

  updateAssetGroup = () ->

    ###*
    Update AssetGroup
    @name updateAssetGroup
    @function
    @memberof external:test/unit/assetGroup
    ###

    it 'should fail 404 to update a assetGroup by ID at a specific property', (done) ->
      request(url)
        .patch('/assetGroup/')
          .expect(404)
        .end (err) ->
          if err
            return done(err)
          done()
          return
      return

  describe 'POST /v1/assetGroup/', registerAssetGroup
  describe 'PATCH /v1/assetGroup/', updateAssetGroup

  return
