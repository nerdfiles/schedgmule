
/**
@fileOverview ./test/server/report.js
@description
REST-ful API Unit Tests for Reports.
@external test/unit/report
 */
var assert, config, has, mongoose, path, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

config = require('../../schedgmule/config');

has = require('../has');

path = require('path');

describe('Report API', function() {
  var baseUrl, postMultiMessage, postSingleMessage, postSingleMessageMulti, removeReport, subdomain, updateReport, uploadReport, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(config.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  uploadReport = function() {

    /**
    Upload Report
    @name uploadReport
    @function
    @memberof external:test/unit/report
     */
    it('should POST a report with public keys and return a transaction receipt', function(done) {
      request(url).post('/report/').field('key', '"{"key": "WjC0XFf7FR8TVL"}"').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  removeReport = function() {

    /**
    Remove Report
    @name removeReport
    @function
    @memberof external:test/unit/report
     */
    it('should fail to DELETE a report with public keys and return a transaction receipt', function(done) {
      request(url)["delete"]('/report/').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  updateReport = function() {

    /**
    Update Report
    @name updateReport
    @function
    @memberof external:test/unit/report
     */
    it('should fail to find PUT for report with public keys and return a transaction receipt', function(done) {
      request(url).put('/report/').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSingleMessage = function() {
    return it('should send a message containing reference DATA of Report itself to a single user by public key on the asset', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForReports;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'email@domain.com';
      listForReports = '';
      request(url).post('/report/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('samples', listForReports).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postMultiMessage = function() {
    return it('should send a message containing reference DATA of Report itself to multiple users by public key on the asset and associated report names', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForReports;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'X';
      listForReports = '';
      request(url).post('/report/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('reports', listForReports).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  postSingleMessageMulti = function() {
    return it('should send a message containing reference DATA of Report itself to multiple users by public key on the asset and associated report names', function(done) {
      var keyListForRecipients, keyStringForRootAccount, listForReports;
      keyStringForRootAccount = 'WjC0XFf7FR8TVL';
      keyListForRecipients = 'X';
      listForReports = '';
      request(url).post('/report/message/').field('key', keyStringForRootAccount).field('recipients', keyListForRecipients).field('reports', listForReports).expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('POST /v1/report/:uuid/message/ (single)', postSingleMessage);
  describe('POST /v1/report/:uuid/message/ (multiple)', postMultiMessage);
  describe('POST /v1/report/:uuid/message/ (multiple recipients)', postSingleMessageMulti);
  describe('POST /v1/report/', uploadReport);
  describe('PUT /v1/report/:uuid/', updateReport);
  describe('DELETE /v1/report/:uuid/', removeReport);
});
