
/**
@fileOverview ./test/server/assetGroup.js
@description
REST-ful API Unit Tests for AssetGroups.
@external test/unit/assetGroup
 */
var assert, config, has, mongoose, request, should;

should = require('should');

assert = require('assert');

request = require('supertest');

mongoose = require('mongoose');

config = require('../../schedgmule/config');

has = require('../has');

describe('AssetGroup API', function() {
  var baseUrl, registerAssetGroup, subdomain, updateAssetGroup, url;
  subdomain = 'alpha';
  baseUrl = subdomain + '.schedgmule.com/v1';
  baseUrl = 'localhost:3000/v1';
  url = 'http://' + baseUrl;
  before(function(done) {
    mongoose.connect(config.__meta__.db);
    done();
  });
  after(function(done) {
    mongoose.disconnect();
    done();
  });
  registerAssetGroup = function() {

    /**
    Register AssetGroup
    @name registerAssetGroup
    @function
    @memberof external:test/unit/assetGroup
     */
    return it('should fail 404 to register a new AssetGroup API with metadata', function(done) {
      request(url).post('/assetGroup/').expect(200).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  updateAssetGroup = function() {

    /**
    Update AssetGroup
    @name updateAssetGroup
    @function
    @memberof external:test/unit/assetGroup
     */
    return it('should fail 404 to update a assetGroup by ID at a specific property', function(done) {
      request(url).patch('/assetGroup/').expect(404).end(function(err) {
        if (err) {
          return done(err);
        }
        done();
      });
    });
  };
  describe('POST /v1/assetGroup/', registerAssetGroup);
  describe('PATCH /v1/assetGroup/', updateAssetGroup);
});
