###*
@fileOverview ./test/utils/mock.js
@external test/utils
###

define [], ->

  ###*
  Utils for mocking data models in a client-side context.
  @returns {Function}
  ###

  (constr, name) ->

    ###*
    Mock.
    @name mock
    @function
    @memberof external:test/utils
    @param {Function} constr Constructor.
    @param {String} name Name of the base of Mock.
    ###

    HelperConstr = new Function
    HelperConstr.prototype = constr.prototype
    result = new HelperConstr
    for key of constr.prototype
      try
        if constr.prototype[key] instanceof Function
          result[key] = jasmine.createSpy((name or 'mock') + '.' + key)
      catch ex
        console.log ex
    result
