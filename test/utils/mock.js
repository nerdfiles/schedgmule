
/**
@fileOverview ./test/utils/mock.js
@external test/utils
 */
define([], function() {

  /**
  Utils for mocking data models in a client-side context.
  @returns {Function}
   */
  return function(constr, name) {

    /**
    Mock.
    @name mock
    @function
    @memberof external:test/utils
    @param {Function} constr Constructor.
    @param {String} name Name of the base of Mock.
     */
    var HelperConstr, ex, key, result;
    HelperConstr = new Function;
    HelperConstr.prototype = constr.prototype;
    result = new HelperConstr;
    for (key in constr.prototype) {
      try {
        if (constr.prototype[key] instanceof Function) {
          result[key] = jasmine.createSpy((name || 'mock') + '.' + key);
        }
      } catch (_error) {
        ex = _error;
        console.log(ex);
      }
    }
    return result;
  };
});
