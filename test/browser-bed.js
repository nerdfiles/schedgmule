
/*
var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

var pathToModule = function(path) {
    return path.replace(/^\/base\//, '').replace(/\.js$/, '');
};

Object.keys(window.__karma__.files).forEach(function(file) {
    if (TEST_REGEXP.test(file)) {
        // Normalize paths to RequireJS module names.
        allTestFiles.push(pathToModule(file));
    }
});
 */
var file, tests;

tests = [];

for (file in window.__karma__.files) {
  if (/-tests\.js$/.test(file)) {
    tests.push(file);
  }
}

require.config({
  baseUrl: '/base',
  paths: {
    'utils/mock': './utils/mock',
    'BaseModel': './models/BaseModel',
    'Account': './models/Account'
  },
  deps: tests,
  callback: window.__karma__.start
});
