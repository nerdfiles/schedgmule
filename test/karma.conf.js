module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine-ajax', 'jasmine', 'requirejs'],
    files: [
      {
        pattern: 'utils/mock.js',
        included: false
      }, {
        pattern: 'models/BaseModel.js',
        included: false
      }, {
        pattern: 'models/Account.js',
        included: false
      }, {
        pattern: 'e2e/browser-tests.js',
        included: false
      }, {
        pattern: 'e2e/account-tests.js',
        included: false
      }, 'browser-bed.js'
    ],
    exclude: [],
    preprocessors: {
      'e2e/browser-tests.js': ['jshint', 'coverage'],
      'e2e/account.js': ['jshint', 'coverage']
    },
    jshintPreprocessor: {
      jshintrc: '../.jshintrc',
      stopOnError: true
    },
    coverageReporter: {
      type: 'html',
      dir: 'coverage/'
    },
    reporters: ['karmaSimpleReporter', 'coverage'],
    specReporter: {
      suppressPassed: false,
      suppressSkipped: false,
      suppressFailed: false,
      suppressErrorSummary: false,
      maxLogLines: 5,
      prefixes: {
        success: '✓ ',
        failure: '✗ ',
        skipped: '- '
      }
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    captureTimeout: 60000,
    singleRun: true
  });
};
