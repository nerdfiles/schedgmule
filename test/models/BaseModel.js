
/**
@fileOverview ./test/models/BaseModel.js
@external test/e2e/models/coringsample
@description
Base Model Unit Test Model
 */
define([], function() {
  var BaseModel;
  BaseModel = function() {};
  BaseModel.prototype.setCommentsIndex = function(discussionUrl) {

    /**
    Set Comments Index
    @name setCommentsIndex
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.discussionUrl = discussionUrl;
  };
  BaseModel.prototype.getCommentsIndex = function() {

    /**
    Get Comments Index
    @name getCommentsIndex
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.discussionUrl;
  };
  BaseModel.prototype.setName = function(name) {

    /**
    Set Name
    @name setName
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.name = name;
  };
  BaseModel.prototype.getName = function() {

    /**
    Get Name
    @name getName
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.name;
  };
  BaseModel.prototype.setBuid = function(buid) {

    /**
    Set BUID
    @name setBuid
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.buid = buid;
  };
  BaseModel.prototype.getBuid = function() {

    /**
    Get BUID
    @name getBuid
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.buid;
  };
  BaseModel.prototype.setStatus = function(status) {

    /**
    Set Status
    @name setStatus
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.status = status;
  };
  BaseModel.prototype.getStatus = function() {

    /**
    Get Status
    @name getStatus
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.status;
  };
  BaseModel.prototype.setAddress = function(address) {

    /**
    Set Address
    @name setAddress
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.address = address;
  };
  BaseModel.prototype.getAddress = function() {

    /**
    Get Address
    @name getAddress
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.address;
  };
  BaseModel.prototype.setType = function(type) {

    /**
    Set Type
    @name setType
    @function
    @memberof external:test/e2e/models/coringsample
     */
    this.type = type;
  };
  BaseModel.prototype.getType = function() {

    /**
    Get Type
    @name getType
    @function
    @memberof external:test/e2e/models/coringsample
     */
    return this.type;
  };
  return BaseModel;
});
