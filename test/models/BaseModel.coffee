###*
@fileOverview ./test/models/BaseModel.js
@external test/e2e/models/coringsample
@description
Base Model Unit Test Model
###

define [], ->

  BaseModel = () ->

  BaseModel::setCommentsIndex = (discussionUrl) ->

    ###*
    Set Comments Index
    @name setCommentsIndex
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @discussionUrl = discussionUrl
    return

  BaseModel::getCommentsIndex = ->

    ###*
    Get Comments Index
    @name getCommentsIndex
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @discussionUrl

  BaseModel::setName = (name) ->

    ###*
    Set Name
    @name setName
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @name = name
    return

  BaseModel::getName = ->

    ###*
    Get Name
    @name getName
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @name

  BaseModel::setBuid = (buid) ->

    ###*
    Set BUID
    @name setBuid
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @buid = buid
    return

  BaseModel::getBuid = ->

    ###*
    Get BUID
    @name getBuid
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @buid

  BaseModel::setStatus = (status) ->

    ###*
    Set Status
    @name setStatus
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @status = status
    return

  BaseModel::getStatus = ->

    ###*
    Get Status
    @name getStatus
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @status

  BaseModel::setAddress = (address) ->

    ###*
    Set Address
    @name setAddress
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @address = address
    return

  BaseModel::getAddress = ->

    ###*
    Get Address
    @name getAddress
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @address

  BaseModel::setType = (type) ->

    ###*
    Set Type
    @name setType
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @type = type
    return

  BaseModel::getType = ->

    ###*
    Get Type
    @name getType
    @function
    @memberof external:test/e2e/models/coringsample
    ###

    @type

  BaseModel
