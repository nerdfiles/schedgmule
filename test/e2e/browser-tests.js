
/**
@fileOverview ./test/browser-test.js
@description
PhantomJS E2E/behavior tests. Mocks are available at ./models/*.
@TODO
1. Front end models — Lovefield?
 */
define(['utils/mock', 'BaseModel'], function(mock, BaseModel) {
  var baseUrl;
  baseUrl = 'http://localhost:3000';
  describe('BaseModel', function() {
    var entity;
    entity = void 0;
    beforeEach(function() {
      jasmine.Ajax.install();
      entity = mock(BaseModel);
    });
    afterEach(function() {
      jasmine.Ajax.uninstall();
    });
    describe('Load Base Model Details', function() {
      var _loadedBaseModelDetailsMock;
      beforeEach(function() {});
      _loadedBaseModelDetailsMock = {
        get: {
          success: {
            links: {},
            data: [{}]
          }
        }
      };
      it('loads the correct user details', function() {
        var doneFn, r, xhr;
        doneFn = jasmine.createSpy('success');
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(args) {
          if (this.readyState === this.DONE) {
            doneFn(this.responseText);
          }
        };
        xhr.open('GET', baseUrl + '/v1/coringsample/XcPu898xQ9Cb72cvWNR3yX1jSJifBMPYX3');
        xhr.send();
        r = jasmine.Ajax.requests.mostRecent();
        r.respondWith(_loadedBaseModelDetailsMock.get.success);
      });
    });
    it('GET BaseModel', function() {
      var doneFn, targetUrl, xhr;
      doneFn = jasmine.createSpy('success');
      xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function(args) {
        if (this.readyState === this.DONE) {
          doneFn(this.responseText);
        }
      };
      xhr.open('GET', baseUrl + '/v1/coringsample/XcPu898xQ9Cb72cvWNR3yX1jSJifBMPYX3');
      xhr.send();
      targetUrl = baseUrl + '/v1/coringsample/XcPu898xQ9Cb72cvWNR3yX1jSJifBMPYX3';
      expect(jasmine.Ajax.requests.mostRecent().url).toBe(targetUrl);
      expect(doneFn).not.toHaveBeenCalled();
      jasmine.Ajax.requests.mostRecent().respondWith({
        'status': 200,
        'contentType': 'text/plain',
        'responseText': 'truthy resp'
      });
      expect(doneFn).toHaveBeenCalledWith('truthy resp');
    });
    it('Get Name on Base Model', function(done) {
      entity.getName();
      expect(entity.getName).toHaveBeenCalled();
      done();
    });
    it('Get BUID on Base Model', function(done) {
      entity.getBuid();
      expect(entity.getBuid).toHaveBeenCalled();
      done();
    });
    it('Set BUID on Base Model', function(done) {
      entity.setBuid('A');
      expect(entity.getBuid).not.toBe(null);
      done();
    });

    /*
     *
     *    beforeEach(function () {
     *      this.addMatchers({
     *        toBeGET: function() {
     *          // @usage expect({ method: 'GET'}).toBeGET();
     *          var actual = this.actual.method;
     *          return actual === 'GET';
     *        },
     *        toHaveUrl: function(expected) {
     *          // @usage expect({ url: '<string:url>' }).toHaveUrl('<string:url>')
     *          var actual = this.actual.url;
     *          this.message = function() {
     *            return "Expected request to have url " + expected + " but was " + actual
     *          };
     *          return actual === expected;
     *        }
     *      });
     *    });
     *
     */

    /*
     *  it('should run through various tests', function () {
     *
     *    entity.getName();
     *    entity.setBuid('ABC');
     *
     *    entity.setType('physical');
     *    expect(entity.setType).toHaveBeenCalledWith(jasmine.any(String));
     *
     *    expect(entity.getName).toHaveBeenCalled();
     *    expect(entity.setBuid).toHaveBeenCalledWith('ABC');
     *    expect(entity.getBuid()).toEqual('A');
     *    expect(entity.getStatus()).toEqual('Active');
     *
     *    entity.getPhoneNumber = jasmine.createSpy('entity type').andReturn('digital');
     *
     *    expect(entity.getType()).toEqual('10065');
     *    expect(entity.getType).toHaveBeenCalled();
     *
     *
     *    var addressSpy = jasmine.createSpyObj('Address', ['setStreetName', 'getStreetName']);
     *    (function () {
     *      var streetName;
     *      addressSpy.getStreetName = function () {
     *        return streetName;
     *      };
     *      addressSpy.setStreetName = function (name) {
     *        streetName = name;
     *      };
     *    }());
     *
     *    entity.setAddress(addressSpy);
     *    entity.getAddress().setStreetName('Carrington');
     *    expect(entity.getAddress().getStreetName()).toEqual('Carrington');
     *
     *    var pullName = function () {
     *      entity.setName('Base Model #888');
     *    };
     *
     *    runs(
     *      function () {
     *        setTimeout(pullName, 100);
     *      }
     *    );
     *
     *    waitsFor(function () {
     *        return entity.getName() !== undefined;
     *      },
     *      'entity name should have been set to what i expected', 200
     *    );
     *
     *  });
     */
  });
});
