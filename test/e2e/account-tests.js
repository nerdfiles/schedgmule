define(['utils/mock', 'Account'], function(mock, Account) {
  var baseUrl;
  baseUrl = 'http://localhost:3000/v1';
  describe('Account', function() {
    var entity;
    entity = void 0;
    beforeEach(function() {
      jasmine.Ajax.install();
      entity = mock(Account);
    });
    afterEach(function() {
      var _loadedAccountDetails;
      jasmine.Ajax.uninstall();
      _loadedAccountDetails = {
        get: {
          success: {
            links: {},
            data: [{}]
          }
        }
      };
    });
    describe('Load Account Details', function() {
      beforeEach(function() {});
      it('should load an account with permissions to read data', function() {
        var doneFn, targetUrl, xhr;
        doneFn = jasmine.createSpy('success');
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(args) {
          if (this.readyState === this.DONE) {
            doneFn(this.responseText);
          }
        };
        xhr.open('GET', baseUrl + '/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/');
        xhr.send();
        targetUrl = baseUrl + '/account/f0d8368d-85e2-54fb-73c4-2d60374295e2?account=/p2pkh/XjsApTFjTL63YTototE8rX3NF5aqRqcari/';
        expect(jasmine.Ajax.requests.mostRecent().url).toBe(targetUrl);
        expect(doneFn).not.toHaveBeenCalled();
        jasmine.Ajax.requests.mostRecent().respondWith({
          'status': 200,
          'contentType': 'text/plain',
          'responseText': 'truthy resp'
        });
        expect(doneFn).toHaveBeenCalledWith('truthy resp');
      });
    });
  });
});
