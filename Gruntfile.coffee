###*
@fileOverview ./Gruntfile.coffee
@description
Local development task recipes for deployments, testing, dev ops, code management, docs 
generation.
@TODO Add tasks for interaction and administration with MongoDB via mongoose and 
elasticsearch. It would be great to have grunt tasks for issuing arbitrary API tests, 
but this will depend on the level of fragmentation in the tests themselves.
###

module.exports = (grunt) ->

  require('load-grunt-tasks') grunt
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-apidoc'
  grunt.loadNpmTasks 'grunt-plato'
  grunt.loadNpmTasks 'grunt-simple-mocha'
  grunt.loadNpmTasks 'grunt-karma'
  grunt.loadNpmTasks 'grunt-jsdoc'
  grunt.loadNpmTasks('grunt-bg-shell')

  ###*
  Define Configuration Variables.
  Note: cwd is './setup' so the `setup` variable defined below is only to be used
        when cwd has been changed to `app` and grunt needs to reference './setup'
  ###

  gruntConfig = grunt.file.readJSON('Gruntconfig.json')

  grunt.initConfig
    cvars: gruntConfig.configVars

    bgShell:
      _defaults:
        bg: true
      watchCompass:
        cmd: 'compass watch'
      watchCoffee:
        cmd: 'coffee --watch --output lib/ src/'
      runNode:
        cmd: 'node server/www',
        bg: true
      'pm2':
        bg: true
        fail: true
        stdout: true
        stderr: true
        cmd: 'pm2 kill && pm2 start server/www && pm2 logs'
      'pm2-hidden':
        fail: true
        stdout: false
        stderr: false
        cmd: 'pm2 kill && pm2 start server/www && pm2 logs'
      'test-api':
        bg: false
        fail: true
        stdout: true
        stderr: true
        cmd: 'grunt test/api'

    shell:

      'mocha-phantomjs':
        command: 'mocha-phantomjs -R dot http://localhost:3000/test/e2e',
        options:
          stdout: true
          stderr: true

      'merge-deploy':
        command: 'git add . && git commit -m "Auto-deploy." && git checkout master && git merge -X theirs develop && git push -u origin master && git checkout develop && git push -u origin develop'
        options:
          stdout: true
          stderr: true

      'pre-deploy':
        command: 'git add . && git commit -m "Auto-deploy." && git checkout master && git merge develop && git push -u origin master && git checkout develop && git push -u origin develop'
        options:
          stdout: true
          stderr: true

      'git-add-and-commit-docs-src':
        command: 'git add src/ && git commit -m "Update Source Code Documentation."'
        options:
          stdout: true
          stderr: true

      'git-add-and-commit-docs-api':
        command: 'git add docs/ && git commit -m "Update API Documentation."'
        options:
          stdout: true
          stderr: true

      'git-add-and-commit-docs-analysis':
        command: 'git add analysis/ && git commit -m "Update Static Analysis."'
        options:
          stdout: true
          stderr: true

      'git-add-and-commit-test-api':
        command: 'git add mochawesome-reports/* && git commit -m "Update API Unit Tests."'
        options:
          stdout: true
          stderr: true

      'git-add-and-commit-test-coverage':
        command: 'git add test/coverage/ && git commit -m "Update E2E Test Coverage."'
        options:
          stdout: true
          stderr: true

      'git-docs-merge':
        command: 'git add src && git add docs && git add mochawesome-reports && git add analysis && git commit -m "Merge docs."'
        options:
          stdout: true
          stderr: true

    jsdoc:
      dist:
        src: [
          'schedgmule/**/**/**/*.js'
          'test/**/**/*.js'
          '!test/coverage/*'
        ]
      options:
        destination: 'src'

    simplemocha:
      options:
        ui              : 'bdd'
        reporter        : 'mochawesome'
        timeout         : 7000
        retries         : 2
        bail            : true
        slow            : 2000
        ignoreLeaks     : false
        fullTrace       : true
        reporterOptions :
          reportName: 'index'
      backend:
        src: [
          'test/server/*.js'
        ]

    karma:
      unit:
        configFile: 'test/karma.conf.js'
        background: false
        plugins: [
          'karma-jasmine-ajax'
          'karma-jasmine'
          'karma-requirejs'
          'karma-phantomjs-launcher'
          'karma-simple-reporter'
          'karma-coverage'
          'karma-jshint'
        ]

    plato:
      analysis:
        options:
          complexity:
            logicalor    : true
            switchcase   : true
            forin        : true
            trycatch     : true
            newmi        : true
        files:
          'analysis': [
            'schedgmule/**/**/**/*.js'
            'test/**/*.js'
            '!test/coverage/**/*.js'
          ]

    apidoc:
      schedgmule:
        src: 'schedgmule/'
        dest: 'docs/'

    coffee:
      compile:
        files: gruntConfig.coffeeFiles
      e2e:
        options:
          bare: true
        files: gruntConfig.e2eFiles
      compileBare:
        options:
          bare: true
        files: gruntConfig.coffeeFiles

    bower:
      setup:
        options:
          install: true
          copy: false

    copy:
      setup: files: [
        {
          cwd: 'bower_components'
          expand: true
          flatten: true
          dest: '<%= cvars.app %>/<%= cvars.appjs %>/ext/'
          src: gruntConfig.bowerFiles
        }
        {
          cwd: 'bower_components'
          expand: true
          flatten: false
          dest: '<%= cvars.app %>/<%= cvars.appcss %>/ext/'
          src: gruntConfig.cssFiles
        }
      ]
      build: files: [ {
        cwd: '<%= cvars.app %>/'
        expand: true
        dest: '<%= cvars.build %>/'
        src: gruntConfig.buildFiles
      } ]
      deploy: files: [ {
        cwd: '<%= cvars.build %>/'
        expand: true
        dest: '<%= cvars.dist %>/'
        src: [
          'css/**'
          'images/**'
          'data/**'
        ]
      } ]

    clean:
      options:
        force: true
      build: [ '<%= cvars.build %>' ]
      deploy: [ '<%= cvars.dist %>/*' ]

    cssmin:
      build:
        files: '<%= cvars.build %>/<%= cvars.appcss %>/style.css': [
          'bower_components/bootstrap/dist/css/bootstrap.min.css'
          '<%= cvars.app %>/<%= cvars.appcss %>/style.css'
        ]

    preprocess:
      build:
        src: '<%= cvars.app %>/index.html'
        dest: '<%= cvars.build %>/index.build.html'

    htmlmin:
      build:
        options:
          removeComments: true
          collapseBooleanAttributes: true
          removeAttributeQuotes: true
          removeRedundantAttributes: true
          removeEmptyAttributes: true
          removeEmptyElements: false
        files: [
          { '<%= cvars.build %>/index.html': '<%= cvars.build %>/index.build.html' }
          {
            cwd: '<%= cvars.app %>/view/'
            expand: true
            flatten: false
            dest: '<%= cvars.build %>/view/'
            src: [ '*.html' ]
          }
        ]
      deploy:
        options: collapseWhitespace: true
        files: [
          { '<%= cvars.dist %>/index.html': '<%= cvars.build %>/index.html' }
          {
            cwd: '<%= cvars.build %>/js/directive/template/'
            expand: true
            flatten: false
            dest: '<%= cvars.dist %>/js/directive/template/'
            src: [ '*.html' ]
          }
          {
            cwd: '<%= cvars.build %>/view/'
            expand: true
            flatten: false
            dest: '<%= cvars.dist %>/view/'
            src: [ '*.html' ]
          }
        ]

    requirejs: build: options:
      baseUrl: '<%= cvars.app %>/js'
      mainConfigFile: '<%= cvars.app %>/js/main.js'
      removeCombined: true
      findNestedDependencies: true
      optimize: 'none'
      dir: '<%= cvars.build %>/js/'
      modules: [
        { name: 'main' }
        {
          name: 'controller/home_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/matches_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/match_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/players_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/player_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/teams_ctrl'
          exclude: [ 'main' ]
        }
        {
          name: 'controller/team_ctrl'
          exclude: [ 'main' ]
        }
      ]

    uglify:
      deploy:
        options:
          preserveComments: 'some'
        files: [ {
          cwd: '<%= cvars.build %>/js/'
          expand: true
          dest: '<%= cvars.dist %>/js/'
          src: [
            '*.js'
            'ext/require.js'
            'controller/*.js'
          ]
        } ]

    jshint:
      build:
        options:
          jshintrc: '<%= cvars.app %>/js/jshintrc.json'
        files: src: [
          '<%= cvars.app %>/js/*.js'
          '<%= cvars.app %>/js/controller/*.js'
          '<%= cvars.app %>/js/directive/*.js'
          '<%= cvars.app %>/js/provider/*.js'
        ]

    watch:
      e2e:
        files: [
          '<%= cvars.test %>/e2e/*.coffee'
          '<%= cvars.test %>/karma.conf.coffee'
          '<%= cvars.test %>/browser-bed.coffee'
          '<%= cvars.test %>/headless-browser-bed.coffee'
          '<%= cvars.test %>/utils/mock.coffee'
          '<%= cvars.test %>/models/*.coffee'
        ]
        tasks: [
          'coffee:e2e'
          'karma'
        ]
        options:
          spawn      : true
          livereload : false
      www:
        files: [
          '<%= cvars.app %>/**/**/**/*'
          '<%= cvars.modules.schedgmule %>/**/**/**/*'
          '<%= cvars.test %>/karma.conf.coffee'
          '<%= cvars.test %>/browser-bed.coffee'
          '<%= cvars.test %>/browser-tests.coffee'
        ]
        tasks: [
          'coffee:compileBare'
          'bgShell:pm2'
          #'docs/api'
          #'docs/src'

          #'test/api'
          'bgShell:test-api'
        ]
        options:
          spawn      : true
          livereload : true

    connect:
      server:
        livereload: false
        options:
          port: gruntConfig.configVars.port
          base: '<%= cvars.app %>'

  ###*
  # setup task
  # Run the initial setup, sourcing all needed upstream dependencies
  ###

  grunt.registerTask 'setup', [
    'bower:setup'
    'copy:setup'
  ]

  ###*
  # devel task
  # Launch webserver and watch for changes
  ###

  grunt.registerTask 'devel', [
    'bgShell:pm2-hidden'
    'watch:www'
  ]

  ###*
  # build task
  # Use r.js to build the project
  ###

  grunt.registerTask 'build', [
    'jshint:build'
    'clean:build'
    'preprocess:build'
    'htmlmin:build'
    'cssmin:build'
    'requirejs:build'
    'copy:build'
  ]

  ###*
  # deploy task
  # Deploy to dist_www directory
  ###

  grunt.registerTask 'deploy', [
    'build'
    'clean:deploy'
    'htmlmin:deploy'
    'uglify:deploy'
    'copy:deploy'
  ]

  grunt.registerTask 'ping', ->
    grunt.log.write 'ping: ', gruntConfig
    return

  grunt.registerTask 'docs/analysis', [
    'plato'
    #'shell:git-add-and-commit-docs-analysis'
  ]

  grunt.registerTask 'docs/api', [
    'apidoc'
    #'shell:git-add-and-commit-docs-api'
  ]

  grunt.registerTask 'docs/src', [
    'jsdoc'
    #'shell:git-add-and-commit-docs-src'
  ]

  grunt.registerTask 'docs/merge', [
    'shell:git-docs-merge'
  ]

  grunt.registerTask 'test/api', [
    'simplemocha'
    #'shell:git-add-and-commit-test-api'
  ]

  grunt.registerTask 'test/browser', [
    'karma'
    #'shell:git-add-and-commit-test-coverage'
  ]

  grunt.registerTask 'watch/browser', [
    'watch:e2e'
    #'shell:git-add-and-commit-test-coverage'
  ]

  # Does not work because jasmine.Ajax does not load to global.
  grunt.registerTask 'test/runner', [
    'shell:mocha-phantomjs'
  ]

  grunt.registerTask 'deploy/pre/dev', [
    'test/browser'
    'shell:pre-deploy'
    #'ping'
    'pm2deploy:dev'
  ]

  grunt.registerTask 'deploy/merge/dev', [
    'test/browser'
    'shell:merge-deploy'
    #'ping'
    'pm2deploy:dev'
  ]

  return
